package com.hypernymbiz.zenath.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.toolbox.OnDialogButtonClick;

/**
 * Created by Metis on 19-Apr-18.
 */

public class RequestDialog extends Dialog {
    private String mTitle_icon;
    private String mMessage;
    EditText remaks;
    private OnDialogButtonClick mOnClickListener;

    public RequestDialog(Context context, String icon, String message, OnDialogButtonClick onClickListener) {
        super(context);
        this.mTitle_icon = icon;
        this.mMessage = message;
        this.mOnClickListener = onClickListener;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_request_job);
        TextView mtitle = (TextView) findViewById(R.id.img_tick);
        TextView messageText = (TextView) findViewById(R.id.txt_message);
        ImageView positiveButton = (ImageView) findViewById(R.id.img_ok);
        ImageView negativeButton = (ImageView) findViewById(R.id.img_cancel);
        remaks = (EditText) findViewById(R.id.edittxt_remark);


//        positiveButton.setTransformationMethod(null);


        if (mTitle_icon != null) {
            mtitle.setText(mTitle_icon);
            mtitle.setVisibility(View.VISIBLE);
        }


        if (mMessage != null) {
            messageText.setText(mMessage);
            messageText.setVisibility(View.VISIBLE);
        }

        positiveButton.setVisibility(View.VISIBLE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnClickListener.onButtonClick(view, remaks.getText().toString());
            }
        });


        negativeButton.setVisibility(View.VISIBLE);
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnClickListener.onButtonClick(view, remaks.getText().toString());
            }
        });


        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }
}
