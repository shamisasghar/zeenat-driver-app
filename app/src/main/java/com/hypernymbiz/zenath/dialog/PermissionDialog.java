package com.hypernymbiz.zenath.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hypernymbiz.zenath.R;

/**
 * Created by Metis on 04-Apr-18.
 */

public class PermissionDialog extends Dialog {
    private String mTitle;
    private String mMessage;
    private String mPositiveButtonText;
    private String mNegativeButtonText;
    private int micon;
    private View.OnClickListener mOnClickListener;

    public PermissionDialog(Context context, String title, String message, String positiveButtonText, int icon_id, View.OnClickListener onClickListener) {
        super(context);
        this.mTitle = title;
        this.mMessage = message;
        this.mPositiveButtonText = positiveButtonText;
        this.mOnClickListener = onClickListener;
        this.micon=icon_id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_request);
        TextView titleText = (TextView) findViewById(R.id.txt_title);
        TextView messageText = (TextView) findViewById(R.id.txt_message);
        ImageView imgicon = (ImageView) findViewById(R.id.img_internet);
        Button positiveButton = (Button) findViewById(R.id.btn_dialog_ok);
        positiveButton.setTransformationMethod(null);
        if (mTitle != null) {
            titleText.setText(mTitle);
            titleText.setVisibility(View.VISIBLE);
        }
        if (mMessage != null) {
            messageText.setText(mMessage);
            messageText.setVisibility(View.VISIBLE);
        }


        if (mPositiveButtonText != null) {
            positiveButton.setText(mPositiveButtonText);
            positiveButton.setVisibility(View.VISIBLE);
            positiveButton.setOnClickListener(mOnClickListener);
        }

            imgicon.setImageResource(micon);
            imgicon.setVisibility(View.VISIBLE);



        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }
}
