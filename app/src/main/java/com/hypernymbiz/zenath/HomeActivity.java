package com.hypernymbiz.zenath;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hypernymbiz.zenath.api.ApiInterface;
import com.hypernymbiz.zenath.dialog.PermissionDialog;
import com.hypernymbiz.zenath.dialog.SimpleDialog;
import com.hypernymbiz.zenath.fragments.HomeFragment;
import com.hypernymbiz.zenath.fragments.IncidentReportFragment;
import com.hypernymbiz.zenath.fragments.JobFragment;
import com.hypernymbiz.zenath.fragments.JobNotificationFragment;
import com.hypernymbiz.zenath.fragments.MaintenanceFragment;
import com.hypernymbiz.zenath.fragments.ProfileFragment;
import com.hypernymbiz.zenath.model.Profile;
import com.hypernymbiz.zenath.model.WebAPIResponse;
import com.hypernymbiz.zenath.toolbox.ToolbarListener;
import com.hypernymbiz.zenath.utils.ActivityUtils;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shamis on 10/10/2017.
 */

public class HomeActivity extends AppCompatActivity implements ToolbarListener, OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {
    private Toolbar mToolbar;
    private SimpleDialog mSimpleDialog;
    GoogleMap googleMap;
    TextView driveremail, drivername;
    CircleImageView image_profile;
    SharedPreferences pref;
    private PermissionDialog permissionDialog;
    View view;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbarSetup();
        checkForUpdates();


        String fragmentName = getIntent().getStringExtra(Constants.FRAGMENT_NAME);
        Bundle bundle = getIntent().getBundleExtra(Constants.DATA);
        if (!TextUtils.isEmpty(fragmentName)) {
            Fragment fragment = Fragment.instantiate(this, fragmentName);
            if (bundle != null)
                fragment.setArguments(bundle);
            addFragment(fragment);
        } else {
            addFragment(new HomeFragment());
        }

        Locationcheck();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);


        drivername = (TextView) headerView.findViewById(R.id.txt_drivername);
        driveremail = (TextView) headerView.findViewById(R.id.txt_email);
        image_profile = (CircleImageView) headerView.findViewById(R.id.img_driver);
        pref = this.getSharedPreferences("TAG", MODE_PRIVATE);
        String email, name;

        email = pref.getString("Email", "");
        name = pref.getString("Drivername", "");

        driveremail.setText(email);
        drivername.setText(name);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

//            Toast.makeText(this, " already granted", Toast.LENGTH_SHORT).show();
        } else {

            requestlocationpermission();
        }

        ApiInterface.retrofit.getprofile().enqueue(new Callback<WebAPIResponse<Profile>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<Profile>> call, Response<WebAPIResponse<Profile>> response) {

                try {
                    if (response.isSuccessful()) {

                        if (response.body().status == 200) {

                            String  url;
                            url = response.body().response.getPhoto();
                            Glide.with(getApplicationContext()).load(url).into(image_profile);
                        }

                    } else {
                    }
                } catch (Exception ex) {

                    AppUtils.showSnackBar(view,AppUtils.getErrorMessage(getApplicationContext(), 2));
                }


            }

            @Override
            public void onFailure(Call<WebAPIResponse<Profile>> call, Throwable t) {

            }
        });






//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }


    private void requestlocationpermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            new AlertDialog.Builder(this)
                    .setTitle("Permission Needed")
                    .setMessage("This permission needed to use location")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //Prompt the user once explanation has been shown
                            ActivityCompat.requestPermissions(HomeActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_LOCATION);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .create()
                    .show();

        } else {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {

        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(this, "permission not granted", Toast.LENGTH_SHORT).show();


            }


        }


    }


    public void addFragment(final Fragment fragment) {
        if (fragment.getClass().getName().equals(HomeFragment.class.getName())) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(fragment.getClass().getName()
            ).commit();
        }
    }

    public void toolbarSetup() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(" ");
        ActivityUtils.centerToolbarTitle(mToolbar, true);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public void setTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment instanceof HomeFragment) {
                mSimpleDialog = new SimpleDialog(this, null, getString(R.string.msg_exit),
                        getString(R.string.button_cancel), getString(R.string.button_ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.button_positive:
                                mSimpleDialog.dismiss();
                                HomeActivity.this.finish();
                                break;
                            case R.id.button_negative:
                                mSimpleDialog.dismiss();
                                break;
                        }
                    }
                });
                mSimpleDialog.show();
            } else {
                super.onBackPressed();
            }

//            mSimpleDialog = new SimpleDialog(this, null, getString(R.string.msg_exit),
//                    getString(R.string.button_cancel), getString(R.string.button_ok), new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    switch (view.getId()) {
//                        case R.id.button_positive:
//                            mSimpleDialog.dismiss();
//                            HomeActivity.this.finish();
//                            break;
//                        case R.id.button_negative:
//                            mSimpleDialog.dismiss();
//                            break;
//                    }
//                }
//            });
//            mSimpleDialog.show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.home) {

            if (!AppUtils.isInternetAvailable(this)) {

                permissionDialog = new PermissionDialog(this, getString(R.string.title_internet), getString(R.string.msg_internet)
                        , getString(R.string.button_ok), R.drawable.ic_no_internet, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.btn_dialog_ok:
                                permissionDialog.dismiss();
                                ActivityUtils.startWifiSettings(HomeActivity.this);
                                break;

                        }
                    }
                });
                permissionDialog.show();

//                mSimpleDialog = new SimpleDialog(this, getString(R.string.title_internet), getString(R.string.msg_internet),
//                        getString(R.string.button_cancel), getString(R.string.button_ok), new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        switch (view.getId()) {
//                            case R.id.button_positive:
//                                mSimpleDialog.dismiss();
//                                ActivityUtils.startWifiSettings(HomeActivity.this);
//                                break;
//                            case R.id.button_negative:
//                                mSimpleDialog.dismiss();
//                                break;
//                        }
//                    }
//                });
//                mSimpleDialog.show();
                return true;
            }
            addFragment(new HomeFragment());
        } else if (id == R.id.jobs) {

            if (!AppUtils.isInternetAvailable(this)) {
                permissionDialog = new PermissionDialog(this, getString(R.string.title_internet), getString(R.string.msg_internet)
                        , getString(R.string.button_ok), R.drawable.ic_no_internet, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.btn_dialog_ok:
                                permissionDialog.dismiss();
                                ActivityUtils.startWifiSettings(HomeActivity.this);
                                break;

                        }
                    }
                });
                permissionDialog.show();
                return true;
            }
            addFragment(new JobFragment());

        } else if (id == R.id.profile) {
            if (!AppUtils.isInternetAvailable(this)) {
                permissionDialog = new PermissionDialog(this, getString(R.string.title_internet), getString(R.string.msg_internet)
                        , getString(R.string.button_ok), R.drawable.ic_no_internet, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.btn_dialog_ok:
                                permissionDialog.dismiss();
                                ActivityUtils.startWifiSettings(HomeActivity.this);
                                break;

                        }
                    }
                });
                permissionDialog.show();
                return true;
            }
            addFragment(new ProfileFragment());
        } else if (id == R.id.maintenance) {

            Toast.makeText(this, "Maintenance", Toast.LENGTH_SHORT).show();
//            if (!AppUtils.isInternetAvailable(this)) {
//                permissionDialog = new PermissionDialog(this, getString(R.string.title_internet), getString(R.string.msg_internet)
//                        , getString(R.string.button_ok), R.drawable.ic_no_internet, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        switch (view.getId()) {
//                            case R.id.btn_dialog_ok:
//                                permissionDialog.dismiss();
//                                ActivityUtils.startWifiSettings(HomeActivity.this);
//                                break;
//
//                        }
//                    }
//                });
//                permissionDialog.show();
//                return true;
//            }
//            addFragment(new MaintenanceFragment());
        } else if (id == R.id.notification) {
            if (!AppUtils.isInternetAvailable(this)) {
                permissionDialog = new PermissionDialog(this, getString(R.string.title_internet), getString(R.string.msg_internet)
                        , getString(R.string.button_ok), R.drawable.ic_no_internet, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.btn_dialog_ok:
                                permissionDialog.dismiss();
                                ActivityUtils.startWifiSettings(HomeActivity.this);
                                break;

                        }
                    }
                });
                permissionDialog.show();
                return true;
            }
            ActivityUtils.startActivity(this, FrameActivity.class, JobNotificationFragment.class.getName(), null);

        } else if (id == R.id.incident_report) {
            if (!AppUtils.isInternetAvailable(this)) {
                permissionDialog = new PermissionDialog(this, getString(R.string.title_internet), getString(R.string.msg_internet)
                        , getString(R.string.button_ok), R.drawable.ic_no_internet, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.btn_dialog_ok:
                                permissionDialog.dismiss();
                                ActivityUtils.startWifiSettings(HomeActivity.this);
                                break;

                        }
                    }
                });
                permissionDialog.show();
                return true;
            }
            addFragment(new IncidentReportFragment());
        } else if (id == R.id.violation) {
            Toast.makeText(this, "Violation", Toast.LENGTH_SHORT).show();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void Locationcheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            permissionDialog = new PermissionDialog(this, getString(R.string.title_location), getString(R.string.msg_location)
                    , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.btn_dialog_ok:
                            permissionDialog.dismiss();
                            ActivityUtils.startWifiSettings(HomeActivity.this);
                            break;

                    }
                }
            });
            permissionDialog.setCancelable(false);
            permissionDialog.setCanceledOnTouchOutside(false);
            permissionDialog.show();
            return;
        }
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }
    private void unregisterManagers() {
        UpdateManager.unregister();
    }
    private void checkForCrashes() {
        CrashManager.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterManagers();
    }
}
