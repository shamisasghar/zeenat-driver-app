package com.hypernymbiz.zenath.toolbox;


public interface ToolbarListener {
    void setTitle(String title);
}
