package com.hypernymbiz.zenath.toolbox;

import android.text.Editable;
import android.view.View;

public interface OnAfterTextChange {
    void onAfterTextChange(View view, Editable editable);
}
