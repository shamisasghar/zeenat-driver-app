package com.hypernymbiz.zenath.toolbox;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View view, Object data, int position);
}
