package com.hypernymbiz.zenath.toolbox;

import android.view.View;

/**
 * Created by Metis on 19-Apr-18.
 */

public interface OnDialogButtonClick {
    void onButtonClick(View view, String data);
}
