package com.hypernymbiz.zenath.service;

import android.os.Bundle;
import android.util.Log;

import com.hypernymbiz.zenath.FrameActivity;
import com.hypernymbiz.zenath.dialog.SimpleDialog;
import com.hypernymbiz.zenath.fragments.HomeFragment;
import com.hypernymbiz.zenath.fragments.JobDetailsFragment;
import com.hypernymbiz.zenath.fragments.JobNotificationFragment;
import com.hypernymbiz.zenath.fragments.MaintenanceAssignedFragment;
import com.hypernymbiz.zenath.model.NotifyEvent;
import com.hypernymbiz.zenath.model.PayloadNotification;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;
import com.hypernymbiz.zenath.utils.GsonUtils;
import com.hypernymbiz.zenath.utils.LoginUtils;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Metis on 22-Mar-18.
 */

public class OneSignalReceiver extends NotificationExtenderService {
    PayloadNotification payloadNotification;
    private SimpleDialog mSimpleDialog;
    private EventBus bus = EventBus.getDefault();
    NotifyEvent event=new NotifyEvent("notifcation");


    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {


//

        // Read properties from result.
        JSONObject additionalData = receivedResult.payload.additionalData;
        Log.e("test", additionalData.toString());
        if (additionalData != null) {
            if (receivedResult.isAppInFocus) {
                payloadNotification = new PayloadNotification();
                try {

                        payloadNotification.title = additionalData.getString("title");
                        payloadNotification.message = additionalData.getString("message");
                        payloadNotification.notification_type=additionalData.getInt("notification_type");
                    if(payloadNotification.notification_type==118||payloadNotification.notification_type==119||payloadNotification.notification_type==113||payloadNotification.notification_type==114||payloadNotification.notification_type==115)
                    {
                        bus.post(event);
                        // ObservableObject.getInstance().updateValue("Notification");

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PAYLOAD, GsonUtils.toJson(payloadNotification));
                        if (payloadNotification.notification_type == 80) {
                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobDetailsFragment.class.getName(), bundle, payloadNotification.title, false, 0);
                        } else if (payloadNotification.notification_type == 85) {

                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobDetailsFragment.class.getName(), bundle, payloadNotification.title, false, 0);

                        } else if (payloadNotification.title.equals("Upcoming maintenance")) {

                            AppUtils.makeNotification(getApplication(), FrameActivity.class, MaintenanceAssignedFragment.class.getName(), bundle, payloadNotification.title, false, 0);

                        } else {
                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobNotificationFragment.class.getName(), bundle, payloadNotification.title, false, 0);
                        }
                    }
                    else
                    {
                        payloadNotification.title = additionalData.getString("title");
                        payloadNotification.message = additionalData.getString("message");
                        payloadNotification.notification_type = additionalData.getInt("notification_type");
                        payloadNotification.job_id = additionalData.getInt("job_id");

                        bus.post(event);
                        // ObservableObject.getInstance().updateValue("Notification");

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PAYLOAD, GsonUtils.toJson(payloadNotification));
                        if (payloadNotification.notification_type == 80) {
                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobDetailsFragment.class.getName(), bundle, payloadNotification.title, false, payloadNotification.job_id);
                        } else if (payloadNotification.notification_type == 85) {

                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobDetailsFragment.class.getName(), bundle, payloadNotification.title, false, payloadNotification.job_id);

                        } else if (payloadNotification.title.equals("Upcoming maintenance")) {

                            AppUtils.makeNotification(getApplication(), FrameActivity.class, MaintenanceAssignedFragment.class.getName(), bundle, payloadNotification.title, false, payloadNotification.job_id);

                        } else {
                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobNotificationFragment.class.getName(), bundle, payloadNotification.title, false, payloadNotification.job_id);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            else
                {

                try {
                    payloadNotification = new PayloadNotification();



                        payloadNotification.title = additionalData.getString("title");
                        payloadNotification.message = additionalData.getString("message");
                        payloadNotification.notification_type=additionalData.getInt("notification_type");
                    if(payloadNotification.notification_type==118||payloadNotification.notification_type==119||payloadNotification.notification_type==113||payloadNotification.notification_type==114||payloadNotification.notification_type==115)
                    {

                        bus.post(event);
                        // ObservableObject.getInstance().updateValue("Notification");

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PAYLOAD, GsonUtils.toJson(payloadNotification));
                        if (payloadNotification.notification_type == 80) {
                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobDetailsFragment.class.getName(), bundle, payloadNotification.title, false, 0);
                        } else if (payloadNotification.notification_type == 85) {

                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobDetailsFragment.class.getName(), bundle, payloadNotification.title, false, 0);

                        } else if (payloadNotification.title.equals("Upcoming maintenance")) {

                            AppUtils.makeNotification(getApplication(), FrameActivity.class, MaintenanceAssignedFragment.class.getName(), bundle, payloadNotification.title, false, 0);

                        } else {
                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobNotificationFragment.class.getName(), bundle, payloadNotification.title, false, 0);
                        }
                    }
                    else
                    {
                        payloadNotification.title = additionalData.getString("title");
                        payloadNotification.message = additionalData.getString("message");
                        payloadNotification.notification_type = additionalData.getInt("notification_type");
                        payloadNotification.job_id = additionalData.getInt("job_id");

                        bus.post(event);
                        // ObservableObject.getInstance().updateValue("Notification");

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PAYLOAD, GsonUtils.toJson(payloadNotification));
                        if (payloadNotification.notification_type == 80) {
                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobDetailsFragment.class.getName(), bundle, payloadNotification.title, false, payloadNotification.job_id);
                        } else if (payloadNotification.notification_type == 85) {

                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobDetailsFragment.class.getName(), bundle, payloadNotification.title, false, payloadNotification.job_id);

                        } else if (payloadNotification.title.equals("Upcoming maintenance")) {

                            AppUtils.makeNotification(getApplication(), FrameActivity.class, MaintenanceAssignedFragment.class.getName(), bundle, payloadNotification.title, false, payloadNotification.job_id);

                        } else {
                            AppUtils.makeNotification(getApplication(), FrameActivity.class, JobNotificationFragment.class.getName(), bundle, payloadNotification.title, false, payloadNotification.job_id);
                        }
                    }
//                    if (orderStatus.status == OrderPlacedEnum.READY_FOR_PAYMENT.getValue()) {
//                        if (!AppUtils.isRunningInForeground(this)) {
//                            Bundle bundle = new Bundle();
//                            bundle.putString(Constants.READY_FOR_PAYMENT, GsonUtils.toJson(orderStatus));
//                            AppUtils.makeNotification(this, FrameActivity.class, PayFragment.class.getName(), bundle, orderStatus.message, false, orderStatus.order_id);
//                        } else {
//                            Bundle bundle = new Bundle();
//                            bundle.putString(Constants.READY_FOR_PAYMENT, GsonUtils.toJson(orderStatus));
//                            ActivityUtils.startActivity(this, FrameActivity.class, PayFragment.class.getName(), bundle);
//                        }
//                    } else if (orderStatus.status == OrderPlacedEnum.NEED_TO_BE_CHANGED.getValue()) {
//                        if (!AppUtils.isRunningInForeground(this)) {
//                            orderStatus.lines_deleted = additionalData.getInt("lines_deleted");
//                            Bundle bundle = new Bundle();
//                            bundle.putString(Constants.NEED_TO_BE_CHANGED, GsonUtils.toJson(orderStatus));
//                            AppUtils.makeNotification(this, FrameActivity.class, MyCartFragment.class.getName(), bundle, orderStatus.message, true, orderStatus.order_id);
//                        } else {
//                            orderStatus.lines_deleted = additionalData.getInt("lines_deleted");
//                            Bundle bundle = new Bundle();
//                            bundle.putString(Constants.NEED_TO_BE_CHANGED, GsonUtils.toJson(orderStatus));
//                            ActivityUtils.startActivity(this, FrameActivity.class, MyCartFragment.class.getName(), bundle, true);
//                        }
//                    } else if (orderStatus.status == OrderPlacedEnum.PAYMENT_DONE.getValue()) {
//                        if (!AppUtils.isRunningInForeground(this)) {
//                            Bundle bundle = new Bundle();
//                            bundle.putString(Constants.PAYMENT_DONE, GsonUtils.toJson(orderStatus));
//                        } else {
//                            Bundle bundle = new Bundle();
//                            bundle.putString(Constants.PAYMENT_DONE, GsonUtils.toJson(orderStatus));
//                            ActivityUtils.startActivity(this, FrameActivity.class, PaymentDoneFragment.class.getName(), bundle, true);
//                        }
//                    } else if (orderStatus.status == OrderPlacedEnum.CANCELLED.getValue()) {
//                        if (!AppUtils.isRunningInForeground(this)) {
//                            Bundle bundle = new Bundle();
//                            bundle.putString(Constants.CANCELED, GsonUtils.toJson(orderStatus));
//                            AppUtils.makeNotification(this, FrameActivity.class, MyCartFragment.class.getName(), bundle, orderStatus.message, true, orderStatus.order_id);
//                        } else {
//                            Bundle bundle = new Bundle();
//                            bundle.putString(Constants.CANCELED, GsonUtils.toJson(orderStatus));
//                            ActivityUtils.startActivity(this, FrameActivity.class, MyCartFragment.class.getName(), bundle, true);
//                        }
//                    } else if (orderStatus.status == OrderPlacedEnum.PAYMENT_UNSUCCESSFUL.getValue()) {
//                        if (!AppUtils.isRunningInForeground(this)) {
//                            Bundle bundle = new Bundle();
//                            bundle.putString(Constants.PAYMENT_UNSUCCESSFUL, GsonUtils.toJson(orderStatus));
//                            AppUtils.makeNotification(this, FrameActivity.class, PaymentDoneFragment.class.getName(), bundle, orderStatus.message, false, orderStatus.order_id);
//                        } else {
//                            Bundle bundle = new Bundle();
//                            bundle.putString(Constants.PAYMENT_UNSUCCESSFUL, GsonUtils.toJson(orderStatus));
//                            ActivityUtils.startActivity(this, FrameActivity.class, PaymentDoneFragment.class.getName(), bundle, true);
//                        }
                    // } else
//                    if (orderStatus.status == OrderPlacedEnum.READY_TO_BE_SERVED.getValue() ||
//                            orderStatus.status == OrderPlacedEnum.SERVED.getValue()) {
//                        Bundle bundle = new Bundle();
//                        bundle.putInt(Constants.ORDER_ID, orderStatus.order_id);
//                        AppUtils.makeNotification(MyApplication.getAppContext(), FrameActivity.class, OrderTrackingFragment.class.getName(), bundle, orderStatus.message, true, orderStatus.order_id);
////                        AppUtils.makeNotification(this, HomeActivity.class, CategoriesFragment.class.getName(), null, orderStatus.message, true, new Random().nextInt(1000 - 1 + 1) + 1);
//                    }
                } catch (Exception e) {
                    Log.e("TAAG",""+e);
                    e.printStackTrace();
                }
            }
        }
        // Return true to stop the notification from displaying.
        return true;
    }
}