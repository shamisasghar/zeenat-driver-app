package com.hypernymbiz.zenath.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Metis on 27-Mar-18.
 */

public class JobDetail {

    @SerializedName("activity_type")
    @Expose
    private String activityType;
    @SerializedName("activity_status")
    @Expose
    private String activityStatus;
    @SerializedName("assigned_truck")
    @Expose
    private String assignedTruck;
    @SerializedName("schedule_type")
    @Expose
    private String scheduleType;
    @SerializedName("activity_time")
    @Expose
    private String activityTime;
    @SerializedName("end_point_name")
    @Expose
    private Object endPointName;

    public Object getCheckPointName() {
        return checkPointName;
    }

    public void setCheckPointName(Object checkPointName) {
        this.checkPointName = checkPointName;
    }

    public Object getCheckPointLatLong() {
        return checkPointLatLong;
    }

    public void setCheckPointLatLong(Object checkPointLatLong) {
        this.checkPointLatLong = checkPointLatLong;
    }

    @SerializedName("check_point_name")
    @Expose
    private Object checkPointName;

    public Integer getDuration() {
        return duration;
    }

    @SerializedName("duration")
    @Expose
    private Integer duration;

    @SerializedName("end_point_lat_long")
    @Expose
    private Object endPointLatLong;
    @SerializedName("check_point_lat_long")
    @Expose
    private Object checkPointLatLong;

    @SerializedName("action_items")
    @Expose
    private List<ActionItem> actionItems = null;

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getAssignedTruck() {
        return assignedTruck;
    }

    public void setAssignedTruck(String assignedTruck) {
        this.assignedTruck = assignedTruck;
    }

    public String getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(String scheduleType) {
        this.scheduleType = scheduleType;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }

    public Object getEndPointName() {
        return endPointName;
    }

    public void setEndPointName(Object endPointName) {
        this.endPointName = endPointName;
    }

    public Object getEndPointLatLong() {
        return endPointLatLong;
    }

    public void setEndPointLatLong(Object endPointLatLong) {
        this.endPointLatLong = endPointLatLong;
    }

    public List<ActionItem> getActionItems() {
        return actionItems;
    }

    public void setActionItems(List<ActionItem> actionItems) {
        this.actionItems = actionItems;
    }

}