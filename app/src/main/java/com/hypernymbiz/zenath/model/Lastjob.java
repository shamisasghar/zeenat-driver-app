package com.hypernymbiz.zenath.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Metis on 17-May-18.
 */

public class Lastjob {

    @SerializedName("check_point_name")
    @Expose
    private Object checkPointName;

    public Object getCheckPointName() {
        return checkPointName;
    }

    public void setCheckPointName(Object checkPointName) {
        this.checkPointName = checkPointName;
    }

    public Object getCheckPointLatLong() {
        return checkPointLatLong;
    }

    public void setCheckPointLatLong(Object checkPointLatLong) {
        this.checkPointLatLong = checkPointLatLong;
    }

    @SerializedName("check_point_lat_long")
    @Expose
    private Object checkPointLatLong;

    @SerializedName("activity_type")
    @Expose
    private String activityType;
    @SerializedName("activity_status")
    @Expose
    private String activityStatus;
    @SerializedName("assigned_truck")
    @Expose
    private String assignedTruck;
    @SerializedName("driver")
    @Expose
    private String driver;
    @SerializedName("schedule_type")
    @Expose
    private String scheduleType;
    @SerializedName("activity_time")
    @Expose
    private String activityTime;
    @SerializedName("end_point_name")
    @Expose
    private String endPointName;
    @SerializedName("end_point_lat_long")
    @Expose
    private String endPointLatLong;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("action_items")
    @Expose
    private List<ActionItem> actionItems = null;

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getAssignedTruck() {
        return assignedTruck;
    }

    public void setAssignedTruck(String assignedTruck) {
        this.assignedTruck = assignedTruck;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(String scheduleType) {
        this.scheduleType = scheduleType;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }

    public String getEndPointName() {
        return endPointName;
    }

    public void setEndPointName(String endPointName) {
        this.endPointName = endPointName;
    }

    public String getEndPointLatLong() {
        return endPointLatLong;
    }

    public void setEndPointLatLong(String endPointLatLong) {
        this.endPointLatLong = endPointLatLong;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public List<ActionItem> getActionItems() {
        return actionItems;
    }

    public void setActionItems(List<ActionItem> actionItems) {
        this.actionItems = actionItems;
    }

}