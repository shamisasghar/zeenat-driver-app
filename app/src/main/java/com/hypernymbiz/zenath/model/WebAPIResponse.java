package com.hypernymbiz.zenath.model;

/**
 * Created by Metis on 22-Mar-18.
 */

public class WebAPIResponse<T> {
    public Integer status;
    public String message;
    public T response;
}