package com.hypernymbiz.zenath.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Metis on 10-May-18.
 */

public class Datum {

    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("driver")
    @Expose
    private String driver;
    @SerializedName("truck")
    @Expose
    private String truck;
    @SerializedName("incident")
    @Expose
    private String incident;
    @SerializedName("activity")
    @Expose
    private String activity;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
    }

    public String getIncident() {
        return incident;
    }

    public void setIncident(String incident) {
        this.incident = incident;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

}