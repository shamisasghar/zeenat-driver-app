package com.hypernymbiz.zenath.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Metis on 07-May-18.
 */

public class Completed {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("activity_type")
    @Expose
    private String activityType;
    @SerializedName("activity_status")
    @Expose
    private String activityStatus;
    @SerializedName("assigned_truck")
    @Expose
    private String assignedTruck;
    @SerializedName("schedule_type")
    @Expose
    private Object scheduleType;
    @SerializedName("activity_time")
    @Expose
    private String activityTime;
    @SerializedName("end_point_name")
    @Expose
    private String endPointName;
    @SerializedName("end_point_lat_long")
    @Expose
    private String endPointLatLong;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getAssignedTruck() {
        return assignedTruck;
    }

    public void setAssignedTruck(String assignedTruck) {
        this.assignedTruck = assignedTruck;
    }

    public Object getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(Object scheduleType) {
        this.scheduleType = scheduleType;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }

    public String getEndPointName() {
        return endPointName;
    }

    public void setEndPointName(String endPointName) {
        this.endPointName = endPointName;
    }

    public String getEndPointLatLong() {
        return endPointLatLong;
    }

    public void setEndPointLatLong(String endPointLatLong) {
        this.endPointLatLong = endPointLatLong;
    }

}