package com.hypernymbiz.zenath.model;

/**
 * Created by Metis on 21-Apr-18.
 */

public class RunningResumeJob {
    private String job_name;
    private String job_id;
    private ActionItem[] actionItem;


    public ActionItem[] getActionItem() {
        return actionItem;
    }

    public void setActionItem(ActionItem[] actionItem) {
        this.actionItem = actionItem;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getJob_name() {
        return job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }
}
