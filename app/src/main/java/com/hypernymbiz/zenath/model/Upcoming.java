package com.hypernymbiz.zenath.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Metis on 07-May-18.
 */

public class Upcoming  {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("customer")
    @Expose
    private String customer;
    @SerializedName("module")
    @Expose
    private String module;
    @SerializedName("schedule_status")
    @Expose
    private String scheduleStatus;
    @SerializedName("primary_entity")
    @Expose
    private String primaryEntity;
    @SerializedName("activity_end_point")
    @Expose
    private String activityEndPoint;
    @SerializedName("action_items")
    @Expose
    private List<UpcomingActionItem> actionItems = null;
    @SerializedName("actor")
    @Expose
    private String actor;
    @SerializedName("activity_datetime")
    @Expose
    private String activityDatetime;



    public String getActivityEndPointLatLng() {
        return activityEndPointLatLng;
    }

    public void setActivityEndPointLatLng(String activityEndPointLatLng) {
        this.activityEndPointLatLng = activityEndPointLatLng;
    }

    @SerializedName("activity_end_point_lat_long")
    @Expose
    private String activityEndPointLatLng;

    public Object getCheckPointLatLong() {
        return checkPointLatLong;
    }

    public void setCheckPointLatLong(Object checkPointLatLong) {
        this.checkPointLatLong = checkPointLatLong;
    }

    @SerializedName("check_point_lat_long")
    @Expose
    private Object checkPointLatLong;

    public Object getCheckPointName() {
        return checkPointName;
    }

    public void setCheckPointName(Object checkPointName) {
        this.checkPointName = checkPointName;
    }

    @SerializedName("check_point_name")
    @Expose
    private Object checkPointName;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getScheduleStatus() {
        return scheduleStatus;
    }

    public void setScheduleStatus(String scheduleStatus) {
        this.scheduleStatus = scheduleStatus;
    }

    public String getPrimaryEntity() {
        return primaryEntity;
    }

    public void setPrimaryEntity(String primaryEntity) {
        this.primaryEntity = primaryEntity;
    }

    public String getActivityEndPoint() {
        return activityEndPoint;
    }

    public void setActivityEndPoint(String activityEndPoint) {
        this.activityEndPoint = activityEndPoint;
    }

    public List<UpcomingActionItem> getActionItems() {
        return actionItems;
    }

    public void setActionItems(List<UpcomingActionItem> actionItems) {
        this.actionItems = actionItems;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getActivityDatetime() {
        return activityDatetime;
    }

    public void setActivityDatetime(String activityDatetime) {
        this.activityDatetime = activityDatetime;
    }

}