package com.hypernymbiz.zenath.model;

/**
 * Created by Metis on 14-Apr-18.
 */

public class RuningJobStatus {

    private Boolean buttonleft ;
    private Boolean buttonright;

    public RuningJobStatus(Boolean buttonleft, Boolean buttonright) {
        this.buttonleft = buttonleft;
        this.buttonright = buttonright;
    }


    public Boolean getButtonleft() {
        return buttonleft;
    }

    public void setButtonleft(Boolean buttonleft) {
        this.buttonleft = buttonleft;
    }

    public Boolean getButtonright() {
        return buttonright;
    }

    public void setButtonright(Boolean buttonright) {
        this.buttonright = buttonright;
    }





}
