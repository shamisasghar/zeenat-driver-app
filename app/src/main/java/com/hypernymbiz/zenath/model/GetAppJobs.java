package com.hypernymbiz.zenath.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Metis on 07-May-18.
 */

public class GetAppJobs {

    @SerializedName("completed")
    @Expose
    private List<Completed> completed = null;
    @SerializedName("failed")
    @Expose
    private List<Failed> failed = null;
    @SerializedName("upcoming")
    @Expose
    private Upcoming upcoming = null;

    public List<Completed> getCompleted() {
        return completed;
    }

    public void setCompleted(List<Completed> completed) {
        this.completed = completed;
    }

    public List<Failed> getFailed() {
        return failed;
    }

    public void setFailed(List<Failed> failed) {
        this.failed = failed;
    }

    public Upcoming getUpcoming() {
        return upcoming;
    }

    public void setUpcoming(Upcoming upcoming) {
        this.upcoming = upcoming;
    }

}