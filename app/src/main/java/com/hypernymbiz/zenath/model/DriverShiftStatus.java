package com.hypernymbiz.zenath.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Metis on 31-May-18.
 */

public class DriverShiftStatus {

    @SerializedName("assigned_truck")
    @Expose
    private String assignedTruck;
    @SerializedName("shift_status")
    @Expose
    private Boolean shiftStatus;
    @SerializedName("on_activity")
    @Expose
    private Boolean onActivity;
    @SerializedName("activity_name")
    @Expose
    private Object activityName;

    public String getAssignedTruck() {
        return assignedTruck;
    }

    public void setAssignedTruck(String assignedTruck) {
        this.assignedTruck = assignedTruck;
    }

    public Boolean getShiftStatus() {
        return shiftStatus;
    }

    public void setShiftStatus(Boolean shiftStatus) {
        this.shiftStatus = shiftStatus;
    }

    public Boolean getOnActivity() {
        return onActivity;
    }

    public void setOnActivity(Boolean onActivity) {
        this.onActivity = onActivity;
    }

    public Object getActivityName() {
        return activityName;
    }

    public void setActivityName(Object activityName) {
        this.activityName = activityName;
    }

}