package com.hypernymbiz.zenath.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Metis on 23-Mar-18.
 */

public class JobInfo {

    @SerializedName("assigned_device")
    @Expose
    private String assignedDevice;
    @SerializedName("assigned_device_id")
    @Expose
    private Integer assignedDeviceId;
    @SerializedName("notification_id")
    @Expose
    private Integer notificationId;
    @SerializedName("activity_id")
    @Expose
    private Integer activityId;
    @SerializedName("activity_type")
    @Expose
    private String activityType;
    @SerializedName("activity_status")
    @Expose
    private String activityStatus;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("created_datetime")
    @Expose
    private String createdDatetime;
    @SerializedName("notification_type")
    @Expose
    private Integer notificationType;
    @SerializedName("minutes_ago")
    @Expose
    private String minutesAgo;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @SerializedName("title")
    @Expose
    private String title;


    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("is_viewed")
    @Expose
    private List<IsViewed> isViewed = null;

    public String getAssignedDevice() {
        return assignedDevice;
    }

    public void setAssignedDevice(String assignedDevice) {
        this.assignedDevice = assignedDevice;
    }

    public Integer getAssignedDeviceId() {
        return assignedDeviceId;
    }

    public void setAssignedDeviceId(Integer assignedDeviceId) {
        this.assignedDeviceId = assignedDeviceId;
    }

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(String createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public List<IsViewed> getIsViewed() {
        return isViewed;
    }

    public void setIsViewed(List<IsViewed> isViewed) {
        this.isViewed = isViewed;
    }

    public Integer getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(Integer notificationType) {
        this.notificationType = notificationType;
    }

    public String getMinutesAgo() {
        return minutesAgo;
    }

    public void setMinutesAgo(String minutesAgo) {
        this.minutesAgo = minutesAgo;
    }


}