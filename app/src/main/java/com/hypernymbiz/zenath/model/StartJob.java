package com.hypernymbiz.zenath.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Metis on 27-Mar-18.
 */

public class StartJob {

    @SerializedName("Operation_Successful")
    @Expose
    private Boolean Operation_Successful;

    public Boolean getOperationSuccessful() {
        return Operation_Successful;
    }

    public void setOperationSuccessful(Boolean Operation_Successful) {
        this.Operation_Successful = Operation_Successful;
    }

}