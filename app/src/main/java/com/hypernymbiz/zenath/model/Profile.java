package com.hypernymbiz.zenath.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Metis on 23-Mar-18.
 */

public class Profile {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("customer")
    @Expose
    private Integer customer;
    @SerializedName("assignments")
    @Expose
    private Integer assignments;
    @SerializedName("module")
    @Expose
    private Integer module;
    @SerializedName("module_name")
    @Expose
    private String moduleName;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("status_label")
    @Expose
    private String statusLabel;
    @SerializedName("modified_by")
    @Expose
    private Integer modifiedBy;
    @SerializedName("modified_by_name")
    @Expose
    private String modifiedByName;
    @SerializedName("modified_by_email")
    @Expose
    private String modifiedByEmail;
    @SerializedName("created_datetime")
    @Expose
    private String createdDatetime;
    @SerializedName("modified_datetime")
    @Expose
    private String modifiedDatetime;
    @SerializedName("end_datetime")
    @Expose
    private Object endDatetime;
    @SerializedName("cnic")
    @Expose
    private String cnic;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("date_of_joining")
    @Expose
    private String dateOfJoining;
    @SerializedName("salary")
    @Expose
    private Integer salary;
    @SerializedName("marital_status")
    @Expose
    private Integer maritalStatus;
    @SerializedName("marital_status_label")
    @Expose
    private String maritalStatusLabel;
    @SerializedName("gender")
    @Expose
    private Integer gender;
    @SerializedName("gender_label")
    @Expose
    private String genderLabel;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("photo_method")
    @Expose
    private String photoMethod;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("driver_email")
    @Expose
    private String driverEmail;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCustomer() {
        return customer;
    }

    public void setCustomer(Integer customer) {
        this.customer = customer;
    }

    public Integer getAssignments() {
        return assignments;
    }

    public void setAssignments(Integer assignments) {
        this.assignments = assignments;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedByName() {
        return modifiedByName;
    }

    public void setModifiedByName(String modifiedByName) {
        this.modifiedByName = modifiedByName;
    }

    public String getModifiedByEmail() {
        return modifiedByEmail;
    }

    public void setModifiedByEmail(String modifiedByEmail) {
        this.modifiedByEmail = modifiedByEmail;
    }

    public String getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(String createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public String getModifiedDatetime() {
        return modifiedDatetime;
    }

    public void setModifiedDatetime(String modifiedDatetime) {
        this.modifiedDatetime = modifiedDatetime;
    }

    public Object getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Object endDatetime) {
        this.endDatetime = endDatetime;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDateOfJoining() {
        return dateOfJoining;
    }

    public void setDateOfJoining(String dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(Integer maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMaritalStatusLabel() {
        return maritalStatusLabel;
    }

    public void setMaritalStatusLabel(String maritalStatusLabel) {
        this.maritalStatusLabel = maritalStatusLabel;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getGenderLabel() {
        return genderLabel;
    }

    public void setGenderLabel(String genderLabel) {
        this.genderLabel = genderLabel;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhotoMethod() {
        return photoMethod;
    }

    public void setPhotoMethod(String photoMethod) {
        this.photoMethod = photoMethod;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDriverEmail() {
        return driverEmail;
    }

    public void setDriverEmail(String driverEmail) {
        this.driverEmail = driverEmail;
    }

}
