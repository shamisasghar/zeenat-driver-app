package com.hypernymbiz.zenath.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Metis on 28-Mar-18.
 */

public class JobEnd  {

    @SerializedName("job_name")
    @Expose
    private String jobName;
    @SerializedName("activity_start_time")
    @Expose
    private String activityStartTime;
    @SerializedName("activity_end_time")
    @Expose
    private String activityEndTime;
    @SerializedName("activity_volume_consumed")
    @Expose
    private Integer activityVolumeConsumed;
    @SerializedName("activity_distance")
    @Expose
    private Integer activityDistance;
    @SerializedName("activity_start_lat")
    @Expose
    private Integer activityStartLat;
    @SerializedName("activity_start_lng")
    @Expose
    private Integer activityStartLng;
    @SerializedName("activity_end_lat")
    @Expose
    private Integer activityEndLat;
    @SerializedName("activity_end_lng")
    @Expose
    private Integer activityEndLng;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getActivityStartTime() {
        return activityStartTime;
    }

    public void setActivityStartTime(String activityStartTime) {
        this.activityStartTime = activityStartTime;
    }

    public String getActivityEndTime() {
        return activityEndTime;
    }

    public void setActivityEndTime(String activityEndTime) {
        this.activityEndTime = activityEndTime;
    }

    public Integer getActivityVolumeConsumed() {
        return activityVolumeConsumed;
    }

    public void setActivityVolumeConsumed(Integer activityVolumeConsumed) {
        this.activityVolumeConsumed = activityVolumeConsumed;
    }

    public Integer getActivityDistance() {
        return activityDistance;
    }

    public void setActivityDistance(Integer activityDistance) {
        this.activityDistance = activityDistance;
    }

    public Integer getActivityStartLat() {
        return activityStartLat;
    }

    public void setActivityStartLat(Integer activityStartLat) {
        this.activityStartLat = activityStartLat;
    }

    public Integer getActivityStartLng() {
        return activityStartLng;
    }

    public void setActivityStartLng(Integer activityStartLng) {
        this.activityStartLng = activityStartLng;
    }

    public Integer getActivityEndLat() {
        return activityEndLat;
    }

    public void setActivityEndLat(Integer activityEndLat) {
        this.activityEndLat = activityEndLat;
    }

    public Integer getActivityEndLng() {
        return activityEndLng;
    }

    public void setActivityEndLng(Integer activityEndLng) {
        this.activityEndLng = activityEndLng;
    }

}
