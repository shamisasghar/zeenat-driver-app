package com.hypernymbiz.zenath.model;

/**
 * Created by Metis on 27-Mar-18.
 */

public class PayloadNotification {
    public int job_id;
    public int notification_type;
    public String title;
    public String message;

}
