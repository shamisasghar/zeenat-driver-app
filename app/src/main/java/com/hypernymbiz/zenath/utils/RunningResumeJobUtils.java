package com.hypernymbiz.zenath.utils;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hypernymbiz.zenath.model.ActionItem;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Metis on 21-Apr-18.
 */

public class RunningResumeJobUtils {
    public static void jobResumed(Context context) {
        PrefUtils.persistBoolean(context, Constants.JOB_RESUME_LIST_STATUS, true);
    }
    public static boolean isJobResumed(Context context) {
        return PrefUtils.getBoolean(context, Constants.JOB_RESUME_LIST_STATUS, false);
    }

    public static void clearJobResumed(Context context) {
        PrefUtils.remove(context, Constants.JOB_RESUME_LIST_STATUS);
        PrefUtils.remove(context, Constants.JOB_LIST);
        PrefUtils.remove(context,Constants.JOB_ID);
        PrefUtils.remove(context,Constants.JOB_PERCENT);
    }

    public static void saveJobResume(Context context,String jobid, List<ActionItem> job, String dump_name, String dump_latlng,String sort_name,String sort_latlng) {
        if (job == null)
            return;

        PrefUtils.persistString(context, Constants.JOB_LIST, GsonUtils.toJson(job));
        PrefUtils.persistString(context, Constants.JOB_ID, jobid);
        PrefUtils.persistString(context, Constants.DUMP_NAME, dump_name);
        PrefUtils.persistString(context, Constants.DUMP_LATLNG, dump_latlng);
        PrefUtils.persistString(context, Constants.SORT_NAME, sort_name);
        PrefUtils.persistString(context, Constants.SORT_LATLNG, sort_latlng);
    }
    public static void savepercent(Context context,String percent) {
        if (percent == null)
            return;
        PrefUtils.persistString(context, Constants.JOB_PERCENT, percent);

    }

    public static List<ActionItem> getJobResume(Context context) {
        Type type = new TypeToken<List<ActionItem>>(){}.getType();
         return new Gson().fromJson(PrefUtils.getString(context, Constants.JOB_LIST), type);
//        return GsonUtils.fromJson(, ActionItem.class);
    }
    public static String getJobId(Context context){
        return PrefUtils.getString(context, Constants.JOB_ID);
    }

    public static String getDumpName(Context context){
        return PrefUtils.getString(context, Constants.DUMP_NAME);
    }
    public static String getDumpLatLng(Context context){
        return PrefUtils.getString(context, Constants.DUMP_LATLNG);
    }

    public static String getSortName(Context context){
        return PrefUtils.getString(context, Constants.SORT_NAME);
    }
    public static String getSortLatLng(Context context){
        return PrefUtils.getString(context, Constants.SORT_LATLNG);
    }
    public static String getJobPercent(Context context){
        return PrefUtils.getString(context, Constants.JOB_PERCENT);
    }
}
