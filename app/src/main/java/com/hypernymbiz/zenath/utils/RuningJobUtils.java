package com.hypernymbiz.zenath.utils;

import android.content.Context;

import com.hypernymbiz.zenath.model.RuningJobStatus;

/**
 * Created by Metis on 15-Apr-18.
 */

public class RuningJobUtils {
//    public static void saveJobStatus(Context context, RuningJobStatus runingJobStatus) {
//        if (runingJobStatus == null)
//            return;
//
//        PrefUtils.persistString(context, Constants.JOB_RUNNING_STATUS, GsonUtils.toJson(runingJobStatus));
//    }

    public static void saveBtnLeft(Context context, String btnLeft)
    {
         PrefUtils.persistString(context,Constants.BTN_LEFT,btnLeft);
    }

    public static String getBtnLeft(Context context) {
        return PrefUtils.getString(context, Constants.BTN_LEFT);
    }

    public static void saveBtnRight(Context context, String btnRight)
    {
         PrefUtils.persistString(context,Constants.BTN_RIGHT,btnRight);
    }

    public static String getBtnRight(Context context) {
        return PrefUtils.getString(context, Constants.BTN_RIGHT);
    }

    public static void saveBtncomplete(Context context, String btnComplete)
    {
        PrefUtils.persistString(context,Constants.BTN_COMPLETE,btnComplete);
    }

    public static String getBtncomplete(Context context) {
        return PrefUtils.getString(context, Constants.BTN_COMPLETE);
    }

    public static void savestatus(Context context, String status)
    {
        PrefUtils.persistString(context,Constants.JOB_STATUS,status);
    }

    public static String getstatus(Context context) {
        return PrefUtils.getString(context, Constants.JOB_STATUS);
    }

    public static void clearJobbtnstatus(Context context) {
        PrefUtils.remove(context, Constants.BTN_COMPLETE);
        PrefUtils.remove(context, Constants.JOB_STATUS);
        PrefUtils.remove(context,Constants.BTN_RIGHT);

    }
}
