package com.hypernymbiz.zenath.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hypernymbiz.zenath.FrameActivity;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.adapter.JobDetailAdapter;
import com.hypernymbiz.zenath.api.ApiInterface;
import com.hypernymbiz.zenath.dialog.LoadingDialog;
import com.hypernymbiz.zenath.dialog.NotesDialog;
import com.hypernymbiz.zenath.dialog.PermissionDialog;
import com.hypernymbiz.zenath.model.ActionItem;
import com.hypernymbiz.zenath.model.DirectionsJSONParser;
import com.hypernymbiz.zenath.model.Lastjob;
import com.hypernymbiz.zenath.model.WebAPIResponse;
import com.hypernymbiz.zenath.toolbox.ToolbarListener;
import com.hypernymbiz.zenath.utils.ActivityUtils;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;
import com.hypernymbiz.zenath.utils.GsonUtils;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Metis on 09-May-18.
 */

public class LastActivityFragment extends Fragment implements com.google.android.gms.location.LocationListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    View view;
    String getUserAssociatedEntity;
    LoadingDialog dialog;
    TextView txt_assign_truck, txt_schedule_type, txt_activitytime, activity_status;
    RecyclerView recyclerView;
    private PermissionDialog permissionDialog;
    SupportMapFragment supportMapFragment;
    ImageButton mylocation;
    private long UPDATE_INTERVAL = 1000;  /* 1 sec */
    private long FASTEST_INTERVAL = 500; /* 1/2 sec */
    public static int counter = 0;
    GoogleMap googleMap;
    GoogleApiClient googleApiClient;
    private LocationRequest mLocationRequest;
    Marker marker;
    boolean checklocation = true;
    String driverlocation;
    LatLng ll;
    Context fContext;
    boolean check = true;
    int marker_height = 200;
    int marker_width = 200;
    private List<ActionItem> actionItems;
    JobDetailAdapter jobDetailAdapter;
    NotesDialog notesDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("Last Activity");
        }
        fContext = context;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_last_activity, container, false);

        txt_assign_truck = (TextView) view.findViewById(R.id.txt_assigned_truck);
        txt_schedule_type = (TextView) view.findViewById(R.id.txt_end_point);
        activity_status = (TextView) view.findViewById(R.id.txt_activity_status);
        txt_activitytime = (TextView) view.findViewById(R.id.txt_activity_time);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_bins);
        recyclerView.setLayoutManager(getLinearLayoutManager());
        recyclerView.setHasFixedSize(true);
        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
        mylocation = (ImageButton) view.findViewById(R.id.btn_mylocation);
        buildGoogleApiClient();
        initMap();
        startLocationUpdates();


        dialog = new LoadingDialog(getActivity(), getString(R.string.msg_loading));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        LocationCheck();


        ApiInterface.retrofit.getlastjob().enqueue(new Callback<WebAPIResponse<Lastjob>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<Lastjob>> call, Response<WebAPIResponse<Lastjob>> response) {

                try {
                    if (response.body().status == 200) {
                        txt_activitytime.setText(AppUtils.getTime(response.body().response.getActivityTime()));
                        txt_assign_truck.setText(response.body().response.getAssignedTruck());
                        txt_schedule_type.setText(response.body().response.getScheduleType());
                        activity_status.setText(response.body().response.getActivityStatus());
                        actionItems = response.body().response.getActionItems();
                        jobDetailAdapter = new JobDetailAdapter(actionItems, getContext());
                        recyclerView.setAdapter(jobDetailAdapter);

                    }

                } catch (Exception ex) {

                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse<Lastjob>> call, Throwable t) {
                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

            }
        });

        return view;
    }

    private void initMap() {
        supportMapFragment.getMapAsync(this);

    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();
    }


    @SuppressLint("RestrictedApi")
    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        final LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.getFusedLocationProviderClient(getActivity()).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        counter++;
                        try {
                            if (counter > 1) {
                                onLocationChanged(locationResult.getLastLocation());
                                LocationServices.getFusedLocationProviderClient(getActivity()).removeLocationUpdates(this);
                            }
                        } catch (Exception ex) {

                        }
                    }
                },
                Looper.myLooper());
    }


    private LinearLayoutManager getLinearLayoutManager() {

        return new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        final MarkerOptions option;
        if (marker != null) {

            marker.remove();
        }

        ll = new LatLng(location.getLatitude(), location.getLongitude());


        mylocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 12.8f);
                googleMap.animateCamera(update);
            }
        });

        Double lat, lng;

        lat = location.getLatitude();
        lng = location.getLongitude();


        try {
            option = new MarkerOptions().title("Driver").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location)).position(new LatLng(lat, lng));
            marker = googleMap.addMarker(option);
        } catch (Exception ex) {

        }

        if (googleMap != null) {

            if (checklocation == true) {
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 12.8f);
                googleMap.animateCamera(update);
                checklocation = false;

            }
        }


        driverlocation = lat.toString() + "," + lng.toString();
        dialog.dismiss();
        Log.e("TAAAG", "unshow");
//        Toast.makeText(getContext(), "" + driverlocation, Toast.LENGTH_SHORT).show();
        supportMapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;


        MapStyleOptions mapStyleOptions = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map);
        googleMap.setMapStyle(mapStyleOptions);


        ApiInterface.retrofit.getlastjob().enqueue(new Callback<WebAPIResponse<Lastjob>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<Lastjob>> call, Response<WebAPIResponse<Lastjob>> response) {

                try {
                    if (response.isSuccessful()) {
                        actionItems = response.body().response.getActionItems();
                        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_bin_marker);
                        Bitmap b = bitmapdraw.getBitmap();
                        Bitmap smallMarker = Bitmap.createScaledBitmap(b, marker_width, marker_height, false);

                        Log.e("TAAAG", "" + GsonUtils.toJson(actionItems));

                        if (actionItems.size() > 1) {
                            for (int i = 0; i < actionItems.size() - 1; i++) {
                                try {


                                    String array1[] = actionItems.get(i).getEntityLocation().split(",");
                                    LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));
                                    String startbin = actionItems.get(i).getLabel();
                                    String array2[] = actionItems.get(i + 1).getEntityLocation().split(",");
                                    LatLng dest = new LatLng(Double.parseDouble(array2[0]), Double.parseDouble(array2[1]));
                                    String endbin = actionItems.get(i + 1).getLabel();

                                    googleMap.addMarker(new MarkerOptions().title(startbin).position(start).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));
                                    googleMap.addMarker(new MarkerOptions().title(endbin).position(dest).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));

                                    String url = getDirectionsUrl(start, dest);
                                    FetchUrl FetchUrl = new FetchUrl();
                                    FetchUrl.execute(url);

                                } catch (Exception ex) {
                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                    break;

                                }
                            }
                        } else {
                            try {
                                String array1[] = actionItems.get(0).getEntityLocation().split(",");
                                LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));
                                String startbin = actionItems.get(0).getLabel();

                                googleMap.addMarker(new MarkerOptions().title(startbin).position(start).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));


                            } catch (Exception ex) {
                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                            }
                            // functionality if lenght is one
                        }
//lastone

                        BitmapDrawable dumpmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_dump_marker);
                        Bitmap dump = dumpmarker.getBitmap();
                        Bitmap Dumpmarker = Bitmap.createScaledBitmap(dump, marker_width, marker_height, false);

                        BitmapDrawable sortmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_sort_marker);
                        Bitmap sort = sortmarker.getBitmap();
                        Bitmap Sortmarker = Bitmap.createScaledBitmap(sort, marker_width, marker_height, false);

                        try {


                            String dumpname = response.body().response.getEndPointName().toString();
                       //     String sortname = response.body().response.getCheckPointName().toString();

                            String dumpArray[] = response.body().response.getEndPointLatLong().toString().split(",");
                        //    String sortArray[] = response.body().response.getCheckPointLatLong().toString().split(",");


//                            if(sortname==null&&response.body().response.getCheckPointLatLong()==null)
//                            {
//                                LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
//                                // LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));
//
//                                String lastBin[] = actionItems.get(actionItems.size() - 1).getEntityLocation().split(",");
//
//                                //  googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));
//                                googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));
//
//                                String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])), dumping);
//                                FetchUrl FetchUrl = new FetchUrl();
//                                FetchUrl.execute(url);
//
////                                        String urll = getDirectionsUrl(sorting, dumping);
////                                        FetchUrl FetchUrll = new FetchUrl();
////                                        FetchUrll.execute(urll);
////
//                            }
//                            else {

                                LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
                            //    LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));

                                String lastBin[] = actionItems.get(actionItems.size() - 1).getEntityLocation().split(",");

                              //  googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));
                                googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));

                                String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])), dumping);
                                FetchUrl FetchUrl = new FetchUrl();
                                FetchUrl.execute(url);
//
//                                String urlll = getDirectionsUrl(sorting, dumping);
//                                FetchUrl FetchUrlll = new FetchUrl();
//                                FetchUrlll.execute(urlll);
//                            }


                        } catch (Exception ex) {

                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                        }

                    }
                } catch (Exception ex) {

                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse<Lastjob>> call, Throwable t) {
                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        } catch (Exception ex) {
//            Toast.makeText(getContext(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private String getDirectionsUrl(LatLng start, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + start.latitude + "," + start.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        } catch (Exception e) {
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {

            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;

            PolylineOptions lineOptions = null;
            if (result != null) {

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<>();
                    lineOptions = new PolylineOptions();
                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);
                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(10);
                    lineOptions.color(R.color.colorPrimary);
                }

                // Drawing polyline in the Google Map for the i-th route
                if (lineOptions != null) {
                    googleMap.addPolyline(lineOptions);
                } else {
                }
            } else {

                if (check == true) {
                }
                check = false;

            }
        }
    }

    public void LocationCheck()
    {
        final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                    , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.btn_dialog_ok:
                            permissionDialog.dismiss();
                            ActivityUtils.startWifiSettings(getContext());
                            break;
                    }
                }
            });
            permissionDialog.setCancelable(false);
            permissionDialog.setCanceledOnTouchOutside(false);
            permissionDialog.show();
            return;
        }

    }

}
