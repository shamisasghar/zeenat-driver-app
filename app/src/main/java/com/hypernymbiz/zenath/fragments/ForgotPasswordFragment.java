package com.hypernymbiz.zenath.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.hypernymbiz.zenath.LoginActivity;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.api.ApiInterface;
import com.hypernymbiz.zenath.model.WebApiRespone_profile;
import com.hypernymbiz.zenath.toolbox.ToolbarListener;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;
import com.hypernymbiz.zenath.utils.LoginUtils;
import com.onesignal.OneSignal;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Metis on 09-May-18.
 */

public class ForgotPasswordFragment extends Fragment implements View.OnClickListener {
    Context fContext;
    Button btn_proceed;
    TextInputEditText edit_email;
    private TextInputLayout inputLayout_username;

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("Forgot Password");
        }
        fContext = context;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);

        btn_proceed=(Button)view.findViewById(R.id.button_proceed);
        edit_email=(TextInputEditText) view.findViewById(R.id.edit_text_email);
        inputLayout_username=(TextInputLayout) view.findViewById(R.id.input_layout_email);


        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Submit();

            }
        });

        return  view;
    }

    public void Submit()
    {

        if (!validateName())
            return;

        String Email=edit_email.getText().toString();

        HashMap<String, Object> body = new HashMap<>();
        body.put("email", Email);

        ApiInterface.retrofit.forgetpassword(body).enqueue(new Callback<WebApiRespone_profile>() {
            @Override
            public void onResponse(Call<WebApiRespone_profile> call, Response<WebApiRespone_profile> response) {

                try {
                    if (response.isSuccessful()) {

                        Toast.makeText(getContext(), "Check Your Email ! ", Toast.LENGTH_SHORT).show();
                        getActivity().finish();

                    }
                    else {
                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));


                    }
                } catch (Exception ex) {
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }

            @Override
            public void onFailure(Call<WebApiRespone_profile> call, Throwable t) {

                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

            }
        });


    }


    private boolean validateName() {
        if (edit_email.getText().toString().trim().isEmpty()) {
            inputLayout_username.setError(getString(R.string.err_msg_name));
            //requestFocus(edit_email);
            return false;
        } else {
            inputLayout_username.setErrorEnabled(false);
        }

        return true;
    }





}
