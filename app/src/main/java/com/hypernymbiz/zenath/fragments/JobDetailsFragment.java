package com.hypernymbiz.zenath.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hypernymbiz.zenath.FrameActivity;
import com.hypernymbiz.zenath.HomeActivity;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.adapter.DialogAdapter;
import com.hypernymbiz.zenath.adapter.JobDetailAdapter;
import com.hypernymbiz.zenath.api.ApiInterface;
import com.hypernymbiz.zenath.dialog.LoadingDialog;
import com.hypernymbiz.zenath.dialog.NotesDialog;
import com.hypernymbiz.zenath.dialog.PermissionDialog;
import com.hypernymbiz.zenath.dialog.RequestDialog;
import com.hypernymbiz.zenath.dialog.SimpleDialog;
import com.hypernymbiz.zenath.model.ActionItem;
import com.hypernymbiz.zenath.model.BinData;
import com.hypernymbiz.zenath.model.DirectionsJSONParser;
import com.hypernymbiz.zenath.model.JobDetail;
import com.hypernymbiz.zenath.model.Orientation;
import com.hypernymbiz.zenath.model.PayloadNotification;
import com.hypernymbiz.zenath.model.StartJob;
import com.hypernymbiz.zenath.model.WebAPIResponse;
import com.hypernymbiz.zenath.toolbox.OnDialogButtonClick;
import com.hypernymbiz.zenath.toolbox.ToolbarListener;
import com.hypernymbiz.zenath.utils.ActivityUtils;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;
import com.hypernymbiz.zenath.utils.GsonUtils;
import com.hypernymbiz.zenath.utils.LoginUtils;
import com.hypernymbiz.zenath.utils.RunningResumeJobUtils;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Metis on 21-Mar-18.
 */

public class JobDetailsFragment extends Fragment implements View.OnClickListener, com.google.android.gms.location.LocationListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    RecyclerView recyclerView;
    JobDetailAdapter jobDetailAdapter;
    private List<ActionItem> actionItems;
    private String DumpName, SortfacilityName;
    private String DumpLatLng, SortfacilityLatlng;

    Boolean check = true;
    int marker_height = 200;
    int marker_width = 200;
    SupportMapFragment supportMapFragment;
    View view;
    Context fContext;
    Button btn_start, btn_cancel, btn_dialog;
    TextView jbname, activitystatus, jbstart, scheduledtype, activity_assigned_truck;
    String getUserAssociatedEntity, actual_start_time;
    private SwipeRefreshLayout swipelayout;
    PayloadNotification payloadNotification;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    Calendar c;
    private LocationRequest mLocationRequest;
    String driverlocation;
    LatLng ll;
    LoadingDialog dialog;
    GoogleMap googleMap;
    GoogleApiClient googleApiClient;
    Marker marker;
    boolean checklocation = true;
    ImageButton mylocation;
    private PermissionDialog permissionDialog;
    private RequestDialog requestDialog;
    private long UPDATE_INTERVAL = 1000;  /* 1 sec */
    private long FASTEST_INTERVAL = 500; /* 1/2 sec */
    public static int counter = 0;
    NotesDialog notesDialog;

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("Job Details");
        }
        fContext = context;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job_detials, container, false);
        btn_start = (Button) view.findViewById(R.id.btn_startjob);
        btn_cancel = (Button) view.findViewById(R.id.btn_canceljob);
        jbname = (TextView) view.findViewById(R.id.txt_activity_name);
        activitystatus = (TextView) view.findViewById(R.id.txt_activity_status);
        activity_assigned_truck = (TextView) view.findViewById(R.id.txt_assigned_truck);
        jbstart = (TextView) view.findViewById(R.id.txt_activity_time);
        scheduledtype = (TextView) view.findViewById(R.id.txt_scheduled_type);
        mylocation = (ImageButton) view.findViewById(R.id.btn_mylocation);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_bins);
//        layoutManager = new LinearLayoutManager(getContext(), getLinearLayoutManager());
        recyclerView.setLayoutManager(getLinearLayoutManager());
        recyclerView.setHasFixedSize(true);

        dialog = new LoadingDialog(getActivity(), getString(R.string.msg_loading));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        LocationCheck();


        getUserAssociatedEntity = LoginUtils.getUserAssociatedEntity(getActivity());
        pref = getActivity().getSharedPreferences("TAG", MODE_PRIVATE);
        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);

        buildGoogleApiClient();
        initMap();

        //   Toast.makeText(getContext(), "" + driverlocation, Toast.LENGTH_SHORT).show();

//        if(driverlocation==null)
//        {
//           dialog.show();
//        }


        startLocationUpdates();

        btn_start.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);

        c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        actual_start_time = df.format(c.getTime());


        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        outputFmt.setTimeZone(TimeZone.getTimeZone("gmt"));
        actual_start_time = outputFmt.format(new Date());

        //    Toast.makeText(getContext(), "" + actual_start_time, Toast.LENGTH_SHORT).show();

        btn_start.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    btn_start.setBackgroundColor(Color.parseColor("#36AB7A"));
                    return false;
                } else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    btn_start.setBackgroundColor(Color.parseColor("#FF2D8C64"));
                }

                return false;
            }
        });

        btn_cancel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    btn_cancel.setBackgroundColor(Color.parseColor("#f44336"));
                    return false;
                } else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    btn_cancel.setBackgroundColor(Color.parseColor("#bf392f"));
                }

                return false;
            }
        });

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                            , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (view.getId()) {
                                case R.id.btn_dialog_ok:
                                    permissionDialog.dismiss();
                                    ActivityUtils.startWifiSettings(getContext());
                                    break;
                            }
                        }
                    });
                    permissionDialog.setCancelable(false);
                    permissionDialog.setCanceledOnTouchOutside(false);
                    permissionDialog.show();
                    return;
                } else {

                    requestDialog = new RequestDialog(getContext(), getString(R.string.title_start),
                            getString(R.string.msg_start), new OnDialogButtonClick() {
                        @Override
                        public void onButtonClick(View view, String data) {

                            switch (view.getId()) {
                                case R.id.img_ok:
                                    dialog.show();
                                    Intent getintent = getActivity().getIntent();
                                    String id = getintent.getStringExtra("jobid");
                                    String getremark = data;

                                    HashMap<String, Object> body = new HashMap<>();
                                    if (id != null) {
                                        body.put("job_id", Integer.parseInt(id));
                                        body.put("timestamp", actual_start_time);
                                        body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                        body.put("flag", 94);
                                        body.put("lat_long", driverlocation);
                                        body.put("remarks", getremark);
                                    } else {
                                        body.put("job_id", payloadNotification.job_id);
                                        body.put("timestamp", actual_start_time);
                                        body.put("flag", 94);
                                        body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                        body.put("lat_long", driverlocation);
                                        body.put("remarks", getremark);
                                    }

                                    ApiInterface.retrofit.startjob(body).enqueue(new Callback<WebAPIResponse<StartJob>>() {
                                        @Override
                                        public void onResponse(Call<WebAPIResponse<StartJob>> call, Response<WebAPIResponse<StartJob>> response) {
                                            //  dialog.dismiss();
//                                            if (response.isSuccessful()) {
                                            try {

                                                if (response.body().status == 200) {

                                                    Intent getJobintent = getActivity().getIntent();
                                                    String jobid = getJobintent.getStringExtra("jobid");

                                                    RunningResumeJobUtils.saveJobResume(getContext(), jobid, actionItems, DumpName, DumpLatLng, SortfacilityName, SortfacilityLatlng);
                                                    RunningResumeJobUtils.jobResumed(getContext());

                                                    Intent getintent = getActivity().getIntent();
                                                    String id = getintent.getStringExtra("jobid");

                                                    if (id != null) {
                                                        editor = pref.edit();
                                                        editor.putString("jobid", id);
//                                        editor.putString("drivertime", actual_start_time);
//                                        editor.putString("startlocation",driverlocation);
                                                        editor.commit();

                                                        ActivityUtils.startActivity(getActivity(), FrameActivity.class, RunningJobFragment.class.getName(), null);
                                                        getActivity().finish();
                                                        dialog.dismiss();
                                                    } else {
                                                        editor = pref.edit();
                                                        editor.putString("jobid", "" + payloadNotification.job_id);
//                                        editor.putString("driverlocation",driverlocation);

//                                        editor.putString("drivertime", actual_start_time);
                                                        editor.commit();
                                                        Toast.makeText(getContext(), String.valueOf(payloadNotification.job_id), Toast.LENGTH_SHORT).show();
                                                        ActivityUtils.startActivity(getActivity(), FrameActivity.class, RunningJobFragment.class.getName(), null);
                                                        getActivity().finish();
                                                    }
                                                } else if (response.body().status == 400) {
                                                    dialog.dismiss();
                                                    requestDialog.dismiss();
                                                    notesDialog = new NotesDialog(getContext(), "ACTIVITY FAILED", "" + response.body().message, "OK", new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            switch (view.getId()) {
                                                                case R.id.button_positive: {
                                                                    FrameActivity frameActivity = (FrameActivity) getActivity();
                                                                    frameActivity.onBackPressed();
                                                                    notesDialog.dismiss();

                                                                }

                                                            }

                                                        }
                                                    });
                                                    notesDialog.show();


                                                } else {
                                                    dialog.dismiss();
                                                    requestDialog.dismiss();
                                                    notesDialog = new NotesDialog(getContext(), "START SHIFT", "" + response.body().message, "OK", new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            switch (view.getId()) {
                                                                case R.id.button_positive: {
                                                                    notesDialog.dismiss();

                                                                }

                                                            }

                                                        }
                                                    });
                                                    notesDialog.show();
                                                }

                                            } catch (Exception ex) {
                                                dialog.dismiss();
                                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                                //        Toast.makeText(getContext(), "error", Toast.LENGTH_SHORT).show();
                                                ex.printStackTrace();

                                            }
//
                                        }

                                        @Override
                                        public void onFailure(Call<WebAPIResponse<StartJob>> call, Throwable t) {
                                            dialog.dismiss();
                                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                        }
                                    });

                                    break;
                                case R.id.img_cancel:
                                    requestDialog.dismiss();
                                    // Toast.makeText(getContext(), "" + data, Toast.LENGTH_SHORT).show();
                                    break;

                            }
                        }
                    });
                    requestDialog.show();

                }
            }
        });


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                            , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (view.getId()) {
                                case R.id.btn_dialog_ok:
                                    permissionDialog.dismiss();
                                    ActivityUtils.startWifiSettings(getContext());
                                    break;
                            }
                        }
                    });
                    permissionDialog.setCancelable(false);
                    permissionDialog.setCanceledOnTouchOutside(false);
                    permissionDialog.show();
                    return;
                } else {


                    requestDialog = new RequestDialog(getContext(), getString(R.string.title_abort),
                            getString(R.string.msg_abort), new OnDialogButtonClick() {
                        @Override
                        public void onButtonClick(View view, String data) {

                            switch (view.getId()) {
                                case R.id.img_ok:

                                    Intent getintent = getActivity().getIntent();
                                    String id = getintent.getStringExtra("jobid");
                                    String getremark = data;

                                    HashMap<String, Object> body = new HashMap<>();
                                    if (id != null) {
                                        body.put("job_id", Integer.parseInt(id));
                                        body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                        body.put("timestamp", actual_start_time);
                                        body.put("flag", 117);
                                        body.put("lat_long", driverlocation);
                                        body.put("remarks", getremark);


                                    } else {
                                        body.put("job_id", payloadNotification.job_id);
                                        body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                        body.put("timestamp", actual_start_time);
                                        body.put("flag", 117);
                                        body.put("lat_long", driverlocation);
                                        body.put("remarks", getremark);

                                    }


                                    ApiInterface.retrofit.canceljob(body).enqueue(new Callback<WebAPIResponse<StartJob>>() {
                                        @Override
                                        public void onResponse(Call<WebAPIResponse<StartJob>> call, Response<WebAPIResponse<StartJob>> response) {

                                            if (response.isSuccessful()) {
                                                try {
                                                    FrameActivity frameActivity = (FrameActivity) getActivity();
                                                    frameActivity.onBackPressed();

                                                } catch (Exception ex) {
                                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                                }
                                            } else {
                                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<WebAPIResponse<StartJob>> call, Throwable t) {
                                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                        }
                                    });


                                    break;

                                case R.id.img_cancel:
                                    requestDialog.dismiss();
//                                    Toast.makeText(getContext(), "" + data, Toast.LENGTH_SHORT).show();
                                    break;

                            }
                        }
                    });
                    requestDialog.show();

                }

            }
        });

        Bundle extras = getActivity().getIntent().getExtras();
        Bundle value = null;
        if (extras != null) {
            value = extras.getBundle(Constants.DATA);
            if (value != null) {
                payloadNotification = GsonUtils.fromJson(value.getString(Constants.PAYLOAD), PayloadNotification.class);

                ApiInterface.retrofit.getdata(payloadNotification.job_id).enqueue(new Callback<WebAPIResponse<JobDetail>>() {
                    @Override
                    public void onResponse(Call<WebAPIResponse<JobDetail>> call, Response<WebAPIResponse<JobDetail>> response) {
                        if (response.isSuccessful()) {
                            try {
                                if (response.body().status == 200) {

                                    jbname.setText(response.body().response.getActivityType());
                                    jbstart.setText(AppUtils.getTime(response.body().response.getActivityTime()));
                                    scheduledtype.setText(response.body().response.getScheduleType());
                                    activitystatus.setText(response.body().response.getActivityStatus());
                                    activity_assigned_truck.setText(response.body().response.getAssignedTruck());
                                    actionItems = response.body().response.getActionItems();

                                    jobDetailAdapter = new JobDetailAdapter(actionItems, getContext());
                                    recyclerView.setAdapter(jobDetailAdapter);
                                    DumpName = response.body().response.getEndPointName().toString();
                                    DumpLatLng = response.body().response.getEndPointLatLong().toString();
//                                    SortfacilityName = response.body().response.getCheckPointName().toString();
//                                    SortfacilityLatlng = response.body().response.getCheckPointLatLong().toString();


                                    //  jbstart.setText(AppUtils.getFormattedDate(response.body().response.getJobStartDatetime()) + " " + AppUtils.getTimedate(response.body().response.getJobStartDatetime()));
//                                    scheduledtype.setText(AppUtils.getFormattedDate(response.body().response.getJobEndDatetime()) + " " + AppUtils.getTimedate(response.body().response.getJobEndDatetime()));

                                    String status = response.body().response.getActivityStatus();
                                    if (status.equals("Pending") || status.equals("Reviewed")) {
                                        btn_start.setVisibility(View.VISIBLE);
                                        btn_cancel.setVisibility(View.VISIBLE);
                                        btn_start.setText("Accept");
                                        btn_cancel.setText("Reject");
                                        btn_start.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {


                                                final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                                                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                                    permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                                                            , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            switch (view.getId()) {
                                                                case R.id.btn_dialog_ok:
                                                                    permissionDialog.dismiss();
                                                                    ActivityUtils.startWifiSettings(getContext());
                                                                    break;
                                                            }
                                                        }
                                                    });
                                                    permissionDialog.setCancelable(false);
                                                    permissionDialog.setCanceledOnTouchOutside(false);
                                                    permissionDialog.show();
                                                    return;
                                                } else {

                                                    requestDialog = new RequestDialog(getContext(), getString(R.string.title_accept),
                                                            getString(R.string.msg_accept), new OnDialogButtonClick() {
                                                        @Override
                                                        public void onButtonClick(View view, String data) {

                                                            switch (view.getId()) {
                                                                case R.id.img_ok:

                                                                    Intent getintent = getActivity().getIntent();
                                                                    String id = getintent.getStringExtra("jobid");
                                                                    String getremark = data;
                                                                    HashMap<String, Object> body = new HashMap<>();

                                                                    if (id != null) {
                                                                        body.put("job_id", Integer.parseInt(id));
                                                                        body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                                                        body.put("timestamp", actual_start_time);
                                                                        body.put("flag", 74);
                                                                        body.put("lat_long", driverlocation);
                                                                        body.put("remarks", getremark);


                                                                    } else {
                                                                        body.put("job_id", payloadNotification.job_id);
                                                                        body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                                                        body.put("timestamp", actual_start_time);
                                                                        body.put("flag", 74);
                                                                        body.put("lat_long", driverlocation);
                                                                        body.put("remarks", getremark);

                                                                    }
                                                                    ApiInterface.retrofit.canceljob(body).enqueue(new Callback<WebAPIResponse<StartJob>>() {
                                                                        @Override
                                                                        public void onResponse(Call<WebAPIResponse<StartJob>> call, Response<WebAPIResponse<StartJob>> response) {


                                                                            try {
                                                                                if (response.body().status == 200) {
                                                                                    try {
                                                                                        FrameActivity frameActivity = (FrameActivity) getActivity();
                                                                                        frameActivity.onBackPressed();
                                                                                    } catch (Exception ex) {
                                                                                        dialog.dismiss();
                                                                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                                                                    }
                                                                                } else {
                                                                                    dialog.dismiss();
                                                                                    requestDialog.dismiss();
                                                                                    notesDialog = new NotesDialog(getContext(), "ACTIVITY FAILED", "" + response.body().message, "OK", new View.OnClickListener() {
                                                                                        @Override
                                                                                        public void onClick(View view) {

                                                                                            switch (view.getId()) {
                                                                                                case R.id.button_positive: {
                                                                                                    FrameActivity frameActivity = (FrameActivity) getActivity();
                                                                                                    frameActivity.onBackPressed();
                                                                                                    notesDialog.dismiss();

                                                                                                }

                                                                                            }

                                                                                        }
                                                                                    });
                                                                                    notesDialog.show();


                                                                                }

                                                                            } catch (Exception ex) {

                                                                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                                                                            }


                                                                        }

                                                                        @Override
                                                                        public void onFailure(Call<WebAPIResponse<StartJob>> call, Throwable t) {
                                                                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                                                        }
                                                                    });

                                                                case R.id.img_cancel:
                                                                    requestDialog.dismiss();


                                                            }
                                                        }
                                                    });
                                                    requestDialog.show();

                                                }

                                            }

                                        });
                                        btn_cancel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

                                                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                                    permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                                                            , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            switch (view.getId()) {
                                                                case R.id.btn_dialog_ok:
                                                                    permissionDialog.dismiss();
                                                                    ActivityUtils.startWifiSettings(getContext());
                                                                    break;
                                                            }
                                                        }
                                                    });
                                                    permissionDialog.setCancelable(false);
                                                    permissionDialog.setCanceledOnTouchOutside(false);
                                                    permissionDialog.show();
                                                    return;
                                                } else {

                                                    requestDialog = new RequestDialog(getContext(), getString(R.string.title_reject),
                                                            getString(R.string.msg_reject), new OnDialogButtonClick() {
                                                        @Override
                                                        public void onButtonClick(View view, String data) {

                                                            switch (view.getId()) {
                                                                case R.id.img_ok:
                                                                    Intent getintent = getActivity().getIntent();
                                                                    String id = getintent.getStringExtra("jobid");
                                                                    String getremark = data;
                                                                    HashMap<String, Object> body = new HashMap<>();

                                                                    if (id != null) {
                                                                        body.put("job_id", Integer.parseInt(id));
                                                                        body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                                                        body.put("flag", 117);
                                                                        body.put("start_lat_long", driverlocation);
                                                                        body.put("remarks", getremark);

                                                                    } else {
                                                                        body.put("job_id", payloadNotification.job_id);
                                                                        body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                                                        body.put("flag", 117);
                                                                        body.put("start_lat_long", driverlocation);
                                                                        body.put("remarks", getremark);


                                                                    }
                                                                    ApiInterface.retrofit.canceljob(body).enqueue(new Callback<WebAPIResponse<StartJob>>() {
                                                                        @Override
                                                                        public void onResponse(Call<WebAPIResponse<StartJob>> call, Response<WebAPIResponse<StartJob>> response) {

                                                                            try {
                                                                                if (response.body().status == 200) {

                                                                                    FrameActivity frameActivity = (FrameActivity) getActivity();
                                                                                    frameActivity.onBackPressed();

                                                                                }
                                                                            } catch (Exception ex) {
                                                                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                                                            }

                                                                        }

                                                                        @Override
                                                                        public void onFailure(Call<WebAPIResponse<StartJob>> call, Throwable t) {
                                                                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                                                        }
                                                                    });


                                                                case R.id.img_cancel:
                                                                    requestDialog.dismiss();

                                                            }

                                                        }
                                                    });
                                                    requestDialog.show();


                                                }
                                            }
                                        });

                                    } else if (status.equals("Accepted")) {
                                        btn_start.setVisibility(View.VISIBLE);
                                        btn_cancel.setVisibility(View.VISIBLE);
                                        btn_start.setText("Start Job");
                                        btn_cancel.setText("Abort Job");
                                    }

                                }
                            } catch (Exception ex) {

                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));
                                dialog.dismiss();
                            }
                        } else {

                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                            dialog.dismiss();
                        }

                    }

                    @Override
                    public void onFailure(Call<WebAPIResponse<JobDetail>> call, Throwable t) {
                        dialog.dismiss();

                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                    }
                });

            } else {
                Intent getintent = getActivity().getIntent();
                String id = getintent.getStringExtra("jobid");

                ApiInterface.retrofit.getdata(Integer.parseInt(id)).enqueue(new Callback<WebAPIResponse<JobDetail>>() {
                    @Override
                    public void onResponse(Call<WebAPIResponse<JobDetail>> call, Response<WebAPIResponse<JobDetail>> response) {

                        if (response.isSuccessful()) {
                            try {
                                if (response.body().status == 200) {
                                    jbname.setText(response.body().response.getActivityType());
                                    jbstart.setText(AppUtils.getTime(response.body().response.getActivityTime()));
                                    scheduledtype.setText(response.body().response.getScheduleType());
                                    activitystatus.setText(response.body().response.getActivityStatus());
                                    activity_assigned_truck.setText(response.body().response.getAssignedTruck());
                                    actionItems = response.body().response.getActionItems();
                                    jobDetailAdapter = new JobDetailAdapter(actionItems, getContext());
                                    recyclerView.setAdapter(jobDetailAdapter);
                                    DumpName = response.body().response.getEndPointName().toString();
                                    DumpLatLng = response.body().response.getEndPointLatLong().toString();
//                                    SortfacilityName = response.body().response.getCheckPointName().toString();
//                                    SortfacilityLatlng = response.body().response.getCheckPointLatLong().toString();

                                    String status = response.body().response.getActivityStatus();
                                    if (status != null) {
//                                        if (status.equals("Pending")||status.equals("Accepted")) {
//                                            btn_start.setVisibility(View.VISIBLE);
//                                            btn_cancel.setVisibility(View.VISIBLE);
//                                        }
                                        if (status.equals("Pending") || status.equals("Reviewed")) {
                                            btn_start.setVisibility(View.VISIBLE);
                                            btn_cancel.setVisibility(View.VISIBLE);
                                            btn_start.setText("Accept");
                                            btn_cancel.setText("Reject");
                                            btn_start.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {

                                                    final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

                                                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                                        permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                                                                , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                switch (view.getId()) {
                                                                    case R.id.btn_dialog_ok:
                                                                        permissionDialog.dismiss();
                                                                        ActivityUtils.startWifiSettings(getContext());
                                                                        break;
                                                                }
                                                            }
                                                        });
                                                        permissionDialog.setCancelable(false);
                                                        permissionDialog.setCanceledOnTouchOutside(false);
                                                        permissionDialog.show();
                                                        return;
                                                    } else {

                                                        requestDialog = new RequestDialog(getContext(), getString(R.string.title_accept),
                                                                getString(R.string.msg_accept), new OnDialogButtonClick() {
                                                            @Override
                                                            public void onButtonClick(View view, String data) {

                                                                switch (view.getId()) {
                                                                    case R.id.img_ok:

                                                                        Intent getintent = getActivity().getIntent();
                                                                        String id = getintent.getStringExtra("jobid");
                                                                        String getremark = data;
                                                                        HashMap<String, Object> body = new HashMap<>();
                                                                        if (id != null) {
                                                                            body.put("job_id", Integer.parseInt(id));
                                                                            body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                                                            body.put("timestamp", actual_start_time);
                                                                            body.put("flag", 74);
                                                                            body.put("lat_long", driverlocation);
                                                                            body.put("remarks", getremark);


                                                                        } else {
                                                                            body.put("job_id", payloadNotification.job_id);
                                                                            body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                                                            body.put("timestamp", actual_start_time);

                                                                            body.put("flag", 74);
                                                                            body.put("lat_long", driverlocation);
                                                                            body.put("remarks", getremark);


                                                                        }
                                                                        ApiInterface.retrofit.canceljob(body).enqueue(new Callback<WebAPIResponse<StartJob>>() {
                                                                            @Override
                                                                            public void onResponse(Call<WebAPIResponse<StartJob>> call, Response<WebAPIResponse<StartJob>> response) {

                                                                                try {
                                                                                    if (response.body().status == 200) {
                                                                                        try {
                                                                                            FrameActivity frameActivity = (FrameActivity) getActivity();
                                                                                            frameActivity.onBackPressed();
                                                                                        } catch (Exception ex) {
                                                                                            dialog.dismiss();
                                                                                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                                                                        }
                                                                                    } else {
                                                                                        dialog.dismiss();
                                                                                        requestDialog.dismiss();
                                                                                        notesDialog = new NotesDialog(getContext(), "ACTIVITY FAILED", "" + response.body().message, "OK", new View.OnClickListener() {
                                                                                            @Override
                                                                                            public void onClick(View view) {

                                                                                                switch (view.getId()) {
                                                                                                    case R.id.button_positive: {
                                                                                                        FrameActivity frameActivity = (FrameActivity) getActivity();
                                                                                                        frameActivity.onBackPressed();
                                                                                                        notesDialog.dismiss();

                                                                                                    }

                                                                                                }

                                                                                            }
                                                                                        });
                                                                                        notesDialog.show();


                                                                                    }

                                                                                } catch (Exception ex) {

                                                                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                                                                                }


                                                                            }

                                                                            @Override
                                                                            public void onFailure(Call<WebAPIResponse<StartJob>> call, Throwable t) {
                                                                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                                                            }
                                                                        });

                                                                    case R.id.img_cancel:
                                                                        requestDialog.dismiss();

                                                                }
                                                            }
                                                        });
                                                        requestDialog.show();


                                                    }
                                                }

                                            });
                                            btn_cancel.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {

                                                    final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

                                                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                                        permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                                                                , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                switch (view.getId()) {
                                                                    case R.id.btn_dialog_ok:
                                                                        permissionDialog.dismiss();
                                                                        ActivityUtils.startWifiSettings(getContext());
                                                                        break;
                                                                }
                                                            }
                                                        });
                                                        permissionDialog.setCancelable(false);
                                                        permissionDialog.setCanceledOnTouchOutside(false);
                                                        permissionDialog.show();
                                                        return;
                                                    } else {


                                                        requestDialog = new RequestDialog(getContext(), getString(R.string.title_reject),
                                                                getString(R.string.msg_reject), new OnDialogButtonClick() {
                                                            @Override
                                                            public void onButtonClick(View view, String data) {

                                                                switch (view.getId()) {
                                                                    case R.id.img_ok:

                                                                        Intent getintent = getActivity().getIntent();
                                                                        String id = getintent.getStringExtra("jobid");
                                                                        String getremark = data;
                                                                        HashMap<String, Object> body = new HashMap<>();

                                                                        if (id != null) {
                                                                            body.put("job_id", Integer.parseInt(id));
                                                                            body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                                                            body.put("timestamp", actual_start_time);
                                                                            body.put("flag", 117);
                                                                            body.put("lat_long", driverlocation);
                                                                            body.put("remarks", getremark);

                                                                        } else {
                                                                            body.put("job_id", payloadNotification.job_id);
                                                                            body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                                                            body.put("timestamp", actual_start_time);
                                                                            body.put("flag", 117);
                                                                            body.put("lat_long", driverlocation);
                                                                            body.put("remarks", getremark);


                                                                        }
                                                                        ApiInterface.retrofit.canceljob(body).enqueue(new Callback<WebAPIResponse<StartJob>>() {
                                                                            @Override
                                                                            public void onResponse(Call<WebAPIResponse<StartJob>> call, Response<WebAPIResponse<StartJob>> response) {

                                                                                try {
                                                                                    if (response.body().status == 200) {

                                                                                        FrameActivity frameActivity = (FrameActivity) getActivity();
                                                                                        frameActivity.onBackPressed();
                                                                                    } else {
                                                                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                                                                                    }

                                                                                } catch (Exception ex) {
                                                                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                                                                }

                                                                            }

                                                                            @Override
                                                                            public void onFailure(Call<WebAPIResponse<StartJob>> call, Throwable t) {
                                                                                dialog.dismiss();
                                                                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                                                            }
                                                                        });


                                                                    case R.id.img_cancel:
                                                                        requestDialog.dismiss();


                                                                }
                                                            }
                                                        });
                                                        requestDialog.show();


                                                    }
                                                }
                                            });

                                        } else if (status.equals("Accepted")) {
                                            btn_start.setVisibility(View.VISIBLE);
                                            btn_cancel.setVisibility(View.VISIBLE);
                                            btn_start.setText("Start Job");
                                            btn_cancel.setText("Abort Job");
                                        }

//
                                    } else {
                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                        dialog.dismiss();

                                    }

                                } else {
                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                    dialog.dismiss();

                                }
                            } catch (Exception ex) {
                                dialog.dismiss();
                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                dialog.dismiss();

                            }
                        } else {

                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                            dialog.dismiss();
                        }


                    }

                    @Override
                    public void onFailure(Call<WebAPIResponse<JobDetail>> call, Throwable t) {
                        dialog.dismiss();
                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                    }
                });

            }


        }

        return view;
    }

    private LinearLayoutManager getLinearLayoutManager() {

        return new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

    }

    @Override
    public void onLocationChanged(Location location) {
        final MarkerOptions option;
        if (marker != null) {

            marker.remove();
        }

        ll = new LatLng(location.getLatitude(), location.getLongitude());

        mylocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 15.8f);
                googleMap.animateCamera(update);
            }
        });

        Double lat, lng;

        lat = location.getLatitude();
        lng = location.getLongitude();


        try {
            option = new MarkerOptions().title("Driver").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location)).position(new LatLng(lat, lng));
            marker = googleMap.addMarker(option);
        } catch (Exception ex) {

        }

        if (googleMap != null) {

            if (checklocation == true) {
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 12.8f);
                googleMap.animateCamera(update);
                checklocation = false;

            }
        }


        driverlocation = lat.toString() + "," + lng.toString();
        dialog.dismiss();
        Log.e("TAAAG", "unshow");
        //   Toast.makeText(getContext(), "" + driverlocation, Toast.LENGTH_SHORT).show();
        supportMapFragment.getMapAsync(this);

    }


    @SuppressLint("RestrictedApi")
    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        final LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.getFusedLocationProviderClient(getActivity()).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        counter++;
                        try {
                            if (counter > 1) {
                                onLocationChanged(locationResult.getLastLocation());
                                LocationServices.getFusedLocationProviderClient(getActivity()).removeLocationUpdates(this);
                            }
                        } catch (Exception ex) {

                        }
                    }
                },
                Looper.myLooper());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        this.googleMap = googleMap;


        MapStyleOptions mapStyleOptions = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map);
        googleMap.setMapStyle(mapStyleOptions);


        Bundle extras = getActivity().getIntent().getExtras();
        Bundle value = null;
        if (extras != null) {
            value = extras.getBundle(Constants.DATA);
            if (value != null) {
                payloadNotification = GsonUtils.fromJson(value.getString(Constants.PAYLOAD), PayloadNotification.class);

                ApiInterface.retrofit.getdata(payloadNotification.job_id).enqueue(new Callback<WebAPIResponse<JobDetail>>() {
                    @Override
                    public void onResponse(Call<WebAPIResponse<JobDetail>> call, Response<WebAPIResponse<JobDetail>> response) {

                        try {
                            if (response.isSuccessful()) {
                                actionItems = response.body().response.getActionItems();

                                BitmapDrawable binmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_bin_marker);
                                Bitmap b = binmarker.getBitmap();
                                Bitmap BinMarker = Bitmap.createScaledBitmap(b, marker_width, marker_height, false);

                                if (actionItems.size() > 1) {
                                    for (int i = 0; i < actionItems.size() - 1; i++) {
                                        try {
                                            String array1[] = actionItems.get(i).getEntityLocation().split(",");
                                            LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));

                                            String startbin = actionItems.get(i).getLabel();

                                            String array2[] = actionItems.get(i + 1).getEntityLocation().split(",");
                                            LatLng dest = new LatLng(Double.parseDouble(array2[0]), Double.parseDouble(array2[1]));

                                            String endbin = actionItems.get(i + 1).getLabel();

                                            googleMap.addMarker(new MarkerOptions().title(startbin).position(start).icon((BitmapDescriptorFactory.fromBitmap(BinMarker))));
                                            googleMap.addMarker(new MarkerOptions().title(endbin).position(dest).icon((BitmapDescriptorFactory.fromBitmap(BinMarker))));
                                            String url = getDirectionsUrl(start, dest);
                                            FetchUrl FetchUrl = new FetchUrl();
                                            FetchUrl.execute(url);


                                        } catch (Exception ex) {
                                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                            dialog.dismiss();
                                            break;

                                        }
                                    }
                                } else {

                                    try {
                                        String array1[] = actionItems.get(0).getEntityLocation().split(",");
                                        LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));
                                        String startbin = actionItems.get(0).getLabel();
                                        googleMap.addMarker(new MarkerOptions().title(startbin).position(start).icon((BitmapDescriptorFactory.fromBitmap(BinMarker))));
                                    } catch (Exception ex) {
                                        dialog.dismiss();
                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                                    }
                                    // functionality if lenght is one
                                }

                                BitmapDrawable dumpmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_dump_marker);
                                Bitmap dump = dumpmarker.getBitmap();
                                Bitmap Dumpmarker = Bitmap.createScaledBitmap(dump, marker_width, marker_height, false);


                                BitmapDrawable sortmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_sort_marker);
                                Bitmap sort = sortmarker.getBitmap();
                                Bitmap Sortmarker = Bitmap.createScaledBitmap(sort, marker_width, marker_height, false);


                                //for last one
                                try {
                                    String dumpname = response.body().response.getEndPointName().toString();
                                   // String sortname = response.body().response.getCheckPointName().toString();

                                    String dumpArray[] = response.body().response.getEndPointLatLong().toString().split(",");
                                  //  String sortArray[] = response.body().response.getCheckPointLatLong().toString().split(",");
//
//                                    if (sortname == null && response.body().response.getCheckPointLatLong().equals(null)) {
//                                        LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
//                                        // LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));
//
//                                        String lastBin[] = actionItems.get(actionItems.size() - 1).getEntityLocation().split(",");
//
//                                        //  googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));
//                                        googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));
//
//                                        String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])), dumping);
//                                        FetchUrl FetchUrl = new FetchUrl();
//                                        FetchUrl.execute(url);
//
////                                        String urll = getDirectionsUrl(sorting, dumping);
////                                        FetchUrl FetchUrll = new FetchUrl();
////                                        FetchUrll.execute(urll);
////
//                                    } else {


                                        LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
                                        //LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));

                                        String lastBin[] = actionItems.get(actionItems.size() - 1).getEntityLocation().split(",");

                                      //  googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));
                                        googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));

                                        String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])), dumping);
                                        FetchUrl FetchUrl = new FetchUrl();
                                        FetchUrl.execute(url);

//                                        String urll = getDirectionsUrl(sorting, dumping);
//                                        FetchUrl FetchUrll = new FetchUrl();
//                                        FetchUrll.execute(urll);
                                 //   }


                                } catch (Exception ex) {
                                    dialog.dismiss();

                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                                }
                            }
                        } catch (Exception ex) {
                            dialog.dismiss();
                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                        }
                    }

                    @Override
                    public void onFailure(Call<WebAPIResponse<JobDetail>> call, Throwable t) {
                        dialog.dismiss();
                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                    }
                });

            } else {
                Intent getintent = getActivity().getIntent();
                String id = getintent.getStringExtra("jobid");

                ApiInterface.retrofit.getdata(Integer.parseInt(id)).enqueue(new Callback<WebAPIResponse<JobDetail>>() {
                    @Override
                    public void onResponse(Call<WebAPIResponse<JobDetail>> call, Response<WebAPIResponse<JobDetail>> response) {

                        try {
                            if (response.isSuccessful()) {
                                actionItems = response.body().response.getActionItems();
                                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_bin_marker);
                                Bitmap b = bitmapdraw.getBitmap();
                                Bitmap smallMarker = Bitmap.createScaledBitmap(b, marker_width, marker_height, false);
                                Log.e("TAAAG", "" + GsonUtils.toJson(actionItems));

                                if (actionItems.size() > 1) {
                                    for (int i = 0; i < actionItems.size() - 1; i++) {

                                        try {
                                            String array1[] = actionItems.get(i).getEntityLocation().split(",");
                                            LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));

                                            String startbin = actionItems.get(i).getLabel();


                                            String array2[] = actionItems.get(i + 1).getEntityLocation().split(",");
                                            LatLng dest = new LatLng(Double.parseDouble(array2[0]), Double.parseDouble(array2[1]));
                                            String endbin = actionItems.get(i + 1).getLabel();

                                            googleMap.addMarker(new MarkerOptions().title(startbin).position(start).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));
                                            googleMap.addMarker(new MarkerOptions().title(endbin).position(dest).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));
                                            String url = getDirectionsUrl(start, dest);
                                            FetchUrl FetchUrl = new FetchUrl();
                                            FetchUrl.execute(url);

                                        } catch (Exception ex) {
                                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                            dialog.dismiss();
                                            break;

                                        }
                                    }


                                } else {
                                    try {

                                        String array1[] = actionItems.get(0).getEntityLocation().split(",");
                                        LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));

                                        String startbin = actionItems.get(0).getLabel();


                                        googleMap.addMarker(new MarkerOptions().title(startbin).position(start).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));
                                    } catch (Exception ex) {

                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                        dialog.dismiss();
                                    }
                                    // functionality if lenght is one
                                }
//lastone

                                BitmapDrawable dumpmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_dump_marker);
                                Bitmap dump = dumpmarker.getBitmap();
                                Bitmap Dumpmarker = Bitmap.createScaledBitmap(dump, marker_width, marker_height, false);


                                BitmapDrawable sortmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_sort_marker);
                                Bitmap sort = sortmarker.getBitmap();
                                Bitmap Sortmarker = Bitmap.createScaledBitmap(sort, marker_width, marker_height, false);

                                try {

                                    String dumpname = response.body().response.getEndPointName().toString();
                                  //  String sortname = response.body().response.getCheckPointName().toString();
                                    String dumpArray[] = response.body().response.getEndPointLatLong().toString().split(",");
                                 //   String sortArray[] = response.body().response.getCheckPointLatLong().toString().split(",");

//                                    if (sortname == null && response.body().response.getCheckPointLatLong().equals(null)) {
//                                        LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
//                                        // LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));
//
//                                        String lastBin[] = actionItems.get(actionItems.size() - 1).getEntityLocation().split(",");
//
//                                        //  googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));
//                                        googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));
//
//                                        String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])), dumping);
//                                        FetchUrl FetchUrl = new FetchUrl();
//                                        FetchUrl.execute(url);
//
////                                        String urll = getDirectionsUrl(sorting, dumping);
////                                        FetchUrl FetchUrll = new FetchUrl();
////                                        FetchUrll.execute(urll);
////
//                                    }
//                                    else {


                                        LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
                                      //  LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));

                                        String lastBin[] = actionItems.get(actionItems.size() - 1).getEntityLocation().split(",");

                                    //    googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));
                                        googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));

                                        String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])), dumping);
                                        FetchUrl FetchUrl = new FetchUrl();
                                        FetchUrl.execute(url);

//                                        String urll = getDirectionsUrl(sorting, dumping);
//                                        FetchUrl FetchUrll = new FetchUrl();
//                                        FetchUrll.execute(urll);
//                                    }


                                } catch (Exception ex) {


                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                    dialog.dismiss();
                                }
                            }
                        } catch (Exception ex) {

                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<WebAPIResponse<JobDetail>> call, Throwable t) {
                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));
                        dialog.dismiss();
                    }
                });

            }
        }

    }

    public void LocationCheck() {
        final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                    , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.btn_dialog_ok:
                            permissionDialog.dismiss();
                            ActivityUtils.startWifiSettings(getContext());
                            break;
                    }
                }
            });
            permissionDialog.setCancelable(false);
            permissionDialog.setCanceledOnTouchOutside(false);
            permissionDialog.show();
            return;
        }

    }

    private void initMap() {
        supportMapFragment.getMapAsync(this);

    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        } catch (Exception ex) {
            Toast.makeText(fContext, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private String getDirectionsUrl(LatLng start, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + start.latitude + "," + start.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        } catch (Exception e) {
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {

            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            JobDetailsFragment.ParserTask parserTask = new JobDetailsFragment.ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;

            PolylineOptions lineOptions = null;
            if (result != null) {

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<>();
                    lineOptions = new PolylineOptions();
                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);
                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(10);
                    lineOptions.color(R.color.colorPrimary);
                }

                // Drawing polyline in the Google Map for the i-th route
                if (lineOptions != null) {
                    googleMap.addPolyline(lineOptions);
                } else {
                }
            } else {

                if (check == true) {
                }
                check = false;

            }
        }
    }


}
