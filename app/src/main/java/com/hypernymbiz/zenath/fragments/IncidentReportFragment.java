package com.hypernymbiz.zenath.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.adapter.IncidentReportAdapter;
import com.hypernymbiz.zenath.api.ApiInterface;
import com.hypernymbiz.zenath.dialog.LoadingDialog;
import com.hypernymbiz.zenath.model.IncidentReport;
import com.hypernymbiz.zenath.model.Datum;
import com.hypernymbiz.zenath.model.WebAPIResponse;
import com.hypernymbiz.zenath.toolbox.ToolbarListener;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Metis on 01-May-18.
 */

public class IncidentReportFragment extends Fragment implements View.OnClickListener, ToolbarListener {
    private Toolbar mToolbar;
    private TextView mToolbarTitle;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private IncidentReportAdapter incidentReportAdapter ;
ImageView imageView;
    private List<Datum> incidentReports;
    LoadingDialog dialog;



    Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void setTitle(String title) {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(title);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("Incident Report");
        }
        mContext = context;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_incident_report, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_incident);
        imageView = (ImageView) view.findViewById(R.id.img_job_list);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        dialog = new LoadingDialog(getActivity(), getString(R.string.msg_loading));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        ApiInterface.retrofit.incidentlist().enqueue(new Callback<WebAPIResponse<IncidentReport>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<IncidentReport>> call, Response<WebAPIResponse<IncidentReport>> response) {
                dialog.dismiss();
                if (response.isSuccessful()) {
                    try {
                        if (response.body().status==200) {
                            // Toast.makeText(getContext(), "List Detail"+Integer.toString(response.body().response.job_info.size()), Toast.LENGTH_SHORT).show();
                            incidentReports=response.body().response.getData();
                            incidentReportAdapter = new IncidentReportAdapter(incidentReports,getActivity());
                            recyclerView.setAdapter(incidentReportAdapter);
                            String size;
                            size = String.valueOf(incidentReports.size());
                            if (size.equals("0")) {
                                imageView.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            } else
                                imageView.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception ex) {
                        dialog.dismiss();
                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                    }
                } else {

                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse<IncidentReport>> call, Throwable t) {
                dialog.dismiss();
                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

            }
        });






        return view;

    }
































    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResume()
    {
        if (mContext instanceof ToolbarListener) {
            ((ToolbarListener) mContext).setTitle("Incident Report");
        }
        super.onResume();
    }


}
