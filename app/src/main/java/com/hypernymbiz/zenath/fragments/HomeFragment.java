package com.hypernymbiz.zenath.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.TextRoundCornerProgressBar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hypernymbiz.zenath.FrameActivity;
import com.hypernymbiz.zenath.LoginActivity;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.adapter.DialogAdapter;
import com.hypernymbiz.zenath.adapter.JobDetailAdapter;
import com.hypernymbiz.zenath.api.ApiInterface;
import com.hypernymbiz.zenath.dialog.LoadingDialog;
import com.hypernymbiz.zenath.dialog.NotesDialog;
import com.hypernymbiz.zenath.enumerations.AnimationEnum;
import com.hypernymbiz.zenath.model.ActionItem;
import com.hypernymbiz.zenath.model.BinData;
import com.hypernymbiz.zenath.model.DirectionsJSONParser;
import com.hypernymbiz.zenath.model.DriverShiftStatus;
import com.hypernymbiz.zenath.model.JobCount;
import com.hypernymbiz.zenath.model.JobDetail;
import com.hypernymbiz.zenath.model.Lastjob;
import com.hypernymbiz.zenath.model.NotifyEvent;
import com.hypernymbiz.zenath.model.WebAPIResponse;
import com.hypernymbiz.zenath.model.WebApiRespone_profile;
import com.hypernymbiz.zenath.toolbox.ToolbarListener;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.hypernymbiz.zenath.utils.ActivityUtils;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;
import com.hypernymbiz.zenath.utils.RuningJobUtils;
import com.hypernymbiz.zenath.utils.RunningResumeJobUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by shamis on 10/10/2017.
 */

public class HomeFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    RecyclerView recyclerView;
    Calendar calendar;
    int marker_width = 200;
    int marker_height = 200;
    TextView txt_totalduration;
    private long UPDATE_INTERVAL = 1000;  /* 1 sec */
    private long FASTEST_INTERVAL = 500; /* 1/2 sec */
    public static int counter = 0;
    View view;
    Dialog Incident;
    GoogleMap mMap;
    SupportMapFragment supportMapFragment;
    CoordinatorLayout coordinatorLayout;
    GoogleApiClient googleApiClient;
    Marker marker;
    ImageButton mylocation;
    LoadingDialog dialog;
    String size, truckname;
    TextView fromaddress_inprogress, toaddress_inprogress, jobname_inprogress, activity_count_number,shift_status;
    LatLng ll;
    boolean check = true;
    boolean checklocation = true;
    TextView mNumberOfCartItemsText, truckstatus, truckspeed, truck_name, jobstatus;
    LinearLayout linear_inprogress, linear_lastjob;
    CardView cardView_activites, cardView_maintenance, cardView_violations;
    private LocationRequest mLocationRequest;
    Context mContext;
    CardView mapcardalayout;
    NotesDialog notesDialog;
    SharedPreferences pref;
    EditText dialog_date, dialog_time, dialog_remarks;
    Spinner dialog_spinner;
    String date, time, spinner_item;
    ImageView btn_dialog_check, btn_dialog_uncheck;
    int Type;
    String actual_date_time;
    String id;
    ScrollView scrollView;
    String progress;
    SharedPreferences.Editor editor;


    private EventBus bus = EventBus.getDefault();

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, HomeFragment.class));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        boolean checkJobResume=ActiveJobUtils.isJobResumed(getContext());
//        Toast.makeText(mContext, "checkjob resume status"+Boolean.toString(checkJobResume), Toast.LENGTH_SHORT).show();

//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("Dashboard");
        }
        mContext = context;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        View view, view1;

        inflater.inflate(R.menu.menu_main, menu);
        view = menu.findItem(R.id.notification_bell).getActionView();
        view1 = menu.findItem(R.id.incident).getActionView();
        mNumberOfCartItemsText = (TextView) view.findViewById(R.id.text_number_of_cart_items);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(getActivity(), FrameActivity.class, JobNotificationFragment.class.getName(), null);
            }
        });

        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Incident = new Dialog(getContext());
                Incident.setContentView(R.layout.dialog_incident_job);
                dialog_spinner = (Spinner) Incident.findViewById(R.id.spinner_incident);
                dialog_date = (EditText) Incident.findViewById(R.id.edit_date);
                dialog_time = (EditText) Incident.findViewById(R.id.edit_time);
                dialog_remarks = (EditText) Incident.findViewById(R.id.edittxt_remark);
                btn_dialog_check = (ImageView) Incident.findViewById(R.id.img_ok);
                btn_dialog_uncheck = (ImageView) Incident.findViewById(R.id.img_cancel);
                calendar = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                date = df.format(calendar.getTime());
                SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                time = dff.format(calendar.getTime());
                dialog_date.setText(AppUtils.getFormattedDate(date));
                dialog_time.setText(AppUtils.getDateAndTime(time));


                SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                outputFmt.setTimeZone(TimeZone.getTimeZone("gmt"));
                actual_date_time = outputFmt.format(new Date());


                ArrayList<String> stringArrayList = new ArrayList<>();
                stringArrayList.add("Select A Type");
                stringArrayList.add("Tyre_brust");
                stringArrayList.add("Break_failure");
                stringArrayList.add("Accident");
                stringArrayList.add("Others");

                final ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, stringArrayList);
                dialog_spinner.setAdapter(itemsAdapter);
                dialog_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        if (position != 0) {

                            spinner_item = itemsAdapter.getItem(position);
                            Toast.makeText(getContext(), "" + spinner_item, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                Incident.setCanceledOnTouchOutside(false);
                Incident.show();
                Incident.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Incident.setCancelable(false);


                btn_dialog_uncheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Incident.dismiss();

                    }
                });

                btn_dialog_check.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try {

                            if (spinner_item.equals("Tyre_brust")) {
                                Type = 105;
                            } else if (spinner_item.equals("Break_failure")) {
                                Type = 106;

                            } else if (spinner_item.equals("Accident")) {

                                Type = 107;
                            } else if (spinner_item.equals("Others")) {
                                Type = 108;

                            }

                            HashMap<String, Object> body = new HashMap<>();
                            body.put("notes", dialog_remarks.getText().toString());
                            body.put("type", Type);
                            body.put("timestamp", actual_date_time);

                            ApiInterface.retrofit.reportincident(body).enqueue(new Callback<WebApiRespone_profile>() {
                                @Override
                                public void onResponse(Call<WebApiRespone_profile> call, Response<WebApiRespone_profile> response) {

                                    try {
                                        if (response.isSuccessful()) {

                                            Incident.dismiss();
                                            Toast.makeText(getContext(), "Sucessfull", Toast.LENGTH_SHORT).show();

                                        }
                                    } catch (Exception ex) {
                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                                    }
                                }

                                @Override
                                public void onFailure(Call<WebApiRespone_profile> call, Throwable t) {

                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                }
                            });

                        } catch (Exception ex) {

                            Toast.makeText(getContext(), "Incident Missing", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

            }
        });
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinator);
        cardView_activites = (CardView) view.findViewById(R.id.card_activites);
        mapcardalayout = (CardView) view.findViewById(R.id.cardviewhome_map);
        scrollView = (ScrollView) view.findViewById(R.id.layout_scrollView);
        jobstatus = (TextView) view.findViewById(R.id.txt_job_status);
        cardView_maintenance = (CardView) view.findViewById(R.id.card_maintenances);
        cardView_violations = (CardView) view.findViewById(R.id.card_violation);
        linear_inprogress = (LinearLayout) view.findViewById(R.id.layout_linear_inprogress);
        linear_lastjob = (LinearLayout) view.findViewById(R.id.layout_linear_lastjob);
        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
        fromaddress_inprogress = (TextView) view.findViewById(R.id.txt_from_address);
        toaddress_inprogress = (TextView) view.findViewById(R.id.txt_to_address);
        activity_count_number = (TextView) view.findViewById(R.id.txt_progress_count);
        truckstatus = (TextView) view.findViewById(R.id.txt_truck_status);
        truckspeed = (TextView) view.findViewById(R.id.txt_truckspeed);
        txt_totalduration = (TextView) view.findViewById(R.id.txt_duration);
        truck_name = (TextView) view.findViewById(R.id.txt_truck_name);
        shift_status = (TextView) view.findViewById(R.id.txt_shift_status);
        jobname_inprogress = (TextView) view.findViewById(R.id.txt_activity_name);
        mylocation = (ImageButton) view.findViewById(R.id.btn_mylocation);
        pref = getActivity().getSharedPreferences("TAG", MODE_PRIVATE);
      //  truckname = pref.getString("Truckname", "");
      //  truck_name.setText(truckname);
        startLocationUpdates();
        bus.register(this);


        dialog = new LoadingDialog(getActivity(), getString(R.string.msg_loading));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        //      init_persistent_bottomsheet();

        linear_inprogress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    linear_inprogress.setBackgroundColor(Color.parseColor("#EC9940"));
                    return false;
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    linear_inprogress.setBackgroundColor(Color.parseColor("#c47f35"));
                }
                return false;
            }
        });



        linear_lastjob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    linear_lastjob.setBackgroundColor(Color.parseColor("#36AB7A"));
                    return false;
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    linear_lastjob.setBackgroundColor(Color.parseColor("#FF2D8C64"));
                }

                return false;
            }
        });


        cardView_activites.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    cardView_activites.setCardBackgroundColor(Color.parseColor("#0097a7"));
                    return false;
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    cardView_activites.setCardBackgroundColor(Color.parseColor("#FF097B87"));
                }


                return false;
            }
        });

        cardView_maintenance.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    cardView_maintenance.setCardBackgroundColor(Color.parseColor("#EC9940"));
                    return false;
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    cardView_maintenance.setCardBackgroundColor(Color.parseColor("#c47f35"));
                }

                return false;
            }
        });

        cardView_violations.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    cardView_violations.setCardBackgroundColor(Color.parseColor("#f44336"));
                    return false;
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    cardView_violations.setCardBackgroundColor(Color.parseColor("#bf392f"));
                }

                return false;
            }
        });
        cardView_activites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(getActivity(), FrameActivity.class, JobFragment.class.getName(), null);

            }
        });
        cardView_maintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //     ActivityUtils.startActivity(getActivity(), FrameActivity.class, MaintenanceFragment.class.getName(), null);
                Toast.makeText(getContext(), "Maintenance", Toast.LENGTH_SHORT).show();

            }
        });

        cardView_violations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ActivityUtils.startActivity(getActivity(), FrameActivity.class, JobFragment.class.getName(), null);
                Toast.makeText(getContext(), "Violation", Toast.LENGTH_SHORT).show();
            }
        });
        linear_inprogress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(getActivity(), FrameActivity.class, RunningJobFragment.class.getName(), null);
            }
        });

        linear_lastjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ApiInterface.retrofit.getlastjob().enqueue(new Callback<WebAPIResponse<Lastjob>>() {
                    @Override
                    public void onResponse(Call<WebAPIResponse<Lastjob>> call, Response<WebAPIResponse<Lastjob>> response) {

                        try {
                            if (response.body().status == 400) {
                                notesDialog = new NotesDialog(getContext(), "NO LAST ACTIVITY", "" + response.body().message, "OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        switch (view.getId()) {
                                            case R.id.button_positive: {
                                                FrameActivity frameActivity = (FrameActivity) getActivity();
                                                frameActivity.onBackPressed();
                                                notesDialog.dismiss();

                                            }

                                        }

                                    }
                                });
                                notesDialog.show();
                            } else {

                                ActivityUtils.startActivity(getActivity(), FrameActivity.class, LastActivityFragment.class.getName(), null, AnimationEnum.VERTICAL);

                            }


                        } catch (Exception ex) {

                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                        }
                    }

                    @Override
                    public void onFailure(Call<WebAPIResponse<Lastjob>> call, Throwable t) {
                        AppUtils.showSnackBar(getView(), "NO LAST ACTIVITY");

                    }
                });

            }
        });


        initMap();

        return view;

    }


    @Override
    public void onResume() {
        super.onResume();

        if (!RunningResumeJobUtils.isJobResumed(getContext())) {
            linear_inprogress.setVisibility(View.GONE);
            LinearLayout.LayoutParams scrollParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1);
            LinearLayout.LayoutParams cardviewpParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1.1f);
            scrollView.setLayoutParams(scrollParams);
            mapcardalayout.setLayoutParams(cardviewpParams);


        } else {
            linear_inprogress.setVisibility(View.VISIBLE);
            List<ActionItem> binList = RunningResumeJobUtils.getJobResume(getContext());
            String jobId = RunningResumeJobUtils.getJobId(getContext());

            LinearLayout.LayoutParams scrollParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1.8f);
            LinearLayout.LayoutParams cardviewpParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1.0f);
            scrollView.setLayoutParams(scrollParams);
            mapcardalayout.setLayoutParams(cardviewpParams);
//            progress= (pref.getString("activity_percentage",""));
            progress = RunningResumeJobUtils.getJobPercent(getContext());
            jobstatus.setText(RuningJobUtils.getstatus(getContext()));


            if (progress == null) {
                TextRoundCornerProgressBar textRoundCornerProgressBar = (TextRoundCornerProgressBar) view.findViewById(R.id.round_progress);
                textRoundCornerProgressBar.setProgressColor(Color.parseColor("#ffffff"));
                textRoundCornerProgressBar.setProgressBackgroundColor(Color.parseColor("#a8ffffff"));
                textRoundCornerProgressBar.setMax(100);
                textRoundCornerProgressBar.setProgress(0);
                activity_count_number.setText(0 + "%");

            } else {
                TextRoundCornerProgressBar textRoundCornerProgressBar = (TextRoundCornerProgressBar) view.findViewById(R.id.round_progress);
                textRoundCornerProgressBar.setProgressColor(Color.parseColor("#ffffff"));
                textRoundCornerProgressBar.setProgressBackgroundColor(Color.parseColor("#a8ffffff"));
                textRoundCornerProgressBar.setMax(100);
                textRoundCornerProgressBar.setProgress(Float.parseFloat(progress));
                double a = Double.parseDouble(progress);
                NumberFormat df = DecimalFormat.getInstance();
                df.setMinimumFractionDigits(0);
                df.setRoundingMode(RoundingMode.DOWN);
                activity_count_number.setText(df.format(a) + "%");

            }


        }


        if (mContext instanceof ToolbarListener) {
            ((ToolbarListener) mContext).setTitle("Dashboard");
        }


        ApiInterface.retrofit.getcount().enqueue(new Callback<WebAPIResponse<List<JobCount>>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<List<JobCount>>> call, Response<WebAPIResponse<List<JobCount>>> response) {
                dialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().status == 200) {
                        size = String.valueOf(response.body().response.get(0).getCount());
                        if (size != null) {
                            try {
                                mNumberOfCartItemsText.setText(size);
                            } catch (Exception ex) {
                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                            }
                        }

                    }

                } else {
                    dialog.dismiss();
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse<List<JobCount>>> call, Throwable t) {
                dialog.dismiss();
                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

            }
        });

//        id = pref.getString("jobid", "");
//        if(!id.equals("")) {
//            ApiInterface.retrofit.getdata(Integer.parseInt(id)).enqueue(new Callback<WebAPIResponse<JobDetail>>() {
//                @Override
//                public void onResponse(Call<WebAPIResponse<JobDetail>> call, Response<WebAPIResponse<JobDetail>> response) {
//
//                    try {
//                        if (response.isSuccessful()) {
//
//                            String value = String.valueOf(response.body().response.getDuration());
//                            if (!value.equals("null")) {
//
//                                txt_totalduration.setText(String.valueOf(response.body().response.getDuration()));
//                            } else {
//                                txt_totalduration.setText("0");
//                            }
//
//                        }
//                    } catch (Exception ex) {
//
//                        Toast.makeText(mContext, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<WebAPIResponse<JobDetail>> call, Throwable t) {
//                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));
//
//                }
//            });
//        }

        ApiInterface.retrofit.getlastjob().enqueue(new Callback<WebAPIResponse<Lastjob>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<Lastjob>> call, Response<WebAPIResponse<Lastjob>> response) {

                try {
                    if (response.body().status == 200) {

                        if (response.body().response.getDuration() == null) {
                            txt_totalduration.setText("0");
                        } else
                            txt_totalduration.setText(String.valueOf(response.body().response.getDuration()));
                    }

                } catch (Exception ex) {
                    txt_totalduration.setText("0");
                    ex.printStackTrace();
//                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse<Lastjob>> call, Throwable t) {

            }
        });


        ApiInterface.retrofit.getshiftstatus().enqueue(new Callback<WebAPIResponse<DriverShiftStatus>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<DriverShiftStatus>> call, Response<WebAPIResponse<DriverShiftStatus>> response) {

                try {
                    if (response.body().status == 200) {
                        truckname=response.body().response.getAssignedTruck();
                        editor = pref.edit();
                        editor.putString("Truckname",truckname);
                        editor.commit();
                        truck_name.setText(truckname);
                        if(response.body().response.getShiftStatus()==true)
                        {
                            shift_status.setText("On Shift");
                        }
                        else
                        {
                            shift_status.setText("Off Shift");
                        }


                    }

                } catch (Exception ex) {

//                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse<DriverShiftStatus>> call, Throwable t) {

            }
        });

        buildGoogleApiClient();


    }

    @Subscribe
    public void onEvent(NotifyEvent event) {
        Log.e("TAAAF", "notify");

        ApiInterface.retrofit.getcount().enqueue(new Callback<WebAPIResponse<List<JobCount>>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<List<JobCount>>> call, Response<WebAPIResponse<List<JobCount>>> response) {
                dialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().status == 200) {
                        size = String.valueOf(response.body().response.get(0).getCount());
                        if (size != null) {
                            try {
                                mNumberOfCartItemsText.setText(size);
                            } catch (Exception ex) {
                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                            }
                        }

                    }

                } else {
                    dialog.dismiss();
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse<List<JobCount>>> call, Throwable t) {
                dialog.dismiss();
                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

            }
        });
        ApiInterface.retrofit.getshiftstatus().enqueue(new Callback<WebAPIResponse<DriverShiftStatus>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<DriverShiftStatus>> call, Response<WebAPIResponse<DriverShiftStatus>> response) {

                try {
                    if (response.body().status == 200) {
                        truckname=response.body().response.getAssignedTruck();
                        editor = pref.edit();
                        editor.putString("Truckname","");
                        editor.commit();
                        truck_name.setText(truckname);
                        if(response.body().response.getShiftStatus()==true)
                        {
                            shift_status.setText("On Shift");
                        }
                        else
                        {
                            shift_status.setText("Off Shift");
                        }


                    }

                } catch (Exception ex) {

//                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse<DriverShiftStatus>> call, Throwable t) {

            }
        });

    }

    public void init_persistent_bottomsheet() {
        View persistentbottomSheet = coordinatorLayout.findViewById(R.id.bottomsheet);
//        iv_trigger = (ImageView) persistentbottomSheet.findViewById(R.id.iv_fab);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(persistentbottomSheet);


//        iv_trigger.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
        if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
//            }
//        });
        if (behavior != null)
            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    //showing the different states
                    switch (newState) {
                        case BottomSheetBehavior.STATE_HIDDEN:
                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:
                            break;
                        case BottomSheetBehavior.STATE_COLLAPSED:
                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:
                            break;
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    // React to dragging events

                }
            });


    }

    @Override
    public void onClick(View v) {

    }


    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        final MarkerOptions option;
        googleMap.clear();
        if (marker != null) {

            marker.remove();
        }
        MapsInitializer.initialize(getContext());
        this.mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);

//        mMap.clear();
        MapStyleOptions mapStyleOptions = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map);
        googleMap.setMapStyle(mapStyleOptions);


        if (ll != null) {
            if (checklocation == true) {
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 12.8f);
                googleMap.animateCamera(update);
                checklocation = false;
            }
            mylocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 12.8f);
                    googleMap.animateCamera(update);
                }
            });


            option = new MarkerOptions().title("Driver").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location)).position(new LatLng(ll.latitude, ll.longitude));
            marker = googleMap.addMarker(option);

            //map resumed job
            if (RunningResumeJobUtils.isJobResumed(getContext())) {
                List<ActionItem> binList = RunningResumeJobUtils.getJobResume(getContext());

                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_bin_marker);
                Bitmap b = bitmapdraw.getBitmap();
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, marker_width, marker_height, false);

                if (binList.size() > 1) {

                    try {

                        for (int i = 0; i < binList.size() - 1; i++) {

                            try {
                                String start_bin_name = binList.get(i).getLabel();

                                String array1[] = binList.get(i).getEntityLocation().split(",");
                                LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));

                                String array2[] = binList.get(i + 1).getEntityLocation().split(",");
                                LatLng dest = new LatLng(Double.parseDouble(array2[0]), Double.parseDouble(array2[1]));

                                String dest_bin_name = binList.get(i + 1).getLabel();
                                googleMap.addMarker(new MarkerOptions().title(start_bin_name).position(start).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));
                                googleMap.addMarker(new MarkerOptions().title(dest_bin_name).position(dest).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));
                                String url = getDirectionsUrl(start, dest);

                                FetchUrl FetchUrl = new FetchUrl();
                                FetchUrl.execute(url);
                            } catch (Exception ex) {
                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                break;

                            }
                        }
                    } catch (Exception ex) {
                        Toast.makeText(mContext, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    try {

                        String array1[] = binList.get(0).getEntityLocation().split(",");
                        LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));
                        String start_bin_name = binList.get(0).getLabel();

                        googleMap.addMarker(new MarkerOptions().title(start_bin_name).position(start).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));
                    } catch (Exception ex) {

                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                    }
                }


                BitmapDrawable dumpmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_dump_marker);
                Bitmap dump = dumpmarker.getBitmap();
                Bitmap Dumpmarker = Bitmap.createScaledBitmap(dump, marker_width, marker_height, false);

                BitmapDrawable sortmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_sort_marker);
                Bitmap sort = sortmarker.getBitmap();
                Bitmap Sortmarker = Bitmap.createScaledBitmap(sort, marker_width, marker_height, false);


                try {
//                    String dumpArray[] = binList.get(0).getBinLatLong().split(",");
                    String dumpArray[] = RunningResumeJobUtils.getDumpLatLng(getContext()).split(",");
                  //  String sortArray[] = RunningResumeJobUtils.getSortLatLng(getContext()).toString().split(",");

                    String dumpname = RunningResumeJobUtils.getDumpName(getContext());
                 //   String sortname = RunningResumeJobUtils.getSortName(getContext());

                    LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
                 //   LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));

//                    if(sortname==null&&RunningResumeJobUtils.getSortLatLng(getContext()).equals(null))
//                    {
//                        String lastBin[] = binList.get(binList.size() - 1).getEntityLocation().split(",");
//
//                       // googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));
//
//                        googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));
//
//
//                        String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])),dumping);
//                        FetchUrl FetchUrl = new FetchUrl();
//                        FetchUrl.execute(url);
//
////                        String sorting_url = getDirectionsUrl(sorting, dumping);
////                        FetchUrl FetchUrlll = new FetchUrl();
////                        FetchUrlll.execute(sorting_url);
//
//
//                    }
//
//                    else {

                        String lastBin[] = binList.get(binList.size() - 1).getEntityLocation().split(",");

                      //  googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));

                        googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));


                        String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])), dumping);
                        FetchUrl FetchUrl = new FetchUrl();
                        FetchUrl.execute(url);

//                        String sorting_url = getDirectionsUrl(sorting, dumping);
//                        FetchUrl FetchUrlll = new FetchUrl();
//                        FetchUrlll.execute(sorting_url);
//                    }

                } catch (Exception ex) {

                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }


            }

        }


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(final Location location) {


        int currentspeed = (int) ((location.getSpeed() * 3600) / 1000);

        if (currentspeed < 1) {

            truckspeed.setText("0");
            truckstatus.setText("Idle");


        } else {
            truckspeed.setText(String.valueOf(currentspeed));
            truckstatus.setText("Moving");

        }


        ll = new LatLng(location.getLatitude(), location.getLongitude());
//        Toast.makeText(getActivity(), "location changed"+Double.toString(ll.latitude), Toast.LENGTH_SHORT).show();

        supportMapFragment.getMapAsync(this);


    }


    private String getDirectionsUrl(LatLng start, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + start.latitude + "," + start.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        } catch (Exception e) {
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

//    @Override
//    public void update(Observable observable, Object o) {
//        AppUtils.makeToast(getContext(),(String) o);
//
//
//
//        ApiInterface.retrofit.getcount().enqueue(new Callback<WebAPIResponse<List<JobCount>>>() {
//            @Override
//            public void onResponse(Call<WebAPIResponse<List<JobCount>>> call, Response<WebAPIResponse<List<JobCount>>> response) {
//                dialog.dismiss();
//                if (response.isSuccessful()) {
//                    if (response.body().status == 200) {
//                        size = String.valueOf(response.body().response.get(0).getCount());
//                        if (size != null) {
//                            try {
//                                mNumberOfCartItemsText.setText(size);
//                            } catch (Exception ex) {
//                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
//
//                            }
//                        }
//
//                    }
//
//                } else {
//                    dialog.dismiss();
//                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<WebAPIResponse<List<JobCount>>> call, Throwable t) {
//                dialog.dismiss();
//                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));
//
//            }
//        });
//
//
//    }


    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {

            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            HomeFragment.ParserTask parserTask = new HomeFragment.ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;

            PolylineOptions lineOptions = null;
            if (result != null) {

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<>();
                    lineOptions = new PolylineOptions();
                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);
                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(10);
                    lineOptions.color(R.color.colorPrimary);
                }

                // Drawing polyline in the Google Map for the i-th route
                if (lineOptions != null) {
                    mMap.addPolyline(lineOptions);
                } else {
                }
            } else {

                if (check == true) {
                }
                check = false;

            }
        }
    }


    private void initMap() {
        supportMapFragment.getMapAsync(this);

    }


    @Override
    public void onPause() {
        super.onPause();
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        } catch (Exception ex) {
            Toast.makeText(mContext, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

        }
//        ObservableObject.getInstance().deleteObserver(this);
    }

    @SuppressLint("RestrictedApi")
    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        final LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.getFusedLocationProviderClient(getActivity()).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        counter++;
                        try {
                            if (counter > 1) {
                                onLocationChanged(locationResult.getLastLocation());
                                LocationServices.getFusedLocationProviderClient(getActivity()).removeLocationUpdates(this);
                            }
                        } catch (Exception ex) {

                        }
                    }
                },
                Looper.myLooper());
    }


//    public final static class DialogAdapter extends RecyclerView.Adapter<DialogAdapter.RecyclerHolder> {
//
//        private final List<Object> list = new ArrayList<>();
//
//        private final ExpansionLayoutCollection expansionsCollection = new ExpansionLayoutCollection();
//
//        public DialogAdapter() {
//            expansionsCollection.openOnlyOne(true);
//        }
//
//        @Override
//        public RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            return RecyclerHolder.buildFor(parent);
//        }
//
//        @Override
//        public void onBindViewHolder(RecyclerHolder holder, int position) {
//            holder.bind(list.get(position));
//
//            expansionsCollection.add(holder.getExpansionLayout());
//        }
//
//        @Override
//        public int getItemCount() {
//            return list.size();
//        }
//
//        public void setItems(List<Object> items) {
//            this.list.addAll(items);
//            notifyDataSetChanged();
//        }
//
//        public final static class RecyclerHolder extends RecyclerView.ViewHolder {
//
//            private static final int LAYOUT = R.layout.item_dialog_bin;
//
//            ExpansionLayout expansionLayout;
//
//            public static RecyclerHolder buildFor(ViewGroup viewGroup){
//                return new RecyclerHolder(LayoutInflater.from(viewGroup.getContext()).inflate(LAYOUT, viewGroup, false));
//            }
//
//            public RecyclerHolder(View itemView) {
//                super(itemView);
//            }
//
//            public void bind(Object object){
//                expansionLayout.collapse(false);
//            }
//
//            public ExpansionLayout getExpansionLayout() {
//                return expansionLayout;
//            }
//        }
//    }


    @Override
    public void onDestroyView() {

        bus.unregister(this);
        super.onDestroyView();
    }
}