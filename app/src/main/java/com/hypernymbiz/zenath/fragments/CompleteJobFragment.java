package com.hypernymbiz.zenath.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.adapter.CompleteJobAdapter;
import com.hypernymbiz.zenath.api.ApiInterface;
import com.hypernymbiz.zenath.dialog.LoadingDialog;
import com.hypernymbiz.zenath.model.Completed;
import com.hypernymbiz.zenath.model.GetAppJobs;
import com.hypernymbiz.zenath.model.JobInfo_;
import com.hypernymbiz.zenath.model.Respone_Completed_job;
import com.hypernymbiz.zenath.model.WebAPIResponse;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;
import com.hypernymbiz.zenath.utils.LoginUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Metis on 17-Mar-18.
 */

public class CompleteJobFragment extends Fragment {
    View view;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CompleteJobAdapter completeJobAdapter;
    private List<Completed> completeds;
    String getUserAssociatedEntity;
    LoadingDialog dialog;
    ImageView imageView;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       view = inflater.inflate(R.layout.fragment_view_pager_job_compltd, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_complete);
        imageView = (ImageView) view.findViewById(R.id.img_job_list);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        getUserAssociatedEntity = LoginUtils.getUserAssociatedEntity(getContext());
        dialog = new LoadingDialog(getActivity(), getString(R.string.msg_loading));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

            ApiInterface.retrofit.getdata().enqueue(new Callback<WebAPIResponse<GetAppJobs>>() {
                @Override
                public void onResponse(Call<WebAPIResponse<GetAppJobs>> call, Response<WebAPIResponse<GetAppJobs>> response) {
                    dialog.dismiss();
                    if (response.isSuccessful()) {
                        try {
                            if (response.body().status==200) {
                                // Toast.makeText(getContext(), "List Detail"+Integer.toString(response.body().response.job_info.size()), Toast.LENGTH_SHORT).show();
                                completeds = response.body().response.getCompleted();
                                completeJobAdapter = new CompleteJobAdapter(completeds,getActivity());
                                recyclerView.setAdapter(completeJobAdapter);
                                String size;
                                size = String.valueOf(completeds.size());
                                if (size.equals("0")) {
                                    imageView.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                } else
                                    imageView.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception ex) {
                            dialog.dismiss();
                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                        }
                    } else {

                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                    }
                }

                @Override
                public void onFailure(Call<WebAPIResponse<GetAppJobs>> call, Throwable t) {
                    dialog.dismiss();
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                }
            });


        return view;

    }

}
