package com.hypernymbiz.zenath.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.adapter.JobNotifiyAdapter;
import com.hypernymbiz.zenath.api.ApiInterface;
import com.hypernymbiz.zenath.dialog.LoadingDialog;
import com.hypernymbiz.zenath.dialog.PermissionDialog;
import com.hypernymbiz.zenath.model.JobInfo;
import com.hypernymbiz.zenath.model.WebAPIResponse;
import com.hypernymbiz.zenath.toolbox.ToolbarListener;
import com.hypernymbiz.zenath.utils.ActivityUtils;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;
import com.hypernymbiz.zenath.utils.LoginUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Metis on 23-Mar-18.
 */

public class JobNotificationFragment extends Fragment {
    private JobNotifiyAdapter adapter;
    SharedPreferences sharedPreferences;
    String getUserAssociatedEntity;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipelayout;

    private PermissionDialog permissionDialog;
    Context fContext;
    ImageView imageView;
    View view;
    LoadingDialog dialog;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fContext = context;
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("Notifications");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_job_notification, container, false);
        sharedPreferences = getActivity().getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        getUserAssociatedEntity = LoginUtils.getUserAssociatedEntity(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.notification_recycler);
        imageView = (ImageView) view.findViewById(R.id.img_job_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        swipelayout = (SwipeRefreshLayout) view.findViewById(R.id.layout_swipe);
        dialog = new LoadingDialog(getActivity(), getString(R.string.msg_loading));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        swipe();
        if (!AppUtils.isInternetAvailable(getContext())) {
            dialog.dismiss();
            permissionDialog = new PermissionDialog(getActivity(), getString(R.string.title_internet), getString(R.string.msg_internet)
                    , getString(R.string.button_ok), R.drawable.ic_no_internet, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.btn_dialog_ok:
                            permissionDialog.dismiss();
                            ActivityUtils.startWifiSettings(getActivity());
                            break;

                    }
                }
            });
            permissionDialog.setCanceledOnTouchOutside(false);
            permissionDialog.show();

        }
        else {

                ApiInterface.retrofit.getallpendingdata().enqueue(new Callback<WebAPIResponse<List<JobInfo>>>() {
                    @Override
                    public void onResponse(Call<WebAPIResponse<List<JobInfo>>> call, Response<WebAPIResponse<List<JobInfo>>> response) {

                        dialog.dismiss();
                        if (response.isSuccessful()) {
                            try {
                                if (response.body().status==200) {
                                    List<JobInfo> jobInfo = response.body().response;
                                    adapter = new JobNotifiyAdapter(jobInfo, getActivity());
                                    recyclerView.setAdapter(adapter);
                                    String size;
                                    size = String.valueOf(jobInfo.size());
                                    if (size.equals("0")) {
                                        imageView.setVisibility(View.VISIBLE);
                                        swipelayout.setVisibility(View.GONE);
                                    } else
                                        imageView.setVisibility(View.GONE);
                                    swipelayout.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception ex) {
                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));


                            }
                        } else {

                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                        }
                    }

                    @Override
                    public void onFailure(Call<WebAPIResponse<List<JobInfo>>> call, Throwable t) {
                        dialog.dismiss();
                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                    }
                });


        }


        ApiInterface.retrofit.getcountpatch().enqueue(new Callback<WebAPIResponse<String>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<String>> call, Response<WebAPIResponse<String>> response) {
                if (response.isSuccessful()) {
                    try {
                        if (response.body().status==200) {

                            Log.d("TAAAG", "" + response.body().response);
                            Log.d("TAAAG", "" + response.body().status);

                        }

                    } catch (Exception ex) {
                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));


                    }
                } else {

                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                }

            }

            @Override
            public void onFailure(Call<WebAPIResponse<String>> call, Throwable t) {

            }
        });

        return view;
    }

    public void swipe()
    {
        swipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        swipelayout.setRefreshing(false);

                            ApiInterface.retrofit.getallpendingdata().enqueue(new Callback<WebAPIResponse<List<JobInfo>>>() {
                                @Override
                                public void onResponse(Call<WebAPIResponse<List<JobInfo>>> call, Response<WebAPIResponse<List<JobInfo>>> response) {

                                    dialog.dismiss();
                                    if (response.isSuccessful()) {
                                        try {
                                            if (response.body().status==200) {
                                                List<JobInfo> jobInfo = response.body().response;
                                                adapter = new JobNotifiyAdapter(jobInfo, getActivity());
                                                recyclerView.setAdapter(adapter);
                                                String size;
                                                size = String.valueOf(jobInfo.size());
//                                                if (size.equals("0")) {
//                                                    imageView.setVisibility(View.VISIBLE);
//                                                } else
//                                                    imageView.setVisibility(View.GONE);
                                            }
                                        } catch (Exception ex) {
                                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));


                                        }
                                    } else {

                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                                    }
                                }

                                @Override
                                public void onFailure(Call<WebAPIResponse<List<JobInfo>>> call, Throwable t) {
                                    dialog.dismiss();
                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                }
                            });



        ApiInterface.retrofit.getcountpatch().enqueue(new Callback<WebAPIResponse<String>>() {
                        @Override
                        public void onResponse(Call<WebAPIResponse<String>> call, Response<WebAPIResponse<String>> response) {
                            if (response.isSuccessful()) {
                                try {
                                    if (response.body().status==200) {

                                        Log.d("TAAAG", "" + response.body().response);
                                        Log.d("TAAAG", "" + response.body().status);

                                    }

                                } catch (Exception ex) {
                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));


                                }
                            } else {

                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                            }

                        }

                        @Override
                        public void onFailure(Call<WebAPIResponse<String>> call, Throwable t) {

                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                        }
                    });


                }
                }, 100);
            }
        });

    }


    @Override
    public void onResume() {

        super.onResume();

            ApiInterface.retrofit.getallpendingdata().enqueue(new Callback<WebAPIResponse<List<JobInfo>>>() {
                @Override
                public void onResponse(Call<WebAPIResponse<List<JobInfo>>> call, Response<WebAPIResponse<List<JobInfo>>> response) {

                    dialog.dismiss();
                    if (response.isSuccessful()) {
                        try {
                            if (response.body().status==200) {
                                Log.e("TAAAG",""+response);
                                List<JobInfo> jobInfo = response.body().response;
                                adapter = new JobNotifiyAdapter(jobInfo, getActivity());
                                recyclerView.setAdapter(adapter);
                                String size;
                                size = String.valueOf(jobInfo.size());
//                                    if (size.equals("0")) {
//                                     //   imageView.setVisibility(View.VISIBLE);
//                                    } else
//                                      //  imageView.setVisibility(View.GONE);
                            }
                        } catch (Exception ex) {
                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));


                        }
                    } else {

                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                    }
                }

                @Override
                public void onFailure(Call<WebAPIResponse<List<JobInfo>>> call, Throwable t) {
                    dialog.dismiss();
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                }
            });

        swipe();
    }
}
