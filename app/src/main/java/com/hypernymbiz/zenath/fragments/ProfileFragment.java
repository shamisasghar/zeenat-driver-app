package com.hypernymbiz.zenath.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hypernymbiz.zenath.HomeActivity;
import com.hypernymbiz.zenath.LoginActivity;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.api.ApiInterface;
import com.hypernymbiz.zenath.dialog.LoadingDialog;
import com.hypernymbiz.zenath.model.Profile;
import com.hypernymbiz.zenath.model.User;
import com.hypernymbiz.zenath.model.WebAPIResponse;
import com.hypernymbiz.zenath.model.WebApiRespone_profile;
import com.hypernymbiz.zenath.toolbox.ToolbarListener;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;
import com.hypernymbiz.zenath.utils.LoginUtils;
import com.hypernymbiz.zenath.utils.RunningResumeJobUtils;
import com.onesignal.OneSignal;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Metis on 19-Mar-18.
 */

public class ProfileFragment extends Fragment implements View.OnClickListener, ToolbarListener {

    private ViewHolder mHolder;
    TextView email, drivername, drivercnic, martialstatus, dof;
    private Toolbar mToolbar;
    private TextView mToolbarTitle;
    SharedPreferences pref;
    String getUserAssociatedEntity, Email, Driver_name, Driver_id, Driver_photo;
    CircleImageView img_profile;
    Context mContext;
    SharedPreferences.Editor editor;
    LoadingDialog dialog;
    Dialog dialog_password;
    TextInputEditText edit_currentpassword, edit_newpassword, edit_retypepassword;
    ImageView dialog_btn_ok, dialog_btn_cancel;

    private TextInputLayout inputLayout_currentpassword, inputLayout_newpassword, inputLayout_retype_password;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("Profile");
        }
        mContext = context;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        email = (TextView) view.findViewById(R.id.txt_Email);

        drivername = (TextView) view.findViewById(R.id.txt_drivername);
        drivercnic = (TextView) view.findViewById(R.id.txt_driverid);
        martialstatus = (TextView) view.findViewById(R.id.txt_gender);
        dof = (TextView) view.findViewById(R.id.txt_dateofjoin);
        img_profile = (CircleImageView) view.findViewById(R.id.img_driver_profile);
        pref = getActivity().getSharedPreferences("TAG", MODE_PRIVATE);
        Email = pref.getString("Email", "");
        email.setText(Email);

        dialog = new LoadingDialog(getActivity(), getString(R.string.msg_loading));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();



//        swipelayout();

        ApiInterface.retrofit.getprofile().enqueue(new Callback<WebAPIResponse<Profile>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<Profile>> call, Response<WebAPIResponse<Profile>> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {

                        if (response.body().status == 200) {

                            String driverName, driverCnic, drivergender, driverdof, url;
                            url = response.body().response.getPhoto();
                            driverName = response.body().response.getName();

//                                editor = pref.edit();
//                                editor.putString("Drivername", driverName);
//
//                                editor.commit();

                            driverCnic = String.valueOf(response.body().response.getCnic());
                            drivergender = response.body().response.getMaritalStatusLabel();
                            driverdof = response.body().response.getDateOfJoining();
                            drivername.setText(driverName);
                            drivercnic.setText(driverCnic);
                            martialstatus.setText(drivergender);
                            dof.setText(driverdof);
                            Glide.with(getContext()).load(url).into(img_profile);
                        } else {
                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                        }


                    } else {
                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                    }
                } catch (Exception ex) {
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));


                }


            }

            @Override
            public void onFailure(Call<WebAPIResponse<Profile>> call, Throwable t) {
                dialog.dismiss();
                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));
//                Snackbar snackbar = Snackbar.make(swipelayout, "Establish Network Connection!", Snackbar.LENGTH_SHORT);
//                View sbView = snackbar.getView();
//                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//                sbView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
//                textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorDialogToolbarText));
//                textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//                snackbar.show();
            }
        });


        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        //   EventBus.getDefault().post(new DrawerItemSelectedEvent(getString(R.string.drawer_profile)));
        mHolder.btn_sgnout.setOnClickListener(this);
        mHolder.btn_password.setOnClickListener(this);

        mHolder.btn_sgnout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    mHolder.btn_sgnout.setBackgroundColor(Color.parseColor("#f44336"));
                    return false;
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    mHolder.btn_sgnout.setBackgroundColor(Color.parseColor("#bf392f"));
                }


                return false;
            }
        });
        mHolder.btn_password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    mHolder.btn_password.setBackgroundColor(Color.parseColor("#36AB7A"));
                    return false;
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    mHolder.btn_password.setBackgroundColor(Color.parseColor("#FF2D8C64"));
                }


                return false;
            }
        });



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_signout:
                if (!RunningResumeJobUtils.isJobResumed(getContext())) {
                    LoginUtils.clearUser(getContext());
                    startActivity(new Intent(this.getContext(), LoginActivity.class));
                    getActivity().finish();
                    OneSignal.sendTag("email", "null");
                    RunningResumeJobUtils.clearJobResumed(getContext());


                } else {
                    LoginUtils.clearUser(getContext());
                    startActivity(new Intent(this.getContext(), LoginActivity.class));
                    getActivity().finish();
                    OneSignal.sendTag("email", "null");

                }


                break;

            case R.id.btn_password:

                dialog_password = new Dialog(getContext());
                dialog_password.setContentView(R.layout.dialog_change_password);

                edit_currentpassword = (TextInputEditText) dialog_password.findViewById(R.id.edittext_current_password);
                edit_newpassword = (TextInputEditText) dialog_password.findViewById(R.id.edittext_new_password);
                edit_retypepassword = (TextInputEditText) dialog_password.findViewById(R.id.edittext_retype_password);


                inputLayout_currentpassword = (TextInputLayout) dialog_password.findViewById(R.id.input_layout_current_password);
                inputLayout_newpassword = (TextInputLayout) dialog_password.findViewById(R.id.input_layout_newpassword);
                inputLayout_retype_password = (TextInputLayout) dialog_password.findViewById(R.id.input_layout_retype_password);


                dialog_btn_ok = (ImageView) dialog_password.findViewById(R.id.img_ok);
                dialog_btn_cancel = (ImageView) dialog_password.findViewById(R.id.img_cancel);


                dialog_btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Submit();

                    }

                });

                dialog_btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog_password.dismiss();
                    }
                });


                dialog_password.setCanceledOnTouchOutside(false);
                dialog_password.show();
                dialog_password.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                dialog_password.setCancelable(false);


                break;
        }

    }

    @Override
    public void setTitle(String title) {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(title);
        }
    }

    @Override
    public void onResume() {
        if (mContext instanceof ToolbarListener) {
            ((ToolbarListener) mContext).setTitle("Profile");
        }
        super.onResume();
        //  EventBus.getDefault().post(new DrawerItemSelectedEvent(getString(R.string.drawer_profile)));
    }

    public static class ViewHolder {

        Button btn_sgnout, btn_password;

        public ViewHolder(View view) {
            btn_sgnout = (Button) view.findViewById(R.id.btn_signout);
            btn_password = (Button) view.findViewById(R.id.btn_password);

        }
    }


    public void Submit() {

        if (!validateCurrentPassword())
            return;
        if (!validateNewPassword())
            return;
        if (!validateRetypePassword())
            return;


        String currentpassword = edit_currentpassword.getText().toString();
        String newpassword = edit_newpassword.getText().toString();
        String retypepassword = edit_retypepassword.getText().toString();

        HashMap<String, Object> body = new HashMap<>();
        body.put("email", Email);
        body.put("password", newpassword);
        body.put("old_password", currentpassword);

        if (retypepassword.equals(newpassword)) {
            ApiInterface.retrofit.changepassword(body).enqueue(new Callback<WebApiRespone_profile>() {
                @Override
                public void onResponse(Call<WebApiRespone_profile> call, Response<WebApiRespone_profile> response) {

                    try {
                        if (response.isSuccessful()) {

                            Toast.makeText(mContext, "Password Changed Successfully", Toast.LENGTH_SHORT).show();
                            LoginUtils.clearUser(getContext());
                            startActivity(new Intent(getContext(), LoginActivity.class));
                            getActivity().finish();
                            OneSignal.sendTag("email", "null");
                            dialog_password.dismiss();

                        }
                    } catch (Exception ex) {
                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                    }
                }

                @Override
                public void onFailure(Call<WebApiRespone_profile> call, Throwable t) {

                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                }
            });


        } else {
            Toast.makeText(getContext(), "Password Not Match", Toast.LENGTH_SHORT).show();

        }

    }


    private boolean validateCurrentPassword() {
        if (edit_currentpassword.getText().toString().trim().isEmpty()) {
            inputLayout_currentpassword.setError(getString(R.string.err_msg_current_password));
            //requestFocus(edit_email);
            return false;
        } else {
            inputLayout_currentpassword.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateNewPassword() {
        if (edit_newpassword.getText().toString().trim().isEmpty()) {
            inputLayout_newpassword.setError(getString(R.string.err_msg_new_password));
            //requestFocus(edit_email);
            return false;
        } else {
            inputLayout_newpassword.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateRetypePassword() {
        if (edit_retypepassword.getText().toString().trim().isEmpty()) {
            inputLayout_retype_password.setError(getString(R.string.err_msg_retype_password));
            //requestFocus(edit_email);
            return false;
        } else {
            inputLayout_retype_password.setErrorEnabled(false);
        }

        return true;
    }


}
