package com.hypernymbiz.zenath.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.adapter.RunningJobAdapter;
import com.hypernymbiz.zenath.api.ApiInterface;
import com.hypernymbiz.zenath.dialog.LoadingDialog;
import com.hypernymbiz.zenath.dialog.PermissionDialog;
import com.hypernymbiz.zenath.dialog.RequestDialog;
import com.hypernymbiz.zenath.model.ActionItem;
import com.hypernymbiz.zenath.model.DirectionsJSONParser;
import com.hypernymbiz.zenath.model.JobDetail;
import com.hypernymbiz.zenath.model.NotifyEvent;
import com.hypernymbiz.zenath.model.StartJob;
import com.hypernymbiz.zenath.model.WebAPIResponse;
import com.hypernymbiz.zenath.model.WebApiRespone_profile;
import com.hypernymbiz.zenath.toolbox.OnDialogButtonClick;
import com.hypernymbiz.zenath.toolbox.ToolbarListener;
import com.hypernymbiz.zenath.utils.ActivityUtils;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;
import com.hypernymbiz.zenath.utils.LoginUtils;
import com.hypernymbiz.zenath.utils.RuningJobUtils;
import com.hypernymbiz.zenath.utils.RunningResumeJobUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Metis on 09-Apr-18.
 */

public class RunningJobFragment extends Fragment implements OnMapReadyCallback, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    View view;
    GoogleMap googleMap;
    private RecyclerView recyclerView;
    RunningJobAdapter runningJobAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<ActionItem> actionItems;
    SupportMapFragment supportMapFragment;
    Button button_left, button_right, button_complete_status;
    SharedPreferences pref;
    String getUserAssociatedEntity, actual_end_time;
    String endlocation, driverlocation;
    GoogleApiClient googleApiClient;
    String id;
    Boolean check = true;
    LoadingDialog dialog;
    Context fContext;
    int marker_width = 200;
    int marker_height = 200;
    String truckname;
    Dialog Incident;
    Spinner dialog_spinner;
    EditText dialog_date, dialog_time, dialog_remarks;
    ImageView btn_dialog_check, btn_dialog_uncheck;
    Calendar calendar;
    String date, time, spinner_item;
    String actual_date_time;
    int Type;
    SharedPreferences.Editor editor;
    private PermissionDialog permissionDialog;
    private RequestDialog requestDialog;
    Marker marker;
    LatLng ll;
    boolean checklocation = true;
    ImageButton mylocation;
    Boolean getlocation = true;
    TextView status, truck_name;
    double count = 0.0;
    double percent;
    Calendar c;
    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 1000;  /* 1 sec */
    private long FASTEST_INTERVAL = 500; /* 1/2 sec */
    public static int counter = 0;
    private EventBus bus = EventBus.getDefault();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("Active Job");
        }
        fContext = context;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        View view, view1;

        inflater.inflate(R.menu.menu_running, menu);

        view1 = menu.findItem(R.id.incident).getActionView();

        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Incident = new Dialog(getContext());
                Incident.setContentView(R.layout.dialog_incident_job);
                dialog_spinner = (Spinner) Incident.findViewById(R.id.spinner_incident);
                dialog_date = (EditText) Incident.findViewById(R.id.edit_date);
                dialog_time = (EditText) Incident.findViewById(R.id.edit_time);
                dialog_remarks = (EditText) Incident.findViewById(R.id.edittxt_remark);
                btn_dialog_check = (ImageView) Incident.findViewById(R.id.img_ok);
                btn_dialog_uncheck = (ImageView) Incident.findViewById(R.id.img_cancel);
                calendar = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                date = df.format(calendar.getTime());
                SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                time = dff.format(calendar.getTime());
                dialog_date.setText(AppUtils.getFormattedDate(date));
                dialog_time.setText(AppUtils.getDateAndTime(time));


                SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                outputFmt.setTimeZone(TimeZone.getTimeZone("gmt"));
                actual_date_time = outputFmt.format(new Date());


                ArrayList<String> stringArrayList = new ArrayList<>();
                stringArrayList.add("Select a Type");
                stringArrayList.add("Tyre_brust");
                stringArrayList.add("Break_failure");
                stringArrayList.add("Accident");
                stringArrayList.add("Others");

                final ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, stringArrayList);
                dialog_spinner.setAdapter(itemsAdapter);
                dialog_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        if (position != 0) {

                            spinner_item = itemsAdapter.getItem(position);
                            Toast.makeText(getContext(), "" + spinner_item, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                Incident.setCanceledOnTouchOutside(false);
                Incident.show();
                Incident.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Incident.setCancelable(false);


                btn_dialog_uncheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Incident.dismiss();

                    }
                });

                btn_dialog_check.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try {

                            if (spinner_item.equals("Tyre_brust")) {
                                Type = 105;
                            } else if (spinner_item.equals("Break_failure")) {
                                Type = 106;

                            } else if (spinner_item.equals("Accident")) {

                                Type = 107;
                            } else if (spinner_item.equals("Others")) {
                                Type = 108;

                            }

                            HashMap<String, Object> body = new HashMap<>();
                            body.put("notes", dialog_remarks.getText().toString());
                            body.put("type", Type);
                            body.put("timestamp", actual_date_time);

                            ApiInterface.retrofit.reportincident(body).enqueue(new Callback<WebApiRespone_profile>() {
                                @Override
                                public void onResponse(Call<WebApiRespone_profile> call, Response<WebApiRespone_profile> response) {

                                    try {
                                        if (response.isSuccessful()) {

                                            Incident.dismiss();
                                            Toast.makeText(getContext(), "Sucessfull", Toast.LENGTH_SHORT).show();

                                        }
                                    } catch (Exception ex) {
                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                                    }
                                }

                                @Override
                                public void onFailure(Call<WebApiRespone_profile> call, Throwable t) {

                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                }
                            });

                        } catch (Exception ex) {

                            Toast.makeText(getContext(), "Incident Missing", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

            }
        });


    }


    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_running_job, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_running_job);
        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
        button_left = (Button) view.findViewById(R.id.btn_left);
        button_right = (Button) view.findViewById(R.id.btn_right);
        status = (TextView) view.findViewById(R.id.txt_job_status);
        truck_name = (TextView) view.findViewById(R.id.txt_assigned_truck);
        button_complete_status = (Button) view.findViewById(R.id.btn_complete);
        mylocation = (ImageButton) view.findViewById(R.id.btn_mylocation);
        pref = getActivity().getSharedPreferences("TAG", MODE_PRIVATE);
        getUserAssociatedEntity = LoginUtils.getUserAssociatedEntity(getContext());
        dialog = new LoadingDialog(getActivity(), getString(R.string.msg_loading));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        bus.register(this);


//        status.setText("Running");
        if (RuningJobUtils.getstatus(getContext()) == null || RuningJobUtils.getstatus(getContext()).equals("Running")) {
            RuningJobUtils.savestatus(getContext(), "Running");
            status.setText(RuningJobUtils.getstatus(getContext()));

        } else if (RuningJobUtils.getstatus(getContext()).equals("Suspend")) {
            RuningJobUtils.savestatus(getContext(), "Suspend");
            status.setText(RuningJobUtils.getstatus(getContext()));

        }


        truckname = pref.getString("Truckname", "");
        truck_name.setText(truckname);

        initMap();

//        Log.e("TAAAG", "show" + driverlocation);

        if (driverlocation == null) {
            dialog.show();

        }

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        initMap();
        return view;
    }


    private void initMap() {
        supportMapFragment.getMapAsync(this);

    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {

        Log.e("TAAAG", "onmabready");
        final MarkerOptions option;
        this.googleMap = googleMap;
        startLocationUpdates();

        mylocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 12.8f);
                googleMap.animateCamera(update);
            }
        });

        MapStyleOptions mapStyleOptions = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map);
        googleMap.setMapStyle(mapStyleOptions);


    }

    public void getlocation() {
        id = pref.getString("jobid", "");
        try {
            ApiInterface.retrofit.getdata(Integer.parseInt(id)).enqueue(new Callback<WebAPIResponse<JobDetail>>() {
                @Override
                public void onResponse(Call<WebAPIResponse<JobDetail>> call, Response<WebAPIResponse<JobDetail>> response) {

                    try {
                        if (response.isSuccessful()) {
                            actionItems = response.body().response.getActionItems();
                            runningJobAdapter = new RunningJobAdapter(actionItems, getContext());
                            recyclerView.setAdapter(runningJobAdapter);


                            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_bin_marker);
                            Bitmap b = bitmapdraw.getBitmap();
                            Bitmap smallMarker = Bitmap.createScaledBitmap(b, marker_width, marker_height, false);

                            String array[] = actionItems.get(0).getEntityLocation().split(",");
                            LatLng startbin = new LatLng(Double.parseDouble(array[0]), Double.parseDouble(array[1]));

//                            Toast.makeText(getContext(), "" + ll, Toast.LENGTH_SHORT).show();

                            String urll = getDirectionsUrl(ll, startbin);
                            RunningJobFragment.FetchUrl FetchUrll = new RunningJobFragment.FetchUrl();
                            FetchUrll.execute(urll);

                            if (actionItems.size() > 1) {

                                for (int i = 0; i < actionItems.size() - 1; i++) {

                                    try {
                                        String array1[] = actionItems.get(i).getEntityLocation().split(",");
                                        LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));

                                        String start_bin_name = actionItems.get(i).getLabel();


                                        String array2[] = actionItems.get(i + 1).getEntityLocation().split(",");
                                        LatLng dest = new LatLng(Double.parseDouble(array2[0]), Double.parseDouble(array2[1]));

                                        String end_bin_name = actionItems.get(i + 1).getLabel();

                                        googleMap.addMarker(new MarkerOptions().title(start_bin_name).position(start).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));
                                        googleMap.addMarker(new MarkerOptions().title(end_bin_name).position(dest).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));
                                        String url = getDirectionsUrl(start, dest);
                                        RunningJobFragment.FetchUrl FetchUrl = new RunningJobFragment.FetchUrl();
                                        FetchUrl.execute(url);
                                    } catch (Exception ex) {
                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                        break;
                                    }

                                }

                                for (int i = 0; i < actionItems.size(); i++) {

                                    if (!actionItems.get(i).getStatus().equals("Uncollected")) {

                                        count++;
                                    }

                                }
                                int listsize = actionItems.size();
                                percent = (count / listsize) * 100;
                                if (percent == 100.0) {
                                    if (response.body().response.getCheckPointLatLong()==null) {
                                        if (RuningJobUtils.getBtncomplete(getContext()) == null || RuningJobUtils.getBtncomplete(getContext()).equals("bincollected")) {
                                            RuningJobUtils.saveBtncomplete(getContext(), "bincollected");
                                            button_complete_status.setText("Bin Collected");
                                            button_complete_status.setBackgroundColor(Color.parseColor("#EC9940"));
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);

                                        } else if (RuningJobUtils.getBtncomplete(getContext()).equals("completeactivity")) {
                                            button_complete_status.setText("Complete Activity");
                                            button_complete_status.setBackgroundColor(Color.parseColor("#36AB7A"));
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);

                                        }
                                        button_complete_status.setVisibility(View.VISIBLE);
                                        button_left.setVisibility(View.GONE);
                                        button_right.setVisibility(View.GONE);

                                    }
                                    else
                                    {
                                        if (RuningJobUtils.getBtncomplete(getContext()) == null || RuningJobUtils.getBtncomplete(getContext()).equals("bincollected")) {
                                            RuningJobUtils.saveBtncomplete(getContext(), "bincollected");
                                            button_complete_status.setText("Bin Collected");
                                            button_complete_status.setBackgroundColor(Color.parseColor("#EC9940"));
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);
                                        } else if (RuningJobUtils.getBtncomplete(getContext()).equals("sortingfacility")) {
                                            RuningJobUtils.saveBtncomplete(getContext(), "sortingfacility");
                                            button_complete_status.setText("Sorting Facility");
                                            button_complete_status.setBackgroundColor(Color.parseColor("#0097a7"));
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);

                                        } else if (RuningJobUtils.getBtncomplete(getContext()).equals("completeactivity")) {
                                            button_complete_status.setText("Complete Activity");
                                            button_complete_status.setBackgroundColor(Color.parseColor("#36AB7A"));
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);

                                        }
                                        button_complete_status.setVisibility(View.VISIBLE);
                                        button_left.setVisibility(View.GONE);
                                        button_right.setVisibility(View.GONE);

                                    }
                                }
                                RunningResumeJobUtils.savepercent(getContext(), Double.toString(percent));
//                                editor = pref.edit();
//                                editor.putString("activity_percentage", String.valueOf(percent));
//                                editor.apply();

                            } else {
                                try {
                                    String array1[] = actionItems.get(0).getEntityLocation().split(",");
                                    LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));

                                    String start_bin_name = actionItems.get(0).getLabel();

                                    googleMap.addMarker(new MarkerOptions().title(start_bin_name).position(start).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));

                                    for (int i = 0; i < actionItems.size(); i++) {

                                        if (!actionItems.get(i).getStatus().equals("Uncollected")) {

                                            count++;
                                        }

                                    }
                                    int listsize = actionItems.size();
                                    percent = (count / listsize) * 100;
                                    if (percent == 100.0) {
                                        if(response.body().response.getCheckPointLatLong()==null) {

                                            if (RuningJobUtils.getBtncomplete(getContext()) == null || RuningJobUtils.getBtncomplete(getContext()).equals("bincollected")) {
                                                RuningJobUtils.saveBtncomplete(getContext(), "bincollected");
                                                button_complete_status.setText("Bin Collected");
                                                button_complete_status.setBackgroundColor(Color.parseColor("#EC9940"));
                                                button_left.setVisibility(View.GONE);
                                                button_right.setVisibility(View.GONE);

                                            } else if (RuningJobUtils.getBtncomplete(getContext()).equals("completeactivity")) {
                                                button_complete_status.setText("Complete Activity");
                                                button_complete_status.setBackgroundColor(Color.parseColor("#36AB7A"));
                                                button_left.setVisibility(View.GONE);
                                                button_right.setVisibility(View.GONE);

                                            }
                                            button_complete_status.setVisibility(View.VISIBLE);
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);
                                        }
                                        else
                                        {
                                            if (RuningJobUtils.getBtncomplete(getContext()) == null || RuningJobUtils.getBtncomplete(getContext()).equals("bincollected")) {
                                                RuningJobUtils.saveBtncomplete(getContext(), "bincollected");
                                                button_complete_status.setText("Bin Collected");
                                                button_complete_status.setBackgroundColor(Color.parseColor("#EC9940"));
                                                button_left.setVisibility(View.GONE);
                                                button_right.setVisibility(View.GONE);
                                            } else if (RuningJobUtils.getBtncomplete(getContext()).equals("sortingfacility")) {
                                                RuningJobUtils.saveBtncomplete(getContext(), "sortingfacility");
                                                button_complete_status.setText("Sorting Facility");
                                                button_complete_status.setBackgroundColor(Color.parseColor("#0097a7"));
                                                button_left.setVisibility(View.GONE);
                                                button_right.setVisibility(View.GONE);

                                            } else if (RuningJobUtils.getBtncomplete(getContext()).equals("completeactivity")) {
                                                button_complete_status.setText("Complete Activity");
                                                button_complete_status.setBackgroundColor(Color.parseColor("#36AB7A"));
                                                button_left.setVisibility(View.GONE);
                                                button_right.setVisibility(View.GONE);

                                            }
                                            button_complete_status.setVisibility(View.VISIBLE);
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);

                                        }

                                    }
                                    RunningResumeJobUtils.savepercent(getContext(), Double.toString(percent));
//                                    editor = pref.edit();
//                                    editor.putString("activity_percentage", String.valueOf(percent));
//                                    editor.commit();


                                } catch (Exception ex) {

                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                }

                            }

                            BitmapDrawable dumpmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_dump_marker);
                            Bitmap dump = dumpmarker.getBitmap();
                            Bitmap Dumpmarker = Bitmap.createScaledBitmap(dump, marker_width, marker_height, false);

                            BitmapDrawable sortmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_sort_marker);
                            Bitmap sort = sortmarker.getBitmap();
                            Bitmap Sortmarker = Bitmap.createScaledBitmap(sort, marker_width, marker_height, false);


                            try {
                                //for last one

                                String dumpname = response.body().response.getEndPointName().toString();
                               // String sortname = response.body().response.getCheckPointName().toString();
                                String dumpArray[] = response.body().response.getEndPointLatLong().toString().split(",");
                            //    String sortArray[] = response.body().response.getCheckPointLatLong().toString().split(",");


//                                if(sortname==null&&response.body().response.getCheckPointLatLong()==null)
//                                {
//                                    LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
//                                    // LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));
//
//                                    String lastBin[] = actionItems.get(actionItems.size() - 1).getEntityLocation().split(",");
//
//                                    //  googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));
//                                    googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));
//
//                                    String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])), dumping);
//                                    FetchUrl FetchUrl = new FetchUrl();
//                                    FetchUrl.execute(url);
//
////                                        String urll = getDirectionsUrl(sorting, dumping);
////                                        FetchUrl FetchUrll = new FetchUrl();
////                                        FetchUrll.execute(urll);
////
//                                }
//                                else {

                                    LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
                                  //  LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));

                                    String lastBin[] = actionItems.get(actionItems.size() - 1).getEntityLocation().split(",");

                                   // googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));
                                    googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));

                                    String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])), dumping);
                                    FetchUrl FetchUrl = new FetchUrl();
                                    FetchUrl.execute(url);

//                                    String urlll = getDirectionsUrl(sorting, dumping);
//                                    FetchUrl FetchUrlll = new FetchUrl();
//                                    FetchUrlll.execute(urlll);
                              //  }



                            } catch (Exception ex) {
                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                            }

                        }
                    } catch (Exception ex) {

                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                    }
                }

                @Override
                public void onFailure(Call<WebAPIResponse<JobDetail>> call, Throwable t) {
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                }
            });


        } catch (Exception ex) {

            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
        }
    }


    @SuppressLint("RestrictedApi")
    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        final LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.getFusedLocationProviderClient(getActivity()).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        counter++;
                        try {
                            if (counter > 1) {
                                onLocationChanged(locationResult.getLastLocation());
                                LocationServices.getFusedLocationProviderClient(getActivity()).removeLocationUpdates(this);
                            }
                        } catch (Exception ex) {

                        }
                    }
                },
                Looper.myLooper());
    }


    @Override
    public void onPause() {

        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);

        super.onPause();
    }


    @Override
    public void onLocationChanged(Location location) {
        final MarkerOptions option;
        if (marker != null) {

            marker.remove();
        }

        Double lat, lng;

        lat = location.getLatitude();
        lng = location.getLongitude();

        try {
            option = new MarkerOptions().title("Driver").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location)).position(new LatLng(lat, lng));
            marker = googleMap.addMarker(option);
        } catch (Exception ex) {

        }

        if (googleMap != null) {

            if (checklocation == true) {
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 12.8f);
                googleMap.animateCamera(update);
                checklocation = false;

            }
        }


        endlocation = lat.toString() + "," + lng.toString();
        driverlocation = lat.toString() + "," + lng.toString();


        if (getlocation == true) {
            getlocation();
            ll = new LatLng(location.getLatitude(), location.getLongitude());
            getlocation = false;
        }


        dialog.dismiss();
//        Log.e("TAAAG", "unshow" + driverlocation);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResume() {
        super.onResume();

        buildGoogleApiClient();


        if (RuningJobUtils.getBtnRight(getContext()) == null || RuningJobUtils.getBtnRight(getContext()).equals("Suspend")) {
            button_right.setText("Suspend");
            button_left.setText("Mark Incomplete");
            button_right.setBackgroundColor(Color.parseColor("#EC9940"));

            button_left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Endjob();
//                    Toast.makeText(getContext(), button_left.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            button_right.setText("Resume");
            button_left.setText("Mark Incomplete");
            button_right.setBackgroundColor(Color.parseColor("#36AB7A"));

            button_left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Endjob();
//                button_right.setText("Resume");
//                    Toast.makeText(getContext(), button_left.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });


        }


        button_left.setVisibility(View.VISIBLE);
        button_right.setVisibility(View.VISIBLE);

        button_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getContext(), button_right.getText().toString(), Toast.LENGTH_SHORT).show();
                if (button_right.getText().equals("Suspend")) {

                    Suspendjob();
                    button_right.setBackgroundColor(Color.parseColor("#EC9940"));
                } else if (button_right.getText().equals("Resume")) {

                    Resumejob();
                    button_right.setBackgroundColor(Color.parseColor("#36AB7A"));

                } else {
                    button_right.setText("Suspend");
                    RuningJobUtils.saveBtnRight(getContext(), "Suspend");
                    Suspendjob();
                }
            }
        });


        button_complete_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                            , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (view.getId()) {
                                case R.id.btn_dialog_ok:
                                    permissionDialog.dismiss();
                                    ActivityUtils.startWifiSettings(getContext());
                                    break;
                            }
                        }
                    });
                    permissionDialog.setCancelable(false);
                    permissionDialog.setCanceledOnTouchOutside(false);
                    permissionDialog.show();
                    return;
                } else {

                    if (RuningJobUtils.getBtncomplete(getContext()).equals("bincollected")) {
                        requestDialog = new RequestDialog(getContext(), getString(R.string.title_bin_collected),
                                getString(R.string.msg_bin_collected), new OnDialogButtonClick() {
                            @Override
                            public void onButtonClick(View view, String data) {
                                String getremark = data;

                                switch (view.getId()) {
                                    case R.id.img_ok:
                                        RuningJobUtils.saveBtncomplete(getContext(), "completeactivity");
                                        button_complete_status.setText("Complete activity");
                                        button_complete_status.setBackgroundColor(Color.parseColor("#36AB7A"));
//                                    Toast.makeText(getContext(), "sorting facility "+ RuningJobUtils.getBtncomplete(getContext()).toString(), Toast.LENGTH_SHORT).show();

                                        requestDialog.dismiss();
                                        break;
                                    case R.id.img_cancel:
                                        requestDialog.dismiss();
//                                    Toast.makeText(getContext(), "" + data, Toast.LENGTH_SHORT).show();
                                        break;
                                }

                            }

                        });
                        requestDialog.show();
                    }
//                     else if (RuningJobUtils.getBtncomplete(getContext()).equals("sortingfacility")) {
//                        requestDialog = new RequestDialog(getContext(), getString(R.string.title_sorting_facility),
//                                getString(R.string.msg_sorting_facility), new OnDialogButtonClick() {
//                            @Override
//                            public void onButtonClick(View view, String data) {
//                                String getremark = data;
//
//                                switch (view.getId()) {
//                                    case R.id.img_ok:
//                                        RuningJobUtils.saveBtncomplete(getContext(), "completeactivity");
//                                        button_complete_status.setText("Complete Activity");
//                                        button_complete_status.setBackgroundColor(Color.parseColor("#36AB7A"));
//
//                                        requestDialog.dismiss();
//                                        break;
//                                    case R.id.img_cancel:
//                                        requestDialog.dismiss();
////                                    Toast.makeText(getContext(), "" + data, Toast.LENGTH_SHORT).show();
//                                        break;
//                                }
//
//                            }
//
//                        });
//                        requestDialog.show();
//
//                    }
                    else {
                        requestDialog = new RequestDialog(getContext(), getString(R.string.title_complete),
                                getString(R.string.msg_complete), new OnDialogButtonClick() {
                            @Override
                            public void onButtonClick(View view, String data) {
                                final String getremark = data;

                                switch (view.getId()) {
                                    case R.id.img_ok:

                                        id = pref.getString("jobid", "");

                                        HashMap<String, Object> body = new HashMap<>();
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        df.setTimeZone(TimeZone.getTimeZone("gmt"));
                                        String completetime = df.format(Calendar.getInstance().getTime());

                                        body.put("job_id", id);
                                        body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                        body.put("flag", 55);
                                        body.put("timestamp", completetime);
                                        body.put("lat_long", driverlocation);
                                        body.put("remarks", getremark);

                                        ApiInterface.retrofit.canceljob(body).enqueue(new Callback<WebAPIResponse<StartJob>>() {
                                            @Override
                                            public void onResponse(Call<WebAPIResponse<StartJob>> call, Response<WebAPIResponse<StartJob>> response) {

                                                if (response.isSuccessful()) {
                                                    try {
                                                        getActivity().onBackPressed();
                                                        getActivity().finish();
//                                    requestDialog.dismiss();
                                                        RunningResumeJobUtils.clearJobResumed(getContext());
                                                        RuningJobUtils.clearJobbtnstatus(getContext());


                                                    } catch (Exception ex) {
                                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                                    }
                                                } else {
                                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<WebAPIResponse<StartJob>> call, Throwable t) {
                                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                            }
                                        });

                                        break;
                                    case R.id.img_cancel:
                                        requestDialog.dismiss();
                                        Toast.makeText(getContext(), "" + data, Toast.LENGTH_SHORT).show();
                                        break;
                                }


                            }

                        });
                        requestDialog.show();
                    }
                }
            }
        });

    }


//    }


    public void Endjob() {


        final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                    , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.btn_dialog_ok:
                            permissionDialog.dismiss();
                            ActivityUtils.startWifiSettings(getContext());
                            break;
                    }
                }
            });
            permissionDialog.setCancelable(false);
            permissionDialog.setCanceledOnTouchOutside(false);
            permissionDialog.show();
            return;
        } else {

            requestDialog = new RequestDialog(getContext(), getString(R.string.title_abort),
                    getString(R.string.msg_abort), new OnDialogButtonClick() {
                @Override
                public void onButtonClick(View view, String data) {

                    switch (view.getId()) {
                        case R.id.img_ok:
                            id = pref.getString("jobid", "");
                            String getremark = data;

                            HashMap<String, Object> body = new HashMap<>();
                            if (id != null) {
                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String endtime = df.format(Calendar.getInstance().getTime());


                                body.put("job_id", Integer.parseInt(id));
                                body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                body.put("flag", 117);
                                body.put("lat_long", driverlocation);
                                body.put("timestamp", endtime);
                                body.put("remarks", getremark);


                            }

//                        ApiInterface.retrofit.Checkjob(body).enqueue(new Callback<Object>() {
//                            @Override
//                            public void onResponse(Call<Object> call, Response<Object> response) {
//
//                                if (response.isSuccessful()) {
//                                    try {
//                                        getActivity().onBackPressed();
//                                        getActivity().finish();
//                                        requestDialog.dismiss();
//
//                                    } catch (Exception ex) {
//                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));
//
//                                    }
//                                } else {
//                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
//                                }
//
//                            }
//
//                            @Override
//                            public void onFailure(Call<Object> call, Throwable t) {
//                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));
//
//                            }
//                        });
                            ApiInterface.retrofit.canceljob(body).enqueue(new Callback<WebAPIResponse<StartJob>>() {
                                @Override
                                public void onResponse(Call<WebAPIResponse<StartJob>> call, Response<WebAPIResponse<StartJob>> response) {

                                    if (response.isSuccessful()) {
                                        try {
                                            getActivity().onBackPressed();
                                            getActivity().finish();
                                            requestDialog.dismiss();
                                            RunningResumeJobUtils.clearJobResumed(getContext());
                                            RuningJobUtils.clearJobbtnstatus(getContext());

                                        } catch (Exception ex) {
                                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                        }
                                    } else {
                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                    }

                                }

                                @Override
                                public void onFailure(Call<WebAPIResponse<StartJob>> call, Throwable t) {
                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                }
                            });

                            break;
                        case R.id.img_cancel:
                            requestDialog.dismiss();
//                            Toast.makeText(getContext(), "" + data, Toast.LENGTH_SHORT).show();
                            break;

                    }
                }
            });
            requestDialog.show();
        }

    }

    public void Suspendjob() {

        final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                    , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.btn_dialog_ok:
                            permissionDialog.dismiss();
                            ActivityUtils.startWifiSettings(getContext());
                            break;
                    }
                }
            });
            permissionDialog.setCancelable(false);
            permissionDialog.setCanceledOnTouchOutside(false);
            permissionDialog.show();
            return;
        } else {

            requestDialog = new RequestDialog(getContext(), getString(R.string.title_suspend),
                    getString(R.string.msg_suspend), new OnDialogButtonClick() {
                @Override
                public void onButtonClick(View view, String data) {

                    switch (view.getId()) {
                        case R.id.img_ok:
                            id = pref.getString("jobid", "");
                            String getremark = data;

                            HashMap<String, Object> body = new HashMap<>();
                            if (id != null) {
                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                df.setTimeZone(TimeZone.getTimeZone("gmt"));
                                String suspendtime = df.format(Calendar.getInstance().getTime());

                                body.put("job_id", Integer.parseInt(id));
                                body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                body.put("flag", 78);
                                body.put("lat_long", driverlocation);
                                body.put("timestamp", suspendtime);
                                body.put("remarks", getremark);


//                                Toast.makeText(getContext(), "" + suspendtime, Toast.LENGTH_SHORT).show();

                            }
                            ApiInterface.retrofit.canceljob(body).enqueue(new Callback<WebAPIResponse<StartJob>>() {
                                @Override
                                public void onResponse(Call<WebAPIResponse<StartJob>> call, Response<WebAPIResponse<StartJob>> response) {

                                    if (response.isSuccessful()) {
                                        try {
                                            button_right.setText("Resume");
//                                        status.setText("Suspend");
                                            RuningJobUtils.savestatus(getContext(), "Suspend");
                                            status.setText(RuningJobUtils.getstatus(getContext()));
                                            button_right.setBackgroundColor(Color.parseColor("#36AB7A"));
                                            RuningJobUtils.saveBtnRight(getContext(), "Resume");

                                        } catch (Exception ex) {
                                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                        }
                                    } else {
                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                    }

                                }

                                @Override
                                public void onFailure(Call<WebAPIResponse<StartJob>> call, Throwable t) {
                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                }
                            });

                            requestDialog.dismiss();
                            break;
                        case R.id.img_cancel:
                            requestDialog.dismiss();
//                            Toast.makeText(getContext(), "" + data, Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            });
            requestDialog.show();
        }
    }

    public void Resumejob() {

        final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                    , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.btn_dialog_ok:
                            permissionDialog.dismiss();
                            ActivityUtils.startWifiSettings(getContext());
                            break;
                    }
                }
            });
            permissionDialog.setCancelable(false);
            permissionDialog.setCanceledOnTouchOutside(false);
            permissionDialog.show();
            return;
        } else {

            requestDialog = new RequestDialog(getContext(), getString(R.string.title_resume),
                    getString(R.string.msg_resume), new OnDialogButtonClick() {
                @Override
                public void onButtonClick(View view, String data) {

                    switch (view.getId()) {
                        case R.id.img_ok:
                            id = pref.getString("jobid", "");
                            String getremark = data;

                            HashMap<String, Object> body = new HashMap<>();
                            if (id != null) {

                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                df.setTimeZone(TimeZone.getTimeZone("gmt"));
                                String resumetime = df.format(Calendar.getInstance().getTime());

                                body.put("job_id", Integer.parseInt(id));
                                body.put("driver_id", Integer.parseInt(getUserAssociatedEntity));
                                body.put("flag", 79);
                                body.put("lat_long", driverlocation);
                                body.put("timestamp", resumetime);
                                body.put("remarks", getremark);

//                                Toast.makeText(getContext(), "" + resumetime, Toast.LENGTH_SHORT).show();


                            }
                            ApiInterface.retrofit.canceljob(body).enqueue(new Callback<WebAPIResponse<StartJob>>() {
                                @Override
                                public void onResponse(Call<WebAPIResponse<StartJob>> call, Response<WebAPIResponse<StartJob>> response) {

                                    if (response.isSuccessful()) {
                                        button_right.setText("Suspend");
//                                    status.setText("Suspend");
                                        RuningJobUtils.savestatus(getContext(), "Running");
                                        status.setText(RuningJobUtils.getstatus(getContext()));
                                        button_right.setBackgroundColor(Color.parseColor("#EC9940"));
                                        RuningJobUtils.saveBtnRight(getContext(), "Suspend");

                                        try {

                                        } catch (Exception ex) {
                                            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                        }
                                    } else {
                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                    }

                                }

                                @Override
                                public void onFailure(Call<WebAPIResponse<StartJob>> call, Throwable t) {
                                    dialog.dismiss();
                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                                }
                            });

                            requestDialog.dismiss();
                            break;
                        case R.id.img_cancel:
                            requestDialog.dismiss();
//                            Toast.makeText(getContext(), "" + data, Toast.LENGTH_SHORT).show();
                            break;

                    }
                }
            });
            requestDialog.show();
        }

    }

    public void Locationcheck() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            permissionDialog = new PermissionDialog(getContext(), getString(R.string.title_location), getString(R.string.msg_location)
                    , getString(R.string.button_ok), R.drawable.ic_no_location, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.btn_dialog_ok:
                            permissionDialog.dismiss();
                            ActivityUtils.startWifiSettings(getActivity());
                            break;

                    }
                }
            });
            permissionDialog.setCancelable(false);
            permissionDialog.setCanceledOnTouchOutside(false);
            permissionDialog.show();
            return;
        }
    }


    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();
    }


    private String getDirectionsUrl(LatLng start, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + start.latitude + "," + start.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        } catch (Exception e) {
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {

            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            RunningJobFragment.ParserTask parserTask = new RunningJobFragment.ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;

            PolylineOptions lineOptions = null;
            if (result != null) {

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<>();
                    lineOptions = new PolylineOptions();
                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);
                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(10);
                    lineOptions.color(R.color.colorPrimary);
                }

                // Drawing polyline in the Google Map for the i-th route
                if (lineOptions != null) {
                    googleMap.addPolyline(lineOptions);
                } else {
                }
            } else {

                if (check == true) {
                }
                check = false;

            }
        }
    }

    @Subscribe
    public void onEvent(NotifyEvent event) {
        count=0.0;
        id = pref.getString("jobid", "");
        try {
            ApiInterface.retrofit.getdata(Integer.parseInt(id)).enqueue(new Callback<WebAPIResponse<JobDetail>>() {
                @Override
                public void onResponse(Call<WebAPIResponse<JobDetail>> call, Response<WebAPIResponse<JobDetail>> response) {

                    try {
                        if (response.isSuccessful()) {
                            actionItems = response.body().response.getActionItems();
                            runningJobAdapter = new RunningJobAdapter(actionItems, getContext());
                            recyclerView.setAdapter(runningJobAdapter);


                            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_bin_marker);
                            Bitmap b = bitmapdraw.getBitmap();
                            Bitmap smallMarker = Bitmap.createScaledBitmap(b, marker_width, marker_height, false);

                            String array[] = actionItems.get(0).getEntityLocation().split(",");
                            LatLng startbin = new LatLng(Double.parseDouble(array[0]), Double.parseDouble(array[1]));

//                            Toast.makeText(getContext(), "" + ll, Toast.LENGTH_SHORT).show();

                            String urll = getDirectionsUrl(ll, startbin);
                            RunningJobFragment.FetchUrl FetchUrll = new RunningJobFragment.FetchUrl();
                            FetchUrll.execute(urll);

                            if (actionItems.size() > 1) {

                                for (int i = 0; i < actionItems.size() - 1; i++) {

                                    try {
                                        String array1[] = actionItems.get(i).getEntityLocation().split(",");
                                        LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));

                                        String start_bin_name = actionItems.get(i).getLabel();


                                        String array2[] = actionItems.get(i + 1).getEntityLocation().split(",");
                                        LatLng dest = new LatLng(Double.parseDouble(array2[0]), Double.parseDouble(array2[1]));

                                        String end_bin_name = actionItems.get(i + 1).getLabel();

                                        googleMap.addMarker(new MarkerOptions().title(start_bin_name).position(start).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));
                                        googleMap.addMarker(new MarkerOptions().title(end_bin_name).position(dest).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));
                                        String url = getDirectionsUrl(start, dest);
                                        RunningJobFragment.FetchUrl FetchUrl = new RunningJobFragment.FetchUrl();
                                        FetchUrl.execute(url);
                                    } catch (Exception ex) {
                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                        break;
                                    }

                                }

                                for (int i = 0; i < actionItems.size(); i++) {

                                    if (!actionItems.get(i).getStatus().equals("Uncollected")) {

                                        count++;
                                    }

                                }
                                int listsize = actionItems.size();
                                percent = (count / listsize) * 100;
                                if (percent == 100.0) {
                                    if (response.body().response.getCheckPointLatLong()==null) {
                                        if (RuningJobUtils.getBtncomplete(getContext()) == null || RuningJobUtils.getBtncomplete(getContext()).equals("bincollected")) {
                                            RuningJobUtils.saveBtncomplete(getContext(), "bincollected");
                                            button_complete_status.setText("Bin Collected");
                                            button_complete_status.setBackgroundColor(Color.parseColor("#EC9940"));
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);

                                        } else if (RuningJobUtils.getBtncomplete(getContext()).equals("completeactivity")) {
                                            button_complete_status.setText("Complete Activity");
                                            button_complete_status.setBackgroundColor(Color.parseColor("#36AB7A"));
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);

                                        }
                                        button_complete_status.setVisibility(View.VISIBLE);
                                        button_left.setVisibility(View.GONE);
                                        button_right.setVisibility(View.GONE);

                                    }
                                    else
                                    {
                                        if (RuningJobUtils.getBtncomplete(getContext()) == null || RuningJobUtils.getBtncomplete(getContext()).equals("bincollected")) {
                                            RuningJobUtils.saveBtncomplete(getContext(), "bincollected");
                                            button_complete_status.setText("Bin Collected");
                                            button_complete_status.setBackgroundColor(Color.parseColor("#EC9940"));
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);
                                        } else if (RuningJobUtils.getBtncomplete(getContext()).equals("sortingfacility")) {
                                            RuningJobUtils.saveBtncomplete(getContext(), "sortingfacility");
                                            button_complete_status.setText("Sorting Facility");
                                            button_complete_status.setBackgroundColor(Color.parseColor("#0097a7"));
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);

                                        } else if (RuningJobUtils.getBtncomplete(getContext()).equals("completeactivity")) {
                                            button_complete_status.setText("Complete Activity");
                                            button_complete_status.setBackgroundColor(Color.parseColor("#36AB7A"));
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);

                                        }
                                        button_complete_status.setVisibility(View.VISIBLE);
                                        button_left.setVisibility(View.GONE);
                                        button_right.setVisibility(View.GONE);

                                    }
                                }
                                RunningResumeJobUtils.savepercent(getContext(), Double.toString(percent));
//                                editor = pref.edit();
//                                editor.putString("activity_percentage", String.valueOf(percent));
//                                editor.apply();

                            } else {
                                try {
                                    String array1[] = actionItems.get(0).getEntityLocation().split(",");
                                    LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));

                                    String start_bin_name = actionItems.get(0).getLabel();

                                    googleMap.addMarker(new MarkerOptions().title(start_bin_name).position(start).icon((BitmapDescriptorFactory.fromBitmap(smallMarker))));

                                    for (int i = 0; i < actionItems.size(); i++) {

                                        if (!actionItems.get(i).getStatus().equals("Uncollected")) {

                                            count++;
                                        }

                                    }
                                    int listsize = actionItems.size();
                                    percent = (count / listsize) * 100;
                                    if (percent == 100.0) {
                                        if(response.body().response.getCheckPointLatLong()==null) {

                                            if (RuningJobUtils.getBtncomplete(getContext()) == null || RuningJobUtils.getBtncomplete(getContext()).equals("bincollected")) {
                                                RuningJobUtils.saveBtncomplete(getContext(), "bincollected");
                                                button_complete_status.setText("Bin Collected");
                                                button_complete_status.setBackgroundColor(Color.parseColor("#EC9940"));
                                                button_left.setVisibility(View.GONE);
                                                button_right.setVisibility(View.GONE);

                                            } else if (RuningJobUtils.getBtncomplete(getContext()).equals("completeactivity")) {
                                                button_complete_status.setText("Complete Activity");
                                                button_complete_status.setBackgroundColor(Color.parseColor("#36AB7A"));
                                                button_left.setVisibility(View.GONE);
                                                button_right.setVisibility(View.GONE);

                                            }
                                            button_complete_status.setVisibility(View.VISIBLE);
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);
                                        }
                                        else
                                        {
                                            if (RuningJobUtils.getBtncomplete(getContext()) == null || RuningJobUtils.getBtncomplete(getContext()).equals("bincollected")) {
                                                RuningJobUtils.saveBtncomplete(getContext(), "bincollected");
                                                button_complete_status.setText("Bin Collected");
                                                button_complete_status.setBackgroundColor(Color.parseColor("#EC9940"));
                                                button_left.setVisibility(View.GONE);
                                                button_right.setVisibility(View.GONE);
                                            } else if (RuningJobUtils.getBtncomplete(getContext()).equals("sortingfacility")) {
                                                RuningJobUtils.saveBtncomplete(getContext(), "sortingfacility");
                                                button_complete_status.setText("Sorting Facility");
                                                button_complete_status.setBackgroundColor(Color.parseColor("#0097a7"));
                                                button_left.setVisibility(View.GONE);
                                                button_right.setVisibility(View.GONE);

                                            } else if (RuningJobUtils.getBtncomplete(getContext()).equals("completeactivity")) {
                                                button_complete_status.setText("Complete Activity");
                                                button_complete_status.setBackgroundColor(Color.parseColor("#36AB7A"));
                                                button_left.setVisibility(View.GONE);
                                                button_right.setVisibility(View.GONE);

                                            }
                                            button_complete_status.setVisibility(View.VISIBLE);
                                            button_left.setVisibility(View.GONE);
                                            button_right.setVisibility(View.GONE);

                                        }

                                    }
                                    RunningResumeJobUtils.savepercent(getContext(), Double.toString(percent));
//                                    editor = pref.edit();
//                                    editor.putString("activity_percentage", String.valueOf(percent));
//                                    editor.commit();


                                } catch (Exception ex) {

                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                }

                            }

                            BitmapDrawable dumpmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_dump_marker);
                            Bitmap dump = dumpmarker.getBitmap();
                            Bitmap Dumpmarker = Bitmap.createScaledBitmap(dump, marker_width, marker_height, false);

                            BitmapDrawable sortmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_sort_marker);
                            Bitmap sort = sortmarker.getBitmap();
                            Bitmap Sortmarker = Bitmap.createScaledBitmap(sort, marker_width, marker_height, false);


                            try {
                                //for last one

                                String dumpname = response.body().response.getEndPointName().toString();
                             //   String sortname = response.body().response.getCheckPointName().toString();
                                String dumpArray[] = response.body().response.getEndPointLatLong().toString().split(",");
                            //    String sortArray[] = response.body().response.getCheckPointLatLong().toString().split(",");


//                                if(sortname==null&&response.body().response.getCheckPointLatLong()==null)
//                                {
//                                    LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
//                                    // LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));
//
//                                    String lastBin[] = actionItems.get(actionItems.size() - 1).getEntityLocation().split(",");
//
//                                    //  googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));
//                                    googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));
//
//                                    String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])), dumping);
//                                    FetchUrl FetchUrl = new FetchUrl();
//                                    FetchUrl.execute(url);
//
////                                        String urll = getDirectionsUrl(sorting, dumping);
////                                        FetchUrl FetchUrll = new FetchUrl();
////                                        FetchUrll.execute(urll);
////
//                                }
//                                else {

                                    LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
                                   // LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));

                                    String lastBin[] = actionItems.get(actionItems.size() - 1).getEntityLocation().split(",");

                                  //  googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));
                                    googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));

                                    String url = getDirectionsUrl(new LatLng(Double.parseDouble(lastBin[0]), Double.parseDouble(lastBin[1])), dumping);
                                    FetchUrl FetchUrl = new FetchUrl();
                                    FetchUrl.execute(url);
//
//                                    String urlll = getDirectionsUrl(sorting, dumping);
//                                    FetchUrl FetchUrlll = new FetchUrl();
//                                    FetchUrlll.execute(urlll);
                               // }



                            } catch (Exception ex) {
                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                            }

                        }
                    } catch (Exception ex) {

                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                    }
                }

                @Override
                public void onFailure(Call<WebAPIResponse<JobDetail>> call, Throwable t) {
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                }
            });
        } catch (Exception ex) {

            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
        }


    }


}
