package com.hypernymbiz.zenath.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.adapter.CompleteJobAdapter;
import com.hypernymbiz.zenath.adapter.DialogAdapter;
import com.hypernymbiz.zenath.adapter.JobDetailAdapter;
import com.hypernymbiz.zenath.adapter.UpcomingAdapter;
import com.hypernymbiz.zenath.api.ApiInterface;
import com.hypernymbiz.zenath.dialog.LoadingDialog;
import com.hypernymbiz.zenath.model.Completed;
import com.hypernymbiz.zenath.model.GetAppJobs;
import com.hypernymbiz.zenath.model.Upcoming;
import com.hypernymbiz.zenath.model.UpcomingActionItem;
import com.hypernymbiz.zenath.model.WebAPIResponse;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Metis on 19-Mar-18.
 */

public class UpcomingJobFragment extends Fragment implements com.google.android.gms.location.LocationListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    View view;
    String getUserAssociatedEntity;
    LoadingDialog dialog;
    TextView txt_assign_truck, txt_endpoint, txt_activitytime;
    List<UpcomingActionItem> upcomingActionItems;
    RecyclerView recyclerView;
    UpcomingAdapter upcomingAdapter;
    SupportMapFragment supportMapFragment;
    ImageButton mylocation;
    private long UPDATE_INTERVAL = 1000;  /* 1 sec */
    private long FASTEST_INTERVAL = 500; /* 1/2 sec */
    public static int counter = 0;
    GoogleMap googleMap;
    GoogleApiClient googleApiClient;
    private LocationRequest mLocationRequest;
    Marker marker;
    boolean checklocation = true;
    String driverlocation;
    LatLng ll;

    int marker_height = 200;
    int marker_width = 200;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_pager_job_scheduled, container, false);
        txt_assign_truck = (TextView) view.findViewById(R.id.txt_assigned_truck);
        txt_endpoint = (TextView) view.findViewById(R.id.txt_end_point);
        txt_activitytime = (TextView) view.findViewById(R.id.txt_activity_time);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_bins);
        recyclerView.setLayoutManager(getLinearLayoutManager());
        recyclerView.setHasFixedSize(true);
        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
        mylocation = (ImageButton) view.findViewById(R.id.btn_mylocation);
        buildGoogleApiClient();
        initMap();
        startLocationUpdates();


        dialog = new LoadingDialog(getActivity(), getString(R.string.msg_loading));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        return view;

    }

    private void initMap() {
        supportMapFragment.getMapAsync(this);

    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();
    }


    @SuppressLint("RestrictedApi")
    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        final LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.getFusedLocationProviderClient(getActivity()).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        counter++;
                        try {
                            if (counter > 1) {
                                onLocationChanged(locationResult.getLastLocation());
                                LocationServices.getFusedLocationProviderClient(getActivity()).removeLocationUpdates(this);
                            }
                        } catch (Exception ex) {

                        }
                    }
                },
                Looper.myLooper());
    }


    private LinearLayoutManager getLinearLayoutManager() {

        return new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        final MarkerOptions option;
        if (marker != null) {

            marker.remove();
        }

        ll = new LatLng(location.getLatitude(), location.getLongitude());

        mylocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 12.8f);
                googleMap.animateCamera(update);
            }
        });

        Double lat, lng;

        lat = location.getLatitude();
        lng = location.getLongitude();


        try {
            option = new MarkerOptions().title("Driver").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location)).position(new LatLng(lat, lng));
            marker = googleMap.addMarker(option);
        } catch (Exception ex) {

        }

        if (googleMap != null) {

            if (checklocation == true) {
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 12.8f);
                googleMap.animateCamera(update);
                checklocation = false;

            }
        }


        driverlocation = lat.toString() + "," + lng.toString();
        dialog.dismiss();
        Log.e("TAAAG", "unshow");
//        Toast.makeText(getContext(), "" + driverlocation, Toast.LENGTH_SHORT).show();
        supportMapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;


        MapStyleOptions mapStyleOptions = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map);
        googleMap.setMapStyle(mapStyleOptions);

        ApiInterface.retrofit.getdata().enqueue(new Callback<WebAPIResponse<GetAppJobs>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<GetAppJobs>> call, Response<WebAPIResponse<GetAppJobs>> response) {
                dialog.dismiss();
                if (response.isSuccessful()) {
                    try {
                        if (response.body().status == 200) {

                            upcomingActionItems = response.body().response.getUpcoming().getActionItems();
                            upcomingAdapter = new UpcomingAdapter(upcomingActionItems, getContext());
                            recyclerView.setAdapter(upcomingAdapter);


                            txt_assign_truck.setText(response.body().response.getUpcoming().getPrimaryEntity());
                            txt_endpoint.setText(response.body().response.getUpcoming().getActivityEndPoint());
                            txt_activitytime.setText(AppUtils.getTimedate(response.body().response.getUpcoming().getActivityDatetime()));


                            BitmapDrawable binmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_bin_marker);
                            Bitmap b = binmarker.getBitmap();
                            Bitmap BinMarker = Bitmap.createScaledBitmap(b, marker_width, marker_height, false);

                            if (upcomingActionItems.size() > 1) {
                                for (int i = 0; i < upcomingActionItems.size() - 1; i++) {
                                    try {
                                        String array1[] = upcomingActionItems.get(i).getEntityLocation().split(",");
                                        LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));

                                        String start_bin_name = upcomingActionItems.get(i).getLabel();

                                        String array2[] = upcomingActionItems.get(i + 1).getEntityLocation().split(",");
                                        LatLng dest = new LatLng(Double.parseDouble(array2[0]), Double.parseDouble(array2[1]));

                                        String end_bin_name = upcomingActionItems.get(i + 1).getLabel();

                                        googleMap.addMarker(new MarkerOptions().title(start_bin_name).position(start).icon((BitmapDescriptorFactory.fromBitmap(BinMarker))));
                                        googleMap.addMarker(new MarkerOptions().title(end_bin_name).position(dest).icon((BitmapDescriptorFactory.fromBitmap(BinMarker))));
                                    } catch (Exception ex) {

                                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                                        break;
                                    }
                                }
                            } else {
                                try {

                                    String array1[] = upcomingActionItems.get(0).getEntityLocation().split(",");
                                    LatLng start = new LatLng(Double.parseDouble(array1[0]), Double.parseDouble(array1[1]));
                                    String end_bin_name = upcomingActionItems.get(0).getLabel();

                                    googleMap.addMarker(new MarkerOptions().title(end_bin_name).position(start).icon((BitmapDescriptorFactory.fromBitmap(BinMarker))));
                                } catch (Exception ex) {

                                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                                }
                                // functionality if lenght is one
                            }

                            BitmapDrawable dumpmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_dump_marker);
                            Bitmap dump = dumpmarker.getBitmap();
                            Bitmap Dumpmarker = Bitmap.createScaledBitmap(dump, marker_width, marker_height, false);


                            BitmapDrawable sortmarker = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_sort_marker);
                            Bitmap sort = sortmarker.getBitmap();
                            Bitmap Sortmarker = Bitmap.createScaledBitmap(sort, marker_width, marker_height, false);


                            try {

                                String dumpname = response.body().response.getUpcoming().getActivityEndPoint();
                               // String sortname = response.body().response.getUpcoming().getCheckPointName().toString();
                                //for last one
                                String dumpArray[] = response.body().response.getUpcoming().getActivityEndPointLatLng().toString().split(",");
                              //  String sortArray[] = response.body().response.getUpcoming().getCheckPointLatLong().toString().split(",");


//                                if(sortname==null&&response.body().response.getUpcoming().getCheckPointLatLong()==null)
//                                {
//                                    LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
//                                    googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));
//
//                                }
//                                else {

                                    LatLng dumping = new LatLng(Double.parseDouble(dumpArray[0]), Double.parseDouble(dumpArray[1]));
                                  //  LatLng sorting = new LatLng(Double.parseDouble(sortArray[0]), Double.parseDouble(sortArray[1]));

                                    googleMap.addMarker(new MarkerOptions().title(dumpname).position(dumping).icon((BitmapDescriptorFactory.fromBitmap(Dumpmarker))));
                                  //  googleMap.addMarker(new MarkerOptions().title(sortname).position(sorting).icon((BitmapDescriptorFactory.fromBitmap(Sortmarker))));

                              //  }
                            } catch (Exception ex) {
                                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                            }
                        }
                    }
                    catch (Exception ex) {
                        dialog.dismiss();
                    }
                }
                else {

                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse<GetAppJobs>> call, Throwable t) {
                dialog.dismiss();
                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

            }
        });


    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        } catch (Exception ex) {
            Toast.makeText(getContext(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
