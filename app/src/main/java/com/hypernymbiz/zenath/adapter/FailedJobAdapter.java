package com.hypernymbiz.zenath.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hypernymbiz.zenath.FrameActivity;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.fragments.JobDetailsFragment;
import com.hypernymbiz.zenath.model.Failed;
import com.hypernymbiz.zenath.model.JobInfo_;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;

import java.util.List;

/**
 * Created by Metis on 28-Mar-18.
 */

public class FailedJobAdapter extends RecyclerView.Adapter<FailedJobAdapter.MyViewHolder> {
    private List<Failed> jobInfo_s;
    Context context;

    public FailedJobAdapter(List<Failed> jobInfo_s, Context context)
    {

        this.jobInfo_s=jobInfo_s;
        this.context = context;
    }

    @Override
    public FailedJobAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_failed_job, parent, false);
        return new FailedJobAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FailedJobAdapter.MyViewHolder holder, final int position) {

        holder.jobname.setText(jobInfo_s.get(position).getActivityType());
        holder.jobstatus.setText(jobInfo_s.get(position).getActivityStatus());
        holder.starttime.setText(AppUtils.getTime(jobInfo_s.get(position).getActivityTime()));
        holder.endtime.setText(jobInfo_s.get(position).getEndPointName());

//        holder.endtime.setText(jobInfo_s.get(position).getJob_end_time());
        // holder.failed_job.setText(jobInfo_s.size());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer id = jobInfo_s.get(position).getId();

                Intent intent = new Intent(context, FrameActivity.class);
                intent.putExtra("jobid",  Integer.toString(id));
                intent.putExtra(Constants.FRAGMENT_NAME, JobDetailsFragment.class.getName());
//                    intent.putExtra(Constants.DATA, bundle);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return jobInfo_s.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView jobname, jobstatus, starttime, endtime,failed_job;

        public MyViewHolder(View itemView) {
            super(itemView);

            jobname = (TextView) itemView.findViewById(R.id.txt_activity_name);
            jobstatus = (TextView) itemView.findViewById(R.id.txt_job_status);
            starttime = (TextView) itemView.findViewById(R.id.txt_job_starttime);
            endtime = (TextView) itemView.findViewById(R.id.txt_job_endtime);
//            failed_job = (TextView) itemView.findViewById(R.id.txt_no_failed_job);

        }
    }
}