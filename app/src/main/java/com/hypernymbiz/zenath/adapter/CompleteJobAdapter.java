package com.hypernymbiz.zenath.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hypernymbiz.zenath.FrameActivity;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.fragments.CompletedJobDetail;
import com.hypernymbiz.zenath.fragments.JobDetailsFragment;
import com.hypernymbiz.zenath.model.Completed;
import com.hypernymbiz.zenath.model.JobDetail;
import com.hypernymbiz.zenath.model.JobInfo_;
import com.hypernymbiz.zenath.model.Respone_Completed_job;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;

import java.util.List;

/**
 * Created by Metis on 28-Mar-18.
 */

public class CompleteJobAdapter extends RecyclerView.Adapter<CompleteJobAdapter.MyViewHolder> {
    private List<Completed> jobInfo_s;
    Respone_Completed_job respone_completed_job;

    Context context;


    public CompleteJobAdapter(List<Completed> jobInfo_s, Context context)
    {

        this.jobInfo_s=jobInfo_s;
        this.context = context;
    }

    @Override
    public CompleteJobAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_complete_job, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CompleteJobAdapter.MyViewHolder holder,final int position) {
//        holder.jobid.setText(String.valueOf(jobInfo_s.get(position).getJobId()));
        holder.jobname.setText(jobInfo_s.get(position).getActivityType());
        holder.jobstatus.setText(jobInfo_s.get(position).getActivityStatus());
        holder.starttime.setText(AppUtils.getTime(jobInfo_s.get(position).getActivityTime()));
        holder.endtime.setText(jobInfo_s.get(position).getEndPointName());

//        holder.endtime.setText(AppUtils.getTime(jobInfo_s.get(position).getJob_end_time()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer id = jobInfo_s.get(position).getId();

                Intent intent = new Intent(context, FrameActivity.class);
                intent.putExtra("jobid",  Integer.toString(id));
                intent.putExtra(Constants.FRAGMENT_NAME, JobDetailsFragment.class.getName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobInfo_s.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView jobid,jobname, jobstatus, starttime, endtime,compled_job;

        public MyViewHolder(View itemView) {
            super(itemView);

            jobname = (TextView) itemView.findViewById(R.id.txt_activity_name);
//            jobid = (TextView) itemView.findViewById(R.id.txt_job_id);
            jobstatus = (TextView) itemView.findViewById(R.id.txt_job_status);
            starttime = (TextView) itemView.findViewById(R.id.txt_job_starttime);
            endtime = (TextView) itemView.findViewById(R.id.txt_job_endtime);
//            compled_job = (TextView) itemView.findViewById(R.id.txt_no_completed_job);

        }
    }
}