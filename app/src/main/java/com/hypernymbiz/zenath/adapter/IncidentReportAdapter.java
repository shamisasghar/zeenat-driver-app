package com.hypernymbiz.zenath.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.hypernymbiz.zenath.FrameActivity;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.dialog.NotesDialog;
import com.hypernymbiz.zenath.dialog.SimpleDialog;
import com.hypernymbiz.zenath.fragments.JobDetailsFragment;
import com.hypernymbiz.zenath.model.Completed;
import com.hypernymbiz.zenath.model.Datum;
import com.hypernymbiz.zenath.model.IncidentReport;
import com.hypernymbiz.zenath.model.Respone_Completed_job;
import com.hypernymbiz.zenath.toolbox.OnDialogButtonClick;
import com.hypernymbiz.zenath.toolbox.OnItemClickListener;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;

import java.util.List;

/**
 * Created by Metis on 10-May-18.
 */

public class IncidentReportAdapter extends RecyclerView.Adapter<IncidentReportAdapter.MyViewHolder> {
    private List<Datum> incidentReports;
    NotesDialog notesDialog;

    Context context;


    public IncidentReportAdapter(List<Datum> incidentReports, Context context) {

        this.incidentReports = incidentReports;
        this.context = context;
    }

    @Override
    public IncidentReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_incident, parent, false);
        return new IncidentReportAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final IncidentReportAdapter.MyViewHolder holder, final int position) {
        holder.inicident_name.setText(incidentReports.get(position).getIncident());
        holder.incident_time.setText(AppUtils.getTimedate(incidentReports.get(position).getTimestamp()));
        holder.incident_date.setText(AppUtils.getFormattedDate(incidentReports.get(position).getTimestamp()));

        final String notes = incidentReports.get(position).getNotes();


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                notesDialog = new NotesDialog(context, "Remarks", notes, "OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                       switch (view.getId())
                       {
                           case R.id.button_positive:
                               notesDialog.dismiss();
                               break;
                       }

                    }
                });
                notesDialog.show();

            }

//
//                Intent intent = new Intent(context, FrameActivity.class);
//                intent.putExtra("jobid",  Integer.toString(id));
//                intent.putExtra(Constants.FRAGMENT_NAME, JobDetailsFragment.class.getName());
//                context.startActivity(intent);

                });
    }


    @Override
    public int getItemCount() {
        return incidentReports.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView inicident_name, incident_time, incident_notes,incident_date;

        public MyViewHolder(View itemView) {
            super(itemView);

            inicident_name = (TextView) itemView.findViewById(R.id.txt_incident);
            incident_time = (TextView) itemView.findViewById(R.id.txt_time);
            incident_date = (TextView) itemView.findViewById(R.id.txt_date);


        }
    }
}