package com.hypernymbiz.zenath.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.model.ActionItem;
import com.hypernymbiz.zenath.model.BinData;
import com.hypernymbiz.zenath.model.Respone_Completed_job;

import java.util.List;

/**
 * Created by Metis on 10-Apr-18.
 */

public class DialogAdapter extends  RecyclerView.Adapter<DialogAdapter.MyViewHolder>{
    private List<ActionItem> actionItems;
    Context context;


    public DialogAdapter(List<ActionItem> actionItems,Context context)
    {

        this.actionItems=actionItems;
        this.context = context;
    }

    @Override
    public DialogAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dialog_bin, parent, false);
        return new DialogAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DialogAdapter.MyViewHolder holder, int position) {


        holder.binname.setText(actionItems.get(position).getLabel());
        holder.binstatus.setText("Complete");
//        holder.weight.setText(actionItems.get(position).getBinWeight().toString());
        holder.binname.setText(actionItems.get(position).getLabel());


    }

    @Override
    public int getItemCount() {
        return actionItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView binname, binstatus, weight, time;

        public MyViewHolder(View itemView) {
            super(itemView);

            binname = (TextView) itemView.findViewById(R.id.txt_bin_name);
            binstatus = (TextView) itemView.findViewById(R.id.txt_bin_status);
            weight = (TextView) itemView.findViewById(R.id.txt_bin_weight);
            time = (TextView) itemView.findViewById(R.id.txt_bin_time);


        }
    }
}


