package com.hypernymbiz.zenath.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hypernymbiz.zenath.FrameActivity;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.fragments.JobDetailsFragment;
import com.hypernymbiz.zenath.model.JobInfo;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.Constants;

import java.util.List;

/**
 * Created by Metis on 23-Mar-18.
 */

public class JobNotifiyAdapter extends RecyclerView.Adapter<JobNotifiyAdapter.ViewHolder> {

    private List<JobInfo> jobInfo_s;
  //  List<IsViewed> isVieweds;
    public Context context;
    View v;

    public JobNotifiyAdapter(List<JobInfo> data, Context context) {
        this.jobInfo_s = data;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_job_notification, parent, false);
        ViewHolder vh = new ViewHolder(v);


        return vh;
    }

    @Override
    public void onBindViewHolder(final JobNotifiyAdapter.ViewHolder holder, final int position) {

        holder.job_name.setText(jobInfo_s.get(position).getActivityType());
        holder.job_title.setText(jobInfo_s.get(position).getTitle());
        holder.create_time.setText(AppUtils.getTime(jobInfo_s.get(position).getCreatedTime()));
        holder.create_date.setText(jobInfo_s.get(position).getCreatedDatetime());
        holder.job_mintues.setText(jobInfo_s.get(position).getMinutesAgo());




        holder.itemView.setOnClickListener(new View.OnClickListener() {
            int pos = holder.getAdapterPosition();
            Integer id = jobInfo_s.get(position).getActivityId();


            @Override
            public void onClick(View v) {

                if(jobInfo_s.get(position).getNotificationType()==118)
                {
                    AppUtils.showSnackBar(v, ""+jobInfo_s.get(position).getTitle());

                }
                else if(jobInfo_s.get(position).getNotificationType()==119)
                {
                    AppUtils.showSnackBar(v, ""+jobInfo_s.get(position).getTitle());
                }
                else if(jobInfo_s.get(position).getNotificationType()==113)
                {
                    AppUtils.showSnackBar(v, ""+jobInfo_s.get(position).getTitle());
                }
                else if(jobInfo_s.get(position).getNotificationType()==114)
                {
                    AppUtils.showSnackBar(v, ""+jobInfo_s.get(position).getTitle());
                }
                else if(jobInfo_s.get(position).getNotificationType()==115)
                {
                    AppUtils.showSnackBar(v, ""+jobInfo_s.get(position).getTitle());
                }

                else {
                    if(id!=null) {
                        if(jobInfo_s.get(position).getStatus().equals("Active")) {
                            Intent intent = new Intent(context, FrameActivity.class);
                            intent.putExtra("jobid", Integer.toString(id));
                            intent.putExtra(Constants.FRAGMENT_NAME, JobDetailsFragment.class.getName());
                            context.startActivity(intent);
                        }
                        else
                        {
                            AppUtils.showSnackBar(v, "Already Viewed This Notification");
                        }
                    }
                    else
                    {
                        AppUtils.showSnackBar(v, AppUtils.getErrorMessage(context, 2));

                    }
                }
            }

        });

    }

    @Override
    public int getItemCount() {
        return jobInfo_s.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView job_name, job_title,create_date,create_time,job_mintues;

        CardView cardView;
        int pos=getAdapterPosition();

        private ViewHolder(final View itemView) {
            super(itemView);

            job_name = (TextView) itemView.findViewById(R.id.jobname);
            job_title = (TextView) itemView.findViewById(R.id.txt_notification_title);
            create_time = (TextView) itemView.findViewById(R.id.txt_job_time);
            create_date = (TextView) itemView.findViewById(R.id.txt_job_date);
           job_mintues = (TextView) itemView.findViewById(R.id.txt_job_mintues);
            cardView = (CardView) itemView.findViewById(R.id.layout_cardview);


        }

    }
}