package com.hypernymbiz.zenath.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.model.ActionItem;
import com.hypernymbiz.zenath.utils.VectorDrawableUtils;

import java.util.List;

/**
 * Created by Metis on 25-Apr-18.
 */

public class JobDetailAdapter extends  RecyclerView.Adapter<JobDetailAdapter.MyViewHolder>{
    private List<ActionItem> actionItems;
    Context context;


    public JobDetailAdapter(List<ActionItem> actionItems,Context context)
    {

        this.actionItems=actionItems;
        this.context = context;
    }

    @Override
    public JobDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bin_jobdetail, parent, false);
        return new JobDetailAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JobDetailAdapter.MyViewHolder holder, int position) {


        holder.binname.setText(actionItems.get(position).getLabel());
//        holder.binstatus.setText("Complete");
////        holder.weight.setText(actionItems.get(position).getBinWeight().toString());
//        holder.binname.setText(actionItems.get(position).getBinName());
        holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(context, R.drawable.ic_marker_inactive, R.color.colorPrimary));

    }

    @Override
    public int getItemCount() {

        if(actionItems!=null)
        {

            return actionItems.size();
        }
        return 0;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView binname, binstatus, weight, time;
        TimelineView mTimelineView;

        public MyViewHolder(View itemView) {
            super(itemView);

            binname = (TextView) itemView.findViewById(R.id.txt_bin_name);
//            binstatus = (TextView) itemView.findViewById(R.id.txt_bin_status);
//            weight = (TextView) itemView.findViewById(R.id.txt_bin_weight);
//            time = (TextView) itemView.findViewById(R.id.txt_bin_time);
            mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);



        }
    }
}

