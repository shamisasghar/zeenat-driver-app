package com.hypernymbiz.zenath.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.hypernymbiz.zenath.R;
import com.hypernymbiz.zenath.model.ActionItem;
import com.hypernymbiz.zenath.model.BinData;
import com.hypernymbiz.zenath.model.JobInfo_;
import com.hypernymbiz.zenath.model.Orientation;
import com.hypernymbiz.zenath.model.Respone_Completed_job;
import com.hypernymbiz.zenath.utils.AppUtils;
import com.hypernymbiz.zenath.utils.VectorDrawableUtils;

import java.util.List;

/**
 * Created by Metis on 09-Apr-18.
 */

public class RunningJobAdapter extends  RecyclerView.Adapter<RunningJobAdapter.MyViewHolder>{
    private List<ActionItem> actionItems;
    Context context;
    private Orientation mOrientation;


    public RunningJobAdapter(List<ActionItem> actionItems,Context context) {

        this.actionItems = actionItems;
        this.context = context;

    }

    @Override
    public RunningJobAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_running_job, parent, false);
        return new RunningJobAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RunningJobAdapter.MyViewHolder holder, int position) {


        holder.binname.setText(actionItems.get(position).getLabel());
        holder.binstatus.setText(actionItems.get(position).getStatus());
       holder.weight.setText(actionItems.get(position).getWeight().toString()+"kg");
        holder.time.setText(actionItems.get(position).getClient());

        if(actionItems.get(position).equals("Uncollected"))
            {
                holder.binname.setTextColor(Color.WHITE);
                holder.binstatus.setTextColor(Color.WHITE);
                holder.weight.setTextColor(Color.WHITE);
//                holder.bin_check.setVisibility(View.VISIBLE);
                holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(context, R.drawable.ic_marker_inactive, R.color.colorPrimary));

            }
            else
            {
                holder.mTimelineView.setMarker(ContextCompat.getDrawable(context, R.drawable.ic_marker_inactive), ContextCompat.getColor(context, R.color.colorPrimary));
//                holder.bin_uncheck.setVisibility(View.VISIBLE);

            }

    }

    @Override
    public int getItemCount() {
        return actionItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView binname, binstatus, weight, invoice,time;
        ImageView bin_check,bin_uncheck;
        TimelineView mTimelineView;


        public MyViewHolder(View itemView) {
            super(itemView);

            binname = (TextView) itemView.findViewById(R.id.txt_bin_name);
            binstatus = (TextView) itemView.findViewById(R.id.txt_bin_status);
            weight = (TextView) itemView.findViewById(R.id.txt_bin_weight);
            time = (TextView) itemView.findViewById(R.id.txt_bin_time);
            mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);

//            bin_uncheck = (ImageView) itemView.findViewById(R.id.img_bin_uncheck);

        }
    }


}



